const images = {

  croix:require("../Images/croix.png"),
  ic_details: require("../Images/ic_details.png"),
  ic_order: require("../Images/ic_order.png"),
  ic_address: require("../Images/ic_address.png"),
  ic_notification: require("../Images/ic_notification.png"),
  ic_get_reward: require("../Images/ic_get_reward.png"),
  logoutIcon: require("../Images/ic_logout_yellow.png"),
  back:require("../Images/ic_Left_arrow_black_su09.png")

};

export default images;
