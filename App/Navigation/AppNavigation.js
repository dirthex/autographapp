import { Dimensions } from "react-native";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";
const { width, height } = Dimensions.get("window");


/* WooCommerce Screens List START */
import ECommerceLogin from "../Containers/ECommerce/Login/index";

import ECommerceMenu from "../Containers/ECommerce/Home/Main";
import WooCommerceSideMenu from "../Containers/ECommerce/Home/Sidebar";
import ECommerceAddress from "../Containers/ECommerce/Address/index";
import ECommerceChangePassword from "../Containers/ECommerce/ChangePassword/index";
import MonCompte from "../Containers/ECommerce/MyAccount/index";
import ECommerceMyInformation from "../Containers/ECommerce/MyInformation/index";
import ECommerceNotification from "../Containers/ECommerce/Notification/index";
import CodePromo from "../Containers/ECommerce/CodePromo/index";
import ECommerceForgotPassword from "../Containers/ECommerce/ForgotPassword/index";
import ECommerceResetPassword from "../Containers/ECommerce/ResetPassword/index";

import Annonces from "../Containers/ECommerce/Annonces/index";
import ECommerceEditInformation from "../Containers/ECommerce/EditInformation/index";
import Filter from "../Containers/ECommerce/Filter/index";
import AnnonceDetails from "../Containers/ECommerce/AnnonceDetails/index";
import AddAnnonce from "../Containers/ECommerce/AddAnnonce";
import BlogDetails from "../Containers/ECommerce/BlogDetails/index";
import maps from "../Containers/ECommerce/Maps/index";
import pdf from "../Containers/ECommerce/pdf/index";
import blog from "../Containers/ECommerce/Blogs/index";
import magazine from "../Containers/ECommerce/Magazines/index";
import mesannonces from "../Containers/ECommerce/MesAnnonces/index";
import BlogMenu from "../Containers/ECommerce/BlogMenu/index";
import email from "../Containers/ECommerce/AnnonceDetails/email";
import webview from "../Containers/ECommerce/AnnonceDetails/webview";
import register from "../Containers/ECommerce/Login/register";
import favoris from "../Containers/ECommerce/Annonces/Favorite";
/* WooCommerce Screens List END */


/*Ecommerce Drawer START*/
const DrawerStackECommerce = createDrawerNavigator(
  {
    ECommerceMenu: { screen: ECommerceMenu },
    ECommerceAddress: { screen: ECommerceAddress },
    ECommerceChangePassword: { screen: ECommerceChangePassword },
    //ECommerceCheckout: { screen: ECommerceCheckout },
    //ECommerceFAQ: { screen: ECommerceFAQ },
   // ECommerceFAQGeneralEnquiry: { screen: ECommerceFAQGeneralEnquiry },
   // ECommerceInviteFriends: { screen: ECommerceInviteFriends },
    MonCompte: { screen: MonCompte },
   // ECommerceMyBag: { screen: ECommerceMyBag },
    ECommerceMyInformation: { screen: ECommerceMyInformation },
    ECommerceNotification: { screen: ECommerceNotification },
    CodePromo: { screen: CodePromo },
    //ECommerceProductDetails: { screen: ECommerceProductDetails },
    //ECommerceProductDetailsTab: { screen: ECommerceProductDetailsTab },
    //ECommerceWishList: { screen: ECommerceWishList },
    //ECommerceChangeCountry: { screen: ECommerceChangeCountry },
    //ECommerceSale: { screen: ECommerceSale },
    //ECommerceAboutUs: { screen: ECommerceAboutUs },
    //ECommerceBrands: { screen: ECommerceBrands },
   // ECommercePrivacyPolicy: { screen: ECommercePrivacyPolicy },
    ECommerceLogin: { screen: ECommerceLogin },
    ECommerceForgotPassword: { screen: ECommerceForgotPassword },
    ECommerceResetPassword: { screen: ECommerceResetPassword },
    ECommerceEditInformation: { screen: ECommerceEditInformation },
    Filter :{screen : Filter},
    AnnonceDetails :{screen : AnnonceDetails},
    AddAnnonce:{screen:AddAnnonce},
    BlogDetails:{screen:BlogDetails},
    Annonces:{screen:Annonces},
    maps:{screen:maps},
    pdf:{screen:pdf},
    blog:{screen:blog},
    magazine:{screen:magazine},
    mesannonces:{screen:mesannonces},
    BlogMenu:{screen:BlogMenu},
    favoris :{screen:favoris},
    webview:{screen:webview},
    register:{screen:register},
    email:{screen:email}
  },
  {
    gesturesEnabled: false,
    contentComponent: WooCommerceSideMenu
  }
);

// const App = createAppContainer(PrimaryNav);

export default DrawerStackECommerce;
