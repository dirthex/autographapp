import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	StatusBar,
	TouchableOpacity,
	Linking,
	Platform,
	BackHandler,
	I18nManager,
	AsyncStorage
} from 'react-native';
import Moment from 'moment';
import {
	Content,
	Container,
	Right,
	Header,
	Left,
	Body,
	Title,
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import styles from './styles';
import { Images, Fonts, Metrics, Colors } from '../../../Themes';
import axios from "axios";
import Config from "../Config/config";
import { SocialIcon } from 'react-native-elements'

export default class CodePromo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			codePromo: [],
			code : null,
			token:"",
		};
	}

	onBackClick() {
		this.props.navigation.navigate('ECommerceMyAccount');
	}
	componentDidMount() {
		this.getToken().then(token => {
			this.getCodePromo(token);
		});
	}
	componentWillMount() {
		var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('MonCompte');
			return true;
		});
	}
	getToken = async () => {
		const userToken = await AsyncStorage.getItem('token');
		const token = userToken;
		this.state.token = userToken;
		return Promise.resolve(token);
	}
	getCodePromo(token) {

		const headers = {
			'Authorization': 'Bearer ' + token
		};

		this.setState({ loading: true });
		axios.get(Config.SERVER_URL + '/api/v1/codepromo', {
			headers
		}
		).then((res) => {
			console.log("*****" + JSON.stringify(res.data.data))

			this.setState({

				codePromo: res.data.data,



			});


			console.log("json codePromo" + JSON.stringify(codePromo))
			this.setState({ loading: false });






		}).catch((err) => {

			this.setState({ loading: false });

		});


	}
	generateCode(id){
		console.log(id)
		console.log(this.state.token)
		const headers = {
			'Authorization': 'Bearer ' + this.state.token
		};

		axios.get(Config.SERVER_URL + '/api/v1/user/activecde/'+id, {
			headers
		}
		).then((res) => {
			console.log("*****" + res.data.code)

			this.setState({

				code: res.data.code ,



			});


			console.log("json codePromo" + JSON.stringify(codePromo))
			this.setState({ loading: false });






		}).catch((err) => {

			this.setState({ loading: false });

		});
		this.getCodePromo(this.state.token)
	}

	_handleBagNavigation() {
		AsyncStorage.multiSet([
			['ArrivedFrom', "ECommerceOrderHistory"],
		]);
		this.props.navigation.navigate("ECommerceMyBag");
	}

	_handleWishListNavigation() {
		AsyncStorage.multiSet([
			['ArrivedForWishList', "ECommerceOrderHistory"],
		]);
		this.props.navigation.navigate("ECommerceWishList");
	}

	render() {
		StatusBar.setBarStyle('light-content', true);
		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor('#0e1130', true);
			StatusBar.setTranslucent(true);
		}

		return (
			<Container style={styles.container}>
				<Header androidStatusBarColor={'#0e1130'} style={styles.header}>
					<Left style={styles.left}>
						<TouchableOpacity
							style={styles.backArrow}
							onPress={()=>	this.props.navigation.navigate('MonCompte')}>
							<FontAwesome
								name={I18nManager.isRTL ? 'angle-right' : 'angle-left'}
								size={30}
								color="white"
								style={{ paddingRight: 20 }}
							/>
						</TouchableOpacity>
					</Left>

					<Right style={styles.right}>
						<SocialIcon
							type='facebook'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.facebook) }}
						/>
						<SocialIcon
							type='instagram'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.instagram) }}
						/>
						<SocialIcon
							type='youtube'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.youtube) }}
						/>
					</Right>

				</Header>

				<Content style={styles.content}> 
				<View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#ed6663"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        // marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      CODES PROMO
                                                            </Text>
                  </View>
					{this.state.codePromo.map((item, index) => {
						return (
							<View
								style={
									item.id === 1
										? [styles.rowBg, { marginTop: Metrics.WIDTH * 0.05 }]
										: styles.rowBg
								}
								key={index}>
								<View style={styles.rowField}>
									<Text style={styles.fieldLabelTxt}>Titre</Text>
									<Text style={styles.fieldDescriptionTxt}>{item.titre}</Text>
								</View>
								<View style={styles.rowListDivider} />

								<View style={styles.rowField}>
									<Text style={styles.fieldLabelTxt}>Date début</Text>
									<Text style={styles.fieldDescriptionTxt}> {Moment(item.dateDebut).format('DD-MM-YYYY')}</Text>
								</View>
								<View style={styles.rowListDivider} />
								<View style={styles.rowField}>
									<Text style={styles.fieldLabelTxt}>Date fin</Text>
									<Text style={styles.fieldDescriptionTxt}> {Moment(item.dateFin).format('DD-MM-YYYY')}</Text>
								</View>
							
								<View style={styles.rowListDivider} />
								{item.nombreBenTotale == null ? (
									<View style={styles.rowField}>
										<Text style={styles.fieldLabelTxt}>NOMBRE RESTANT</Text>
										<Text style={styles.fieldDescriptionTxt}>illimité</Text>
									</View>
								) : (
										<View style={styles.rowField}>
											<Text style={styles.fieldLabelTxt}>NOMBRE RESTANT</Text>
											<Text style={styles.fieldDescriptionTxt}>{item.nombreBenTotale}</Text>
										</View>
									)}
								<View style={styles.rowListDivider} />
								
								{item.related_user_code && item.related_user_code.length >= 1  ? (
									<View style={styles.rowCode}>
											<Text style={styles.fieldLabelcode}>Code</Text>
											<Text style={styles.code}>{item.related_user_code[0].code}</Text>
										</View>
								):(
								<View style={styles.button}>
									<TouchableOpacity onPress={()=>this.generateCode(item.id)} >
										<Text style={styles.buttonTxt}>BENEFICIER</Text>
									</TouchableOpacity>

								</View>
								)
								}
							</View>
						);
					})}
				</Content>
			</Container>
		);
	}
}
