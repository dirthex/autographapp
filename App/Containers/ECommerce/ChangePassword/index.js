
import React, { Component } from 'react';
import { Text, View, Image,Linking, Alert, StatusBar, TouchableOpacity, Platform, ImageBackground, BackHandler, ScrollView, AsyncStorage, I18nManager } from 'react-native';
import { Content, Container, Button, Icon, Right, Item, Input, Header, Left, Body, Title, Segment, Label } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles';
import { Fonts, Metrics, Colors } from '../../../Themes/';
import Globals from '../Globals';
import FloatingLabelInput from '../FloatingLabelInput';
import axios from "axios";
import Config from "../Config/config"

export default class ChangePassword extends Component {

	constructor(props) {
		super(props);
		this.state = {
			old_pwd: '',
			new_pwd: '',
			confirm_pwd: '',
			login_Id: '',
			id: '',
			token: ''

		};
	}

	componentWillMount() {
		const { navigation } = this.props;
		this.state.id = navigation.getParam('id', null)
		this.state.token = navigation.getParam('token', null)
		var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('ECommerceMyInformation');
			return true;
		});

		AsyncStorage.multiGet(['user_id']).then((data) => {
			this.setState({
				login_Id: data[0][1],
				// oldPassword: data[1][1],
			});
		});
	}
	changePassword() {
		console.log("toooooken" + this.state.token)
		console.log(this.state.id)
		const headers = {
			'Authorization': 'Bearer ' + this.state.token
		};
		let data = {

			odlPassword: this.state.old_pwd,
			password: this.state.new_pwd,


		}
		if (this.state.new_pwd != this.state.confirm_pwd) {
			Alert.alert("Les mots de passes ne sont pas identiques")
		
		}
		else if (this.state.new_pwd =="" || this.state.confirm_pwd =="" || this.state.old_pwd =="") 
		{
			Alert.alert("Veuillez remplir les champs")
		}
		else {
			axios.post(Config.SERVER_URL + '/api/v1/profile/password/' + this.state.id, data, {
				headers
			}
			).then((res) => {
				console.log(res.data)
				if (res.data == 200) {
					Alert.alert("Modification avec succés")
				}
				else if (res.data == 304) {
					Alert.alert("Erreur de vérification de mot de passe ")
				}
				else {
					Alert.alert("Réssayer plutard ")
				}
				this.setState({ loading: false })





			}).catch((error) => {
				this.setState({
					error: 'Error retrieving data',
					loading: false
				});
			});
		}

	}
	render() {
		StatusBar.setBarStyle('light-content', true);

		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor("#0e1130", true);
			StatusBar.setTranslucent(true);
		}

		return (
			<Container style={styles.container}>
				<Header
					androidStatusBarColor={"#0e1130"}
					style={styles.header}>
					<Left style={styles.left}>
						<TouchableOpacity style={styles.backArrow} onPress={() => this.props.navigation.navigate('ECommerceMyInformation')}>
							<FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={Fonts.moderateScale(30)} color="white" style={{ paddingRight: 20 }} />
						</TouchableOpacity>
					</Left>
					<Body style={styles.body}>
						<Text style={styles.textTitle}>Modifier mot de passe</Text>
					</Body>
					<Right style={styles.right}>
					</Right>
				</Header>

				<View style={styles.content}>

					<KeyboardAwareScrollView>

						<View style={styles.floatingView}>

							<FloatingLabelInput
								label="Mot de passe"
								value={this.state.old_pwd}
								keyboardType='default'
								secureTextEntry={true}
								returnKeyType='done'
								maxLength={40}
								selectionColor={'#959595'}
								autoCapitalize='none'
								onChangeText={(old_pwd) => this.setState({ old_pwd })} />

							<View style={{ height: 10 }} />

							<FloatingLabelInput
								label="Nouveau mot de passe"
								value={this.state.new_pwd}
								keyboardType='default'
								secureTextEntry={true}
								returnKeyType='done'
								maxLength={40}
								selectionColor={'#959595'}
								autoCapitalize='none'
								onChangeText={(new_pwd) => this.setState({ new_pwd })} />

							<View style={{ height: 10 }} />

							<FloatingLabelInput
								label="Confirmer mot de passe"
								value={this.state.confirm_pwd}
								keyboardType='default'
								secureTextEntry={true}
								returnKeyType='done'
								maxLength={40}
								selectionColor={'#959595'}
								autoCapitalize='none'
								onChangeText={(confirm_pwd) => this.setState({ confirm_pwd })} />

						</View>

					</KeyboardAwareScrollView>

				</View>

				<View style={styles.divider}></View>

				<View style={styles.bottomView}>
					<TouchableOpacity style={styles.changeBtnBG} onPress={() => this.changePassword()}>
						<Text style={styles.changeBtnTxt}>Valider</Text>
					</TouchableOpacity>
				</View>
			</Container>
		);
	}
}
