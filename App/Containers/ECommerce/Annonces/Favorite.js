import React, { PureComponent } from "react";
import {
  View,
  Text,
  Image,
  StatusBar,
  Platform,
  TouchableOpacity,
  BackHandler,
  ListView,
  ImageBackground,
  ScrollView,
  Picker,
  I18nManager,
  AsyncStorage,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  ScrollViewBase
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Swipeout from 'react-native-swipeout';
import { withNavigation } from "react-navigation";
import {
  Container,
  Button,
  Right,
  Left,
  ListItem,
  Content,
  Body,
  Header,
  Spinner
} from "native-base";
// Screen Styles
import styles from "./styles";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import Dropdown from "../../../Components/Dropdown/dropdown";
import { Fonts, Metrics, Colors, Images } from "../../../Themes";
import { CachedImage } from "react-native-cached-image";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "../../../Components/react-native-scrollable-tab-view";
import axios from "axios";
import Config from "../Config/config";
import { throwStatement } from "@babel/types";

// var screenLink = "";

class Favrite extends PureComponent {
  signal = axios.CancelToken.source();
  constructor(props) {
    super(props);

    (_isMounted = false), (this.page = 1);
    this.state = {
      // loading: false, // user list loading
      isRefreshing: true,
      isLoading: true,
      voiture: [],
      voitures: [],
      screenLink: "",
      selectedSize: "M",
      selectedLots: "1",
      isDroDownOpen: false,
      typography: "M",
      selectedColor: [],
      product_id: [],
      cars: [],
      ecran: "",
      headr: "",
      categorie: "",
      categorieChoisi: "",
      titre: "all",
      marqueChoisi: "all",
      model: "all",
      prixMin: "all",
      prixMax: "all",
      anneemin: "all",
      anneemax: "all",
      kilmin: 0,
      kilmax: 0,
      cylindremin: "",
      cylindremax: "",
      carb: "all",
      vitesse: "all",
      favoriteCars: [],
      finalArray: [],
      url: "",
      nommarque: "",
      type: "",
      ids: [],
      list: []
    };
    this.onChangeText = this.onChangeText.bind(this);
    this._handleProductDetailClick = this._handleProductDetailClick.bind(this);
    this.props.navigation.addListener("willFocus", () => {
      console.log("caaat" + this.state.categorieChoisi);

      //this.getblogs(id);
    });
  }
  componentWillUnmount() {
    //this._isMounted = false;
    this.signal.cancel("Api is being canceled");
  }
  componentDidMount() {
    //this.getFavoris();
    /*   this.getAsync().then(array => {
      this.getFavoris(array);
    });*/
  }

  componentWillMount() {
    AsyncStorage.multiGet(["ids"]).then(data => {
      // console.log("catt data[2][1]");
      console.log("ids favortie" + data[0][1]);

      this.setState({
        ids: JSON.parse(data[0][1])
      });
      this.getFavoris();
      /* console.log("1"+this.state.ids[1].id)
       var tab = [] ;
       for(var i =0 ; i<this.state.ids.length ; i++){
         console.log(this.state.ids[i].id)
          tab.push(this.state.ids[i].id) 
       }
         
  this.getFavoris(tab)*/
    });

    BackHandler.addEventListener("hardwareBackPress", () => {
      if (this.state.ecran == "Menu") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if ((this.state.ecran = "AnnonceDetails")) {
        this.props.navigation.navigate("ECommerceMenu");
      }
      return true;
    });

    console.log("teeest" + this.state.ecran);
  }

  _backPress(backFrom) {
    const { screenLink } = this.state;

    if (screenLink == "ECommerceMenu") {
      if (backFrom == "fromHardwareBack") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (backFrom == "fromLeftIcon") {
        this.props.navigation.openDrawer();
      }
    }
    if (screenLink == "ECommerceMyBag") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (screenLink == "ECommerceWishList") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (screenLink == "ECommerceMain") {
      this.props.navigation.navigate("ECommerceMenu");
    }
  }

  _handleBagNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "ECommerceCategoryProduct"]]);
    this.props.navigation.navigate("ECommerceMyBag");
  }

  _handleWishListNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate("ECommerceWishList");
  }

  onChangeText(text) {
    this.setState({ user_category_id: text });
  }

  onCheckBoxPress(id) {
    this.setState({
      selectedLots: id
    });
  }

  _handleDropDownClick(size) {
    this.setState({ selectedSize: size });
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _openDropDown(id) {
    this.onCheckBoxPress(id);
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _handleProductDetailClick() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate("AnnonceDetails", {
      screen: "Annonces"
    });
  }

  _handleLEFT(screenLink) {
    if (screenLink == "ECommerceMenu") {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Ionicons
            name="ios-menu"
            size={30}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this._backPress("fromLeftIcon")}
        >
          <FontAwesome
            name={I18nManager.isRTL ? "angle-right" : "angle-left"}
            size={35}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    }
  }
  /* getAsync = async () => {
     const data = await AsyncStorage.multiGet(["ids"]);
     
     const array = JSON.parse(data[0][1]);
     console.log("arraaaaaaaaaay")
     console.log(array)
     return Promise.resolve(array);
   }*/
  delete(id) {
    var arraytoBuild = this.state.finalArray; 
    arraytoBuild = arraytoBuild.filter(function(item) { 
      return item.id !== id;  
    });
    this.setState({ finalArray :arraytoBuild});
   
    var valIds = this.state.ids.filter(function(item) { 
      return item.id !== id;  
    });
    console.log(JSON.stringify(valIds))
    AsyncStorage.multiSet([["ids", JSON.stringify(valIds)]]);
  }

  getFavoris() {
    this.setState({ favoriteCars: [] });
    this.setState({ finalArray: [] });

    this.setState({
      loading: true,
      isRefreshing: true
    });

    /*   AsyncStorage.multiSet([
         ['idFav', JSON.stringify(array)],
       ]);*/
    console.log("****************");
    console.log(this.state.ids);
    if (this.state.ids != null) {
      for (var i = 0; i < this.state.ids.length; i++) {
        this.state.list.push(this.state.ids[i].id);
      }
    }
    const url =
      Config.SERVER_URL + "/api/v1/mes_favoris/" + this.state.list + ",";
    console.log(url);
    axios
      .get(url)
      .then(res => {
        this.setState({
          favoriteCars: res.data,

          loading: false,
          isRefreshing: false
        });
        console.log("*******" + JSON.stringify(this.state.favoriteCars));
        var arraytoBuild = [];
        for (var i = 0; i < this.state.favoriteCars.length; i++) {
          if (this.state.favoriteCars[i].principalPhoto.includes("https")) {
            arraytoBuild.push({
              id: this.state.favoriteCars[i].id,
              productTitle: this.state.favoriteCars[i].titre,
              productImage: this.state.favoriteCars[i].principalPhoto,
              price: this.state.favoriteCars[i].prixunqiue,
              boite: this.state.favoriteCars[i].boite,
              annee: this.state.favoriteCars[i].annee,
              kilometrage: this.state.favoriteCars[i].kilometrage,
              cylindre: this.state.favoriteCars[i].cylendree
            });
          } else {
            arraytoBuild.push({
              id: this.state.favoriteCars[i].id,
              productTitle: this.state.favoriteCars[i].titre,
              productImage:
                Config.SERVER_URL + this.state.favoriteCars[i].principalPhoto,
              price: this.state.favoriteCars[i].prixunqiue,
              boite: this.state.favoriteCars[i].boite,
              annee: this.state.favoriteCars[i].annee,
              kilometrage: this.state.favoriteCars[i].kilometrage,
              cylindre: this.state.favoriteCars[i].cylendree
            });
            this.setState({ type: this.state.favoriteCars[i].categorie });
          }
          this.setState({ finalArray :arraytoBuild});
        }

        //this.state.finalArray.push(this.state.favoriteCars)
      })
      .catch(error => {
        this.setState({
          error: "Error retrieving data",
          loading: false
        });
      });
  }
  _handleLike(item, id, like, index) {
    this.state.favoriteCars.push(item);

    var i;
    for (i = 0; i < this.state.cars.length; i++) {
      if (this.state.cars[i].id == id) {
        this.state.cars[i].isLike = false;
      }
    }
    this.state.cars.splice(index, 1);

    this.setState({
      cars: this.state.cars
    });
    AsyncStorage.multiSet([["cars", JSON.stringify(this.state.cars)]]);
    console.log("like" + JSON.stringify(this.state.cars));
  }

  navigateToFiltre() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    if (this.state.categorieChoisi == "all") this.state.categorieChoisi = "";
    if (this.state.titre == "all") this.state.titre = "";
    if (this.state.marqueChoisi == "all") this.state.marqueChoisi = "";
    if (this.state.model == "all") this.state.model = "";
    if (this.state.prixMin == "all") this.state.prixMin = "";
    //if (this.state.prixMin == 0 && this.state.prixMax == 0) { this.state.prixMin = "all"; this.state.prixMax = "all"; }
    if (this.state.prixMax == "all") this.state.prixMax = "";
    if (this.state.anneemin == "all") this.state.anneemin = "";
    if (this.state.anneemax == "all") this.state.anneemax = "";
    //if (this.state.kilmin == "") this.state.kilmin = "all";
    //if (this.state.kilmin == 0 && this.state.kilmax ==0) {this.state.kilmin = "all";this.state.kilmax="all";}
    //if (this.state.kilmax == "" || this.state.kilmax == null) this.state.kilmax = "all";
    if (this.state.carb == "all") this.state.carb = "";
    if (this.state.vitesse == "all") this.state.vitesse = "";
    // if (this.state.cylindremin == "all" ) this.state.cylindremin = "all";
    //if (this.state.cylindremax == "all" || this.state.cylindremax == null) this.state.cylindremax = "all";
    if (this.state.position == "all") this.state.position = "";
    console.log("annonce nom marque " + this.state.model);
    if (this.state.nommarque == "all" || this.state.nommarque == null)
      this.state.nommarque = "";
    console.log("annonce nom marque " + this.state.model);

    this.props.navigation.navigate("Filter", {
      cat: this.state.categorieChoisi,
      tite: this.state.titre,
      marque: this.state.marqueChoisi,
      model: this.state.model,
      prixMin: this.state.prixMin,
      prixMax: this.state.prixMax,
      anneeMin: this.state.anneemin,
      kilmin: this.state.kilmin,
      kilmax: this.state.kilmax,
      cymin: this.state.cylindremin,
      cymax: this.state.cylindremax,
      pos: this.state.position,
      carb: this.state.carb,
      vitesse: this.state.vitesse,
      marqua: this.state.marqueChoisi
    });
  }

  renderItemFavorite = ({ item, index }) => {
    swipeoutBtns = [
      {
        text: 'Supprimer',
        backgroundColor:"#e30613",
        onPress:() => this.delete(item.id)
      }
    ]
    return (
      <Swipeout  right={swipeoutBtns}>
      <View style={styles.mainListRenderRow}>
        <View style={styles.row} key={index}>
          <TouchableOpacity
            style={styles.rowBg}
            onPress={() => {
              this.props.navigation.navigate("AnnonceDetails", {
                idAnnonce: item.id,
                screenName: "annonce"
              });
            }}
          >
            <CachedImage
              source={{ uri: item.productImage }}
              style={styles.productImage}
            >
            </CachedImage>

            <View style={styles.productDetailBg}>
              <Text style={styles.productTitleTxt}>{item.productTitle}</Text>

              <View
                style={
                  styles.detailFieldRow2
                 }
              >
                <Text style={styles.priceTxt}>{item.price}€</Text>
                <TouchableOpacity
                    style={styles.closeIconBg2}
                    onPress={() => this.delete(item.id)}
                  >
                    <FontAwesome
                      name="close"
                      size={20}
                      color={"red"}
                      style={styles.closeImg}
                    />
                  </TouchableOpacity>
              </View>

              <View>
                <View
                  style={{
                    flexDirection: "column",
                    marginTop: Metrics.HEIGHT * 0.03
                  }}
                >
                  <View style={{ flexDirection: "row", marginLeft: "2%" }}>
                    <EvilIcons name="gear" size={25} color={"#000000"} />
                    <Text style={{ marginLeft: "3%" }}>{item.boite}</Text>
                  </View>
                  <View style={{ flexDirection: "row", marginLeft: "2%" }}>
                    <Entypo name="gauge" size={25} color={"#000000"} />
                    <Text style={{ marginLeft: "3%" }}>
                      {item.kilometrage} km
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginLeft: "2%" }}>
                    <EvilIcons name="calendar" size={25} color={"#000000"} />
                    <Text style={{ marginLeft: "3%" }}>{item.annee}</Text>
                  </View>
                </View>
                <View></View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      </Swipeout>
    );
  };

  render() {
    const { screenLink } = this.state;
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#0e1130", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <View style={styles.mainView2}>
        <Header
          style={styles.HeaderBg}
          androidStatusBarColor={"#000000"}
          transparent
        >
          <Left style={styles.left}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Entypo name="menu" color="#FFFFFF" size={30} />
            </TouchableOpacity>
          </Left>

          <Body style={styles.body}>
            <Text style={styles.headerTitle}></Text>
          </Body>

          <Right style={styles.right}>
            <TouchableOpacity>
              <MaterialIcons name="more-vert" color="#FFFFFF" size={25} />
            </TouchableOpacity>
          </Right>
        </Header>

        <View style={{ backgroundColor: "#e9e9e9" }}>
          <FlatList
            data={this.state.finalArray}
            renderItem={this.renderItemFavorite}
            extraData={this.state.finalArray}
            keyExtractor={(item, index) => item.id.toString()}
            ListEmptyComponent={
              <View style={{ justifyContent: "center" }}>
                <Text style={{ alignSelf: "center" }}>No Data Found.</Text>
              </View>
            }
          />
        </View>
      </View>
    );
  }
}
export default withNavigation(Favrite);
