import React, { PureComponent } from "react";
import {
  View,
  Text,
  Image,
  StatusBar,
  Platform,
  TouchableOpacity,
  BackHandler,
  ListView,
  ImageBackground,
  ScrollView,
  Picker,
  I18nManager,
  AsyncStorage,
  FlatList,
  RefreshControl,
  ActivityIndicator,

} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import { withNavigation } from 'react-navigation';
import {
  Container,
  Button,
  Right,
  Left,
  ListItem,
  Content,
  Body,
  Header,
  Spinner
} from "native-base";
// Screen Styles
import styles from "./styles";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import Dropdown from "../../../Components/Dropdown/dropdown";
import { Fonts, Metrics, Colors, Images } from "../../../Themes";
import { CachedImage } from "react-native-cached-image";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "../../../Components/react-native-scrollable-tab-view";
import axios from "axios";
import Config from "../Config/config";



// var screenLink = "";


class CategoryProduct extends PureComponent {
  signal = axios.CancelToken.source();
  constructor(props) {
    super(props);

    _isMounted = false,
      this.page = 1;
    this.state = {
      // loading: false, // user list loading
      isRefreshing: false,
      isLoading: true,
      voiture: [],
      voitures: [],
      array:[],
      screenLink: "",
      selectedSize: "M",
      selectedLots: "1",
      isDroDownOpen: false,
      typography: "M",
      selectedColor: [],
      product_id: [],
      cars: [],
      ecran: '',
      headr: '',
      categorie: '',
      categorieChoisi: "",
      titre: "all",
      marqueChoisi: "all",
      model: "all",
      prixMin: "all",
      prixMax: "all",
      anneemin: "all",
      anneemax: "all",
      kilmin: 0,
      kilmax: 0,
      cylindremin: "",
      cylindremax: "",
      carb: "all",
      vitesse: "all",
      favoriteCars: [],
      finalArray:[],
      url: "",
      nommarque: "",
      type: "",
      ids:[],

    };
    this.onChangeText = this.onChangeText.bind(this);
    this._handleProductDetailClick = this._handleProductDetailClick.bind(this);
    this.props.navigation.addListener('willFocus', () => {

      console.log("caaat" + this.state.categorieChoisi)


      //this.getblogs(id);
    })
  }
  componentWillUnmount() {
    //this._isMounted = false;
    this.signal.cancel('Api is being canceled');

  }
  componentDidMount() {
    this.getCars(this.page);

    
  }


  componentWillMount() {
    AsyncStorage.multiGet([
      "ids",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("idssssss mes annonces" + data[0][1]);

      this.setState({
        ids: JSON.parse(data[0][1]),

      });
    })
    const { navigation } = this.props;
    this.state.categorieChoisi = navigation.getParam('cat', null);
    this.state.titre = navigation.getParam('titre', null);
    this.state.marqueChoisi = navigation.getParam('marque', null);
    this.state.model = navigation.getParam('model', null);
    this.state.prixMin = navigation.getParam('prixMin', null);
    this.state.prixMax = navigation.getParam('prixMax', null);
    this.state.anneemin = navigation.getParam('anneeMin', null);
    this.state.anneemax = navigation.getParam('anneeMax', null);
    this.state.kilmin = navigation.getParam('kmmin', 0);
    this.state.kilmax = navigation.getParam('kmmax', null);
    this.state.cylindremin = navigation.getParam('cylindremin', null);
    this.state.cylindremax = navigation.getParam('cylindremax', null);
    this.state.position = navigation.getParam('pos', null);
    this.state.carb = navigation.getParam('carbu', null);
    this.state.vitesse = navigation.getParam('vitesse', null);
    this.state.nommarque = navigation.getParam('nomarque', null)
  


    console.log("kiiiiiiiiiiim" + this.state.nommarque)




    AsyncStorage.multiGet([
      "header",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("header" + data[0][1]);

      this.setState({
        headr: data[0][1],

      });
    })
    AsyncStorage.multiGet([
      "ArrivedFrom",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("dta" + data[0][1]);

      this.setState({
        ecran: data[0][1],

      });
    })
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.state.ecran == "Menu") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (this.state.ecran = "AnnonceDetails") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      return true;

    });

    console.log("teeest" + this.state.ecran)
  }

  _backPress(backFrom) {
    const { screenLink } = this.state;

    if (screenLink == "ECommerceMenu") {
      if (backFrom == "fromHardwareBack") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (backFrom == "fromLeftIcon") {
        this.props.navigation.openDrawer();
      }
    }
    if (screenLink == "ECommerceMyBag") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (screenLink == "ECommerceWishList") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (screenLink == "ECommerceMain") {
      this.props.navigation.navigate("ECommerceMenu");
    }
  }

  _handleBagNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "ECommerceCategoryProduct"]]);
    this.props.navigation.navigate("ECommerceMyBag");
  }

  _handleWishListNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate("ECommerceWishList");
  }

  onChangeText(text) {
    this.setState({ user_category_id: text });
  }

  onCheckBoxPress(id) {
    this.setState({
      selectedLots: id
    });
  }

  _handleDropDownClick(size) {
    this.setState({ selectedSize: size });
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _openDropDown(id) {
    this.onCheckBoxPress(id);
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _handleProductDetailClick() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate("AnnonceDetails", {
      screen: "Annonces",

    }
    )

  }

  _handleLEFT(screenLink) {
    if (screenLink == "ECommerceMenu") {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Ionicons
            name="ios-menu"
            size={30}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this._backPress("fromLeftIcon")}
        >
          <FontAwesome
            name={I18nManager.isRTL ? "angle-right" : "angle-left"}
            size={35}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    }
  }

 /* getAsync = async () => {
    const data = await AsyncStorage.multiGet(["ids"]);
    const array = JSON.parse(data[0][1]);
    console.log("arraaaaaaaaaay" + JSON.stringify(array))
    return Promise.resolve(array);
  }*/

  ajouterToFavoris(id){
   
    console.log("thi*************" + id)
    if(this.state.ids!=null){
    for(var i =0 ; i<this.state.ids.length ; i++){
      this.state.array.push({id:this.state.ids[i].id})
    }
  }
    this.state.array.push({id:id})
    AsyncStorage.multiSet([
      ['ids', JSON.stringify(this.state.array)],
    ]);
  
/*
    console.log("*********id********")
    console.log(id);
    AsyncStorage.multiGet([
      "ids",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("ids" + data[0][1]);

      this.setState({
        ids: data[0][1],

      });
    })
     
     
        this.state.array.push({id:id})
        if(array.length>0){
          for(var i =0 ; i<this.state.ids.length ; i++){
            this.state.array.push({id:this.state.ids[i].id})
          }
            }
      AsyncStorage.multiSet([
        ['ids', JSON.stringify(this.state.array)],
      ]);
    
  
  console.log("this.state.Array"+ JSON.stringify(this.state.array))
 */

  }
 
  _handleLike(item, id, like, index) {

    this.state.favoriteCars.push(item);

    var i;
    for (i = 0; i < this.state.cars.length; i++) {
      if (this.state.cars[i].id == id) {

        this.state.cars[i].isLike = false;


      }
    }
    this.state.cars.splice(index, 1);

    this.setState({
      cars: this.state.cars
    });
    AsyncStorage.multiSet([["cars", JSON.stringify(this.state.cars)]]);
    console.log("like" + JSON.stringify(this.state.cars))

  }
 
  buildUrl() {
    if (this.state.categorieChoisi == "" || this.state.categorieChoisi == null) this.state.categorieChoisi = "all";
    if (this.state.titre == "" || this.state.titre == null) this.state.titre = "all";
    if (this.state.marqueChoisi == "" || this.state.marqueChoisi == null) this.state.marqueChoisi = "all";
    if (this.state.model == "" || this.state.model == null) this.state.model = "all";
    if (this.state.prixMin == "" || this.state.prixMin == null) this.state.prixMin = "all";
    if (this.state.prixMin == 0 && this.state.prixMax == 0) { this.state.prixMin = "all"; this.state.prixMax = "all"; }
    if (this.state.prixMax == "" || this.state.prixMax == null) this.state.prixMax = "all";
    if (this.state.anneemin == "" || this.state.anneemin == null) this.state.anneemin = "all";
    if (this.state.anneemax == "" || this.state.anneemax == null) this.state.anneemax = "all";
    if (this.state.kilmin == null) this.state.kilmin = "all";
    //if (this.state.kilmin == 0 && this.state.kilmax ==0) {this.state.kilmin = "all";this.state.kilmax="all";}
    if (this.state.kilmax == "" || this.state.kilmax == null) this.state.kilmax = "all";
    if (this.state.carb == "" || this.state.carb == null) this.state.carb = "all";
    if (this.state.vitesse == "" || this.state.vitesse == null) this.state.vitesse = "all";
    if (this.state.cylindremin == "" || this.state.cylindremin == null) this.state.cylindremin = "all";
    if (this.state.cylindremax == "" || this.state.cylindremax == null) this.state.cylindremax = "all";
    if (this.state.position == "" || this.state.position == "Toutes les positions" || this.state.position == null) this.state.position = "all";


    this.state.url =
      Config.SERVER_URL + '/api/v1/annonces/' + this.page + '/' + this.state.categorieChoisi + '/all' + '/all'
      + '/' + this.state.marqueChoisi + '/' + this.state.model + '/' + this.state.anneemin + '/' + this.state.prixMin + '/' +
      this.state.prixMax + '/' + this.state.carb + '/' + this.state.vitesse + '/' + this.state.cylindremin + '/' + this.state.cylindremax + '/' + this.state.kilmin + '/' +
      this.state.kilmax + '/' + this.state.titre + '/' + this.state.position

    console.log("cate " + this.state.categorieChoisi + "  titre " + this.state.titre + "  marque " + this.state.marqueChoisi + "  model " + this.state.model +
      "  prixMin " + this.state.prixMin + "  prixMax " + this.state.prixMax + " anne min  " + this.state.anneemin + " annee max " + this.state.anneemax
      + "Kmmin " + this.state.kilmin + " kilimetra max" + this.state.kilmax + " position " + this.state.position +
      " cylindre min " + this.state.cylindremin + " cylindre max" + this.state.cylindremax + "    carb " + this.state.carb + " vitesse" + this.state.vitesse)

    console.log("dkehhehehel" + this.state.url)
  }
  getCars(page) {
    this.buildUrl();
    this.setState({ loading: true });
    axios.get(this.state.url, {
      cancelToken: this.signal.token,
    }
    ).then((res) => {
      this.setState({

        voitures: res.data.annonces,
        loading: false,


      });


      for (var i = 0; i < this.state.voitures.length; i++) {
        if (this.state.voitures[i].principalPhoto.includes("https")) {
          this.state.voiture.push({
            id: this.state.voitures[i].id,
            productTitle: this.state.voitures[i].titre,
            productImage: this.state.voitures[i].principalPhoto,
            price: this.state.voitures[i].prixunqiue,
            boite: this.state.voitures[i].boite,
            annee: this.state.voitures[i].annee,
            kilometrage: this.state.voitures[i].kilometrage,
            cylindre: this.state.voitures[i].cylendree

          })
        } else {
          this.state.voiture.push({
            id: this.state.voitures[i].id,
            productTitle: this.state.voitures[i].titre,
            productImage: Config.SERVER_URL + this.state.voitures[i].principalPhoto,
            price: this.state.voitures[i].prixunqiue,
            boite: this.state.voitures[i].boite,
            annee: this.state.voitures[i].annee,
            kilometrage: this.state.voitures[i].kilometrage,
            cylindre: this.state.voitures[i].cylendree

          })
        }
      }

      this.setState({
        type: res.data.type
      })







    }).catch((err) => {
      if (axios.isCancel(err)) {
        console.log('Error: ', err.message); // => prints: Api is being canceled
      } else {
        this.setState({ loading: false });
      }
    });

  }
  onRefresh() {
    this.buildUrl();
    this.setState({ isRefreshing: true })

    axios.get(this.state.url, {
      cancelToken: this.signal.token,
    }
    ).then((res) => {
      this.setState({

        voitures: res.data.annonces,

        loading: false,

      });



      for (var i = 0; i < this.state.voitures.length; i++) {
        if (this.state.voitures[i].principalPhoto.includes("https")) {
          console.log("yaaaaaaaaaaaaaaaaaaaaas")
          this.state.voiture.push({
            id: this.state.voitures[i].id,
            productTitle: this.state.voitures[i].titre,
            productImage: this.state.voitures[i].principalPhoto,
            price: this.state.voitures[i].prixunique,
            boite: this.state.voitures[i].boite,
            annee: this.state.voitures[i].annee,
            kilometrage: this.state.voitures[i].kilometrage,

          })
        }
        else {
          this.state.voiture.push({
            id: this.state.voitures[i].id,
            productTitle: this.state.voitures[i].titre,
            productImage: Config.SERVER_URL + this.state.voitures[i].principalPhoto,
            price: this.state.voitures[i].prixunique,
            boite: this.state.voitures[i].boite,
            annee: this.state.voitures[i].annee,
            kilometrage: this.state.voitures[i].kilometrage,

          })
        }
      }
      console.log("voitures" + JSON.stringify(this.state.voiture))
      this.setState({ isRefreshing: false })






    }).catch((err) => {
      if (axios.isCancel(err)) {
        console.log('Error: ', err.message); // => prints: Api is being canceled
      } else {
        this.setState({ loading: false });
      }
    });

    this.setState({ loading: false })
  }

  renderSeparator = () => {
    return (
      <View
        style={{

        }}
      />
    );
  };
  handleLoadMore = () => {

    this.page = this.page + 1; // increase page by 1
    // method for API call 
    console.log("dkhel 622")

    this.getCars(this.page);



  };

  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };

  navigateToFiltre() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    if (this.state.categorieChoisi == "all") this.state.categorieChoisi = "";
    if (this.state.titre == "all") this.state.titre = "";
    if (this.state.marqueChoisi == "all") this.state.marqueChoisi = "";
    if (this.state.model == "all") this.state.model = "";
    if (this.state.prixMin == "all") this.state.prixMin = "";
    //if (this.state.prixMin == 0 && this.state.prixMax == 0) { this.state.prixMin = "all"; this.state.prixMax = "all"; }
    if (this.state.prixMax == "all") this.state.prixMax = "";
    if (this.state.anneemin == "all") this.state.anneemin = "";
    if (this.state.anneemax == "all") this.state.anneemax = "";
    //if (this.state.kilmin == "") this.state.kilmin = "all";
    //if (this.state.kilmin == 0 && this.state.kilmax ==0) {this.state.kilmin = "all";this.state.kilmax="all";}
    //if (this.state.kilmax == "" || this.state.kilmax == null) this.state.kilmax = "all";
    if (this.state.carb == "all") this.state.carb = "";
    if (this.state.vitesse == "all") this.state.vitesse = "";
    // if (this.state.cylindremin == "all" ) this.state.cylindremin = "all";
    //if (this.state.cylindremax == "all" || this.state.cylindremax == null) this.state.cylindremax = "all";
    if (this.state.position == "all") this.state.position = "";
    console.log("annonce nom marque " + this.state.model);
    if (this.state.nommarque == "all" || this.state.nommarque == null) this.state.nommarque = "";
    console.log("annonce nom marque " + this.state.model)

    this.props.navigation.navigate(
      "Filter",
      {
        cat: this.state.categorieChoisi,
        tite: this.state.titre,
        marque: this.state.marqueChoisi,
        model: this.state.model,
        prixMin: this.state.prixMin,
        prixMax: this.state.prixMax,
        anneeMin: this.state.anneemin,
        kilmin: this.state.kilmin,
        kilmax: this.state.kilmax,
        cymin: this.state.cylindremin,
        cymax: this.state.cylindremax,
        pos: this.state.position,
        carb: this.state.carb,
        vitesse: this.state.vitesse,
        marqua: this.state.marqueChoisi,
      }
    );

  }
  _handleDisLike(item, id, like, index) {
    this.getCars()
    this.state.cars.push(item);
    let tmp = this.state.cars;
    if (tmp.includes(id)) {
      tmp.splice(tmp.indexOf(id), 1);
    }

    var newData = this.state.cars.slice(); //copy array
    newData.splice(index, 1); //remove element
    for (var i = 0; i < newData.length; i++) {
      newData[i].index = i;
    }
    // this.setState({datas: newData});

    this.setState({ cars: newData, carsId: tmp });

    AsyncStorage.multiSet([[
      "cars",
      JSON.stringify(this.state.cars)
    ]]);
    for (i = 0; i < this.state.cars.length; i++) {
      if (this.state.cars[i].id == id) {

        this.state.cars[i].isLike = true;


      }
    }
    this.setState({
      cars: this.state.cars
    });

  }
  renderItem = ({ item, index }) => {


    return (

      <View style={styles.mainListRenderRow}>
        {this.state.type == "voiture" ? (
          <View style={styles.row} key={index}>
            <TouchableOpacity
              style={styles.rowBg}
              onPress={() => {
                this.props.navigation.navigate("AnnonceDetails", {
                  idAnnonce: item.id,
                  screenName: 'annonce'
                });
              }}
            >
              <CachedImage
                source={{ uri: item.productImage }}
                style={styles.productImage}
              >
               
                  <TouchableOpacity
                    onPress={() => this.ajouterToFavoris(item.id)}
                  >
                    <FontAwesome
                      name="heart"
                      size={18}
                      color={"white"}
                      style={styles.heartListIcon}
                    />
                  </TouchableOpacity>
               
              </CachedImage>

              <View style={styles.productDetailBg}>
                <Text style={styles.productTitleTxt}>
                  {item.productTitle}
                </Text>

                <View
                  style={[
                    styles.detailFieldRow,
                    {
                      paddingTop: Metrics.WIDTH * 0.01,
                      paddingBottom: Metrics.WIDTH * 0.01
                    }
                  ]}
                >
                  <Text style={styles.priceTxt}>{item.price}€</Text>
                </View>




                <View

                >


                  <View style={{ flexDirection: 'column', marginTop: Metrics.HEIGHT * 0.03 }}>
                    <View style={{ flexDirection: 'row', marginLeft: "2%" }}>
                      <EvilIcons
                        name="gear"
                        size={25}
                        color={"#000000"}


                      />
                      <Text style={{ marginLeft: "3%" }}>{item.boite}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: "2%" }}>
                      <Entypo
                        name="gauge"
                        size={25}
                        color={"#000000"}


                      />
                      <Text style={{ marginLeft: "3%" }}>{item.kilometrage} km</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: "2%" }}>
                      <EvilIcons
                        name="calendar"
                        size={25}
                        color={"#000000"}

                      />
                      <Text style={{ marginLeft: "3%" }}>{item.annee}</Text>
                    </View>
                  </View>
                  <View>






                  </View>
                </View>


              </View>

            </TouchableOpacity>

          </View>
        ) : this.state.type == "moto" ? (
          <View style={styles.row} key={index}>
            <TouchableOpacity
              style={styles.rowBg}
              onPress={() => {
                this.props.navigation.navigate("AnnonceDetails", {
                  idAnnonce: item.id
                });
              }}
            >
              <CachedImage
                source={{ uri: item.productImage }}
                style={styles.productImage}
              >
                {item.isLike == true ? (
                  <TouchableOpacity
                    onPress={() => this._handleLike(item, item.id, item.like, index)}
                  >
                    <FontAwesome
                      name="heart"
                      size={18}
                      color={"red"}
                      style={styles.heartListIcon}
                    />
                  </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                      onPress={() => this._handleDisLike(item, item.id, item.like, index)}
                    >
                      <FontAwesome
                        name="heart"
                        size={18}
                        color={"#cecece"}
                        style={styles.heartListIcon}
                      />
                    </TouchableOpacity>
                  )}
              </CachedImage>

              <View style={styles.productDetailBg}>
                <Text style={styles.productTitleTxt}>
                  {item.productTitle}
                </Text>

                <View
                  style={[
                    styles.detailFieldRow,
                    {
                      paddingTop: Metrics.WIDTH * 0.01,
                      paddingBottom: Metrics.WIDTH * 0.01
                    }
                  ]}
                >
                  <Text style={styles.priceTxt}>{item.price}€</Text>
                </View>




                <View

                >


                  <View style={{ flexDirection: 'column', marginTop: Metrics.HEIGHT * 0.03 }}>
                    <View style={{ flexDirection: 'row', marginLeft: "2%" }}>
                      <EvilIcons
                        name="gear"
                        size={25}
                        color={"#000000"}


                      />
                      <Text style={{ marginLeft: "3%" }}>{item.cylindre}CC</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: "2%" }}>
                      <Entypo
                        name="gauge"
                        size={25}
                        color={"#000000"}


                      />
                      <Text style={{ marginLeft: "3%" }}>{item.kilometrage} km</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: "2%" }}>
                      <EvilIcons
                        name="calendar"
                        size={25}
                        color={"#000000"}

                      />
                      <Text style={{ marginLeft: "3%" }}>{item.annee}</Text>
                    </View>
                  </View>
                  <View>






                  </View>
                </View>


              </View>

            </TouchableOpacity>

          </View>
        ) : (
              <View style={styles.row} key={index}>
                <TouchableOpacity
                  style={styles.rowBg}
                  onPress={() => {
                    this.props.navigation.navigate("AnnonceDetails", {
                      idAnnonce: item.id
                    });
                  }}
                >
                  <CachedImage
                    source={{ uri: item.productImage }}
                    style={styles.productImage}
                  >
                    {item.isLike == true ? (
                      <TouchableOpacity
                        onPress={() => this._handleLike(item, item.id, item.like, index)}
                      >
                        <FontAwesome
                          name="heart"
                          size={18}
                          color={"red"}
                          style={styles.heartListIcon}
                        />
                      </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                          onPress={() => this._handleDisLike(item, item.id, item.like, index)}
                        >
                          <FontAwesome
                            name="heart"
                            size={18}
                            color={"#cecece"}
                            style={styles.heartListIcon}
                          />
                        </TouchableOpacity>
                      )}
                  </CachedImage>

                  <View style={styles.productDetailBgBateau}>
                    <Text style={styles.productTitleTxt}>
                      {item.productTitle}
                    </Text>

                    <View
                      style={[
                        styles.detailFieldRow,
                        {
                          paddingTop: Metrics.WIDTH * 0.01,
                          paddingBottom: Metrics.WIDTH * 0.01
                        }
                      ]}
                    >
                      <Text style={styles.priceTxt}>{item.price}€</Text>
                    </View>



                  </View>

                </TouchableOpacity>

              </View>

            )}
      </View>

    );
  };
  
  render() {
    const { screenLink } = this.state;
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#0e1130", true);
      StatusBar.setTranslucent(true);
    }
    if (this.state.voiture == "") {
      return (
        <View>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Body style={styles.body}>
              <Text style={styles.headerTitle}></Text>
            </Body>

            <Right style={styles.right}>


              <TouchableOpacity>
                <MaterialIcons name="more-vert" color="#FFFFFF" size={25} />
              </TouchableOpacity>
            </Right>
          </Header>



          <View style={styles.imageOverlay}>
            <Spinner color="black" />
          </View>

        </View>
      );
    }
    else {


      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Body style={styles.body}>
              <Text style={styles.headerTitle}></Text>
            </Body>

            <Right style={styles.right}>
              <TouchableOpacity
                onPress={() => this.navigateToFiltre()}

              >
                <MaterialIcons
                  name="search"
                  color="#FFFFFF"
                  size={25}
                  style={{ marginRight: 10 }}
                />
              </TouchableOpacity>

              <TouchableOpacity>
                <MaterialIcons name="more-vert" color="#FFFFFF" size={25} />
              </TouchableOpacity>
            </Right>
          </Header>
        

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >



                <View style={{ backgroundColor: "#e9e9e9" }}>

                  {this.state.voiture.length > 4 ? (
                    <FlatList
                      data={this.state.voiture}
                      renderItem={this.renderItem}

                      keyExtractor={(item, index) => item.id.toString()}
                      ItemSeparatorComponent={this.renderSeparator}
                      ListFooterComponent={this.renderFooter.bind(this)}
                      refreshControl={
                        <RefreshControl
                          refreshing={this.state.isRefreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />
                      }
                      onEndReachedThreshold={0.1}
                      onEndReached={this.handleLoadMore.bind(this)}

                      ListEmptyComponent={
                        <View style={{ justifyContent: "center" }}>
                          <Text style={{ alignSelf: "center" }}>
                            No Data Found.
                              </Text>
                        </View>
                      }

                    />
                  ) : (
                      <FlatList
                        data={this.state.voiture}
                        renderItem={this.renderItem}

                        keyExtractor={(item, index) => item.id.toString()}
                        ItemSeparatorComponent={this.renderSeparator}

                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        onEndReachedThreshold={0.1}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>
                              No Data Found.
                              </Text>
                          </View>
                        }

                      />
                    )
                  }



                </View>


              </View>
            </View>
            
       
       

      );
    }
  }
}
export default withNavigation(CategoryProduct);