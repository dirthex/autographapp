import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	StatusBar,
	TouchableOpacity,
	Platform,
	BackHandler,
	Linking,
	I18nManager,
	AsyncStorage,
	ScrollView,
	Dimensions,

} from 'react-native';
import Entypo from "react-native-vector-icons/Entypo";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {
	Content,
	Container,
	Right,
	Header,
	Left,
	Body,
	Title,
	Spinner
} from 'native-base';
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import axios from "axios";
import Config from "../Config/config"
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import styles from './styles';
import { Images, Fonts, Metrics, Colors } from '../../../Themes/';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import ImagePicker from 'react-native-image-crop-picker';
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import { SocialIcon } from 'react-native-elements'
export default class MyInformation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			token: "",
			id: "",
			email: "",
			nom: "",
			tel: "",
			avatar: "",
			type: "",
			siren: "",
			siret: "",
			type: "",
			adresse: "",
			longitude: "",
			latitudee: "",
			loading: true,
			description: "",
			photo: ""
		};
	}
	takePic = () => {
		const config = {
			headers: {
				"Content-Type": "multipart/form-data; charset=utf-8;"
			}
		};
		const formData = new FormData();
		ImagePicker.openPicker({
			width: 200,
			height: 200, compressImageMaxHeight: 400,
			compressImageMaxWidth: 400, cropping: true, multiple: false
		})
			.then(response => {

				console.log("responseimage-------" + JSON.stringify(response))
				this.setState({ ImageSource: response })
				console.log("responseimagearray" + this.state.ImageSource)

				let image = {
					uri: response.path,
					// width: item.width,
					// height: item.height,
				}
				console.log("imagpath==========" + JSON.stringify(image))
				//tempArray.push(image)
				this.setState({ photo: image })
				// console.log('savedimageuri====='+item.path);

				console.log("imagpat            h==========" + JSON.stringify(this.state.photo))
				formData.append('image_param',
					{ uri: this.state.photo.uri, name: 'principal', type: 'image/jpg' });
				console.log("toooooke,******" + this.state.token)
				console.log(this.state.id)
				const headers = {
					'Authorization': 'Bearer ' + this.state.token
				}
				axios.post(Config.SERVER_URL + '/api/v1/profile/updateImage/' + this.state.id, formData, { headers }).then(
					response => {
						this.setState({ avatar: this.state.photo.uri })
						console.log({ response });
					},
					error => {
						console.log({ error });
					}
				)
			})




	}

	onChangePasswordClick() {
		this.props.navigation.navigate('ECommerceChangePassword');
	}

	onBackClick() {
		this.props.navigation.navigate('ECommerceMyAccount');
	}
	getToken = async () => {
		const userToken = await AsyncStorage.getItem('token');
		const token = userToken;
		this.state.token = userToken;
		return Promise.resolve(token);
	}
	componentWillMount() {
		var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('MonCompte');
			return true;
		});
	}
	componentDidMount() {

		this.getToken().then(token => {
			this.getInfo(token);
		});
	}

	_handleBagNavigation() {
		AsyncStorage.multiSet([
			['ArrivedFrom', "ECommerceMyInformation"],
		]);
		this.props.navigation.navigate("ECommerceMyBag");
	}

	_handleWishListNavigation() {
		AsyncStorage.multiSet([
			['ArrivedForWishList', "ECommerceMyInformation"],
		]);
		this.props.navigation.navigate("ECommerceWishList");
	}

	getInfo(token) {



		const headers = {
			'Authorization': 'Bearer ' + token
		};
		console.log("tooooooooooke" + token)
		axios.get(Config.SERVER_URL + '/api/v1/profile/detail', {
			headers
		}
		).then((res) => {
			console.log({ res })
			this.setState({

				id: res.data.id,
				nom: res.data.nom,
				email: res.data.email,
				tel: res.data.tel,
				adresse: res.data.address,
				avatar: Config.SERVER_URL + res.data.avatar,
				type: res.data.type,

			});

			if (res.data.type == 2) {
				this.setState({
					siren: res.data.related_concess.siren,
					siret: res.data.related_concess.siret,
					longitude: res.data.related_concess.long,
					adresse: res.data.related_concess.address,
					latitudee: res.data.related_concess.lat,
					description: res.data.related_concess.description
				})
			}




			this.setState({ loading: false })





		}).catch((error) => {
			this.setState({
				error: 'Error retrieving data',
				loading: false
			});
		});


	}
	render() {
		StatusBar.setBarStyle('light-content', true);
		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor('#0e1130', true);
			StatusBar.setTranslucent(true);
		}
		if (this.state.loading == true) {
			return (
				<Container style={styles.container}>

					<Header style={styles.HeaderBg} transparent>
						<Left style={styles.left}>
							<TouchableOpacity
								onPress={() => this.props.navigation.openDrawer()}
							>
								<Entypo name="menu" color="#FFFFFF" size={30} />
							</TouchableOpacity>
						</Left>

						<Right style={styles.right}>
							<SocialIcon
								type='facebook'
								iconSize={15}
								style={{ height: 30, width: 30 }}
								onPress={() => { Linking.openURL(Config.facebook) }}
							/>
							<SocialIcon
								type='instagram'
								iconSize={15}
								style={{ height: 30, width: 30 }}
								onPress={() => { Linking.openURL(Config.instagram) }}
							/>
							<SocialIcon
								type='youtube'
								iconSize={15}
								style={{ height: 30, width: 30 }}
								onPress={() => { Linking.openURL(Config.youtube) }}
							/>
						</Right>
					</Header>
					<View style={styles.imageOverlay}>
						<Spinner color="black" />
					</View>

				</Container>


			);
		}
		else {
			return (
				<Container style={styles.container}>

					<Header style={styles.HeaderBg} transparent>
						<Left style={styles.left}>
							<TouchableOpacity
								onPress={() => this.props.navigation.openDrawer()}
							>
								<Entypo name="menu" color="#FFFFFF" size={30} />
							</TouchableOpacity>
						</Left>

						<Right style={styles.right}>
							<SocialIcon
								type='facebook'
								iconSize={15}
								style={{ height: 30, width: 30 }}
								onPress={() => { Linking.openURL(Config.facebook) }}
							/>
							<SocialIcon
								type='instagram'
								iconSize={15}
								style={{ height: 30, width: 30 }}
								onPress={() => { Linking.openURL(Config.instagram) }}
							/>
							<SocialIcon
								type='youtube'
								iconSize={15}
								style={{ height: 30, width: 30 }}
								onPress={() => { Linking.openURL(Config.youtube) }}
							/>
						</Right>
					</Header>




					<View style={styles.mainView}>
						<KeyboardAwareScrollView
							resetScrollToCoords={{ x: 0, y: 0 }}
							scrollEnabled={true}
							enableAutomaticScroll={true}
							enableAutoAutomaticScrol="true"
							enableOnAndroid={true}
							extraHeight={80}
							extraScrollHeight={80}
						>
							<View style={styles.profileHeaderMain}>
								<View style={styles.profileImageSec}>


									<TouchableOpacity

										onPress={() => this.takePic()}>
										<Image source={{ uri: this.state.avatar }} style={styles.profileImage} />

									</TouchableOpacity>

								</View>
								<Text style={styles.name}>{this.state.nom}</Text>
							</View>



							<View style={styles.mainRow}>
								<Text style={styles.labelText}>Nom </Text>
								<Text style={[styles.infoText, { color: '#0e1130' }]}>{this.state.nom}</Text>
							</View>

							<View style={styles.dividerHorizontal} />



							<View style={styles.mainRow}>
								<Text style={styles.labelText}>Email</Text>
								<Text style={[styles.infoText, { color: '#e1e1e1' }]}>
									{this.state.email}
								</Text>
							</View>

							<View style={styles.dividerHorizontal} />

							<View style={styles.mainRow}>
								<Text style={styles.labelText}>Tele</Text>
								<Text style={[styles.infoText, { color: '#0e1130' }]}>
									{this.state.tel}
								</Text>
							</View>

							<View style={styles.dividerHorizontal} />

							<View style={styles.mainRow}>
								<Text style={styles.labelText}>Adresse</Text>
								<Text style={[styles.infoText, { color: '#0e1130' }]}>{this.state.adresse}</Text>
							</View>

							<View style={styles.dividerHorizontal} />
							{this.state.type == 2 ? (
								<View>
									<View style={styles.mainRow}>
										<Text style={styles.labelText}>Siren</Text>
										<Text style={[styles.infoText, { color: '#0e1130' }]}>{this.state.siren}</Text>
									</View>
									<View style={styles.dividerHorizontal} />
									<View style={styles.mainRow}>
										<Text style={styles.labelText}>Siret</Text>
										<Text style={[styles.infoText, { color: '#0e1130' }]}>{this.state.siret}</Text>
									</View>
									<View style={styles.dividerHorizontal} />
									<View style={styles.mainRow}>
										<Text style={styles.labelText}>Description</Text>
										<Text style={[styles.infoText, { color: '#0e1130' }]}>{this.state.description}</Text>
									</View>

									<MapView
										style={styles.MapView}
										provider={PROVIDER_GOOGLE}
										region={{
											latitude: parseFloat(this.state.latitudee),
											longitude: parseFloat(this.state.longitude),
											latitudeDelta: LATITUDE_DELTA,
											longitudeDelta: LONGITUDE_DELTA
										}}
										initialRegion={{
											latitude: parseFloat(this.state.latitudee),
											longitude: parseFloat(this.state.longitude),
											latitudeDelta: LATITUDE_DELTA,
											longitudeDelta: LONGITUDE_DELTA
										}}
									>

										<MapView.Marker
											coordinate={{
												latitude: parseFloat(this.state.latitudee),
												longitude: parseFloat(this.state.longitude),
												latitudeDelta: 0.015,
												longitudeDelta: 0.0121
											}}
										>
											<Entypo name="location-pin" size={35} color="#f05522" />
										</MapView.Marker>


									</MapView>

								</View>
							) : null
							}




						</KeyboardAwareScrollView>
						<View style={styles.editInfoMainView}>
							<View style={styles.editDivider} />
							<TouchableOpacity
								style={styles.editInfoView}
								onPress={() => this.props.navigation.navigate("ECommerceEditInformation", {
									id: this.state.id,
									nom: this.state.nom,
									email: this.state.email,
									tel: this.state.tel,
									adresse: this.state.adresse,
									description: this.state.description,
									type: this.state.type,
									token: this.state.token,
									longitude: this.state.longitude,
									latitude: this.state.latitudee,
									siren: this.state.siren,
									siret: this.state.siret
								})}>
								<Text style={styles.editInfoText}>Modifier les informations</Text>
							</TouchableOpacity>
						</View>
					</View>

				</Container>
			);
		}
	}
}
