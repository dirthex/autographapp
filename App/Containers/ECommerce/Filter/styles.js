
import { Platform, StyleSheet, Dimensions, I18nManager } from 'react-native';
import { Images, Metrics,Fonts,Colors } from "../../../Themes/";

const styles = StyleSheet.create({

    header: {
        backgroundColor: "#0e1130",
        height: Metrics.HEIGHT * 0.1,
        borderBottomWidth: 0,
        ...Platform.select({
            ios: {
                paddingTop: (Metrics.HEIGHT * 0.02),
            },
            android: {
                paddingTop: Metrics.WIDTH * 0.04
            }
        }),
        elevation: 0,
        paddingLeft: (Metrics.WIDTH * 0.05),
        paddingRight: (Metrics.WIDTH * 0.05),
    },

  headerBG: {
    height: (Metrics.HEIGHT * 0.1),
    width: Metrics.WIDTH,
  },
  textCon: {
    width: 380,
    flexDirection: 'row',
    justifyContent: 'space-between'
},
colorGrey: {
    color: '#d3d3d3'
},
colorYellow: {
    color: '#ff6347'
},
bottomView: {
  width: Metrics.WIDTH,
  alignItems: 'center',
  justifyContent: 'center',
  height: Metrics.HEIGHT * 0.094,
},
loginWithFacebookTxt: {
  color: Colors.black,
  fontSize: Fonts.moderateScale(17),
  // fontFamily: Fonts.type.sfuiDisplayLight,
  marginLeft: Metrics.WIDTH * 0.03,
},
facebookBtnBg: {
  backgroundColor: 'yellow',
  width: Metrics.WIDTH * 0.96,
  alignSelf: 'center',
  borderRadius: 5,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  paddingTop: Metrics.WIDTH * 0.025,
  paddingBottom: Metrics.WIDTH * 0.025,
  
},

    left: {
        flex: 1,
        backgroundColor: Colors.transparent,
    },

    body: {
        flex: 2,
        alignItems: 'center',
        backgroundColor: Colors.transparent
    },

    headetTxt: {
    color: Colors.snow
  },
    textTitle: {
        color: Colors.snow,
        fontSize: Fonts.moderateScale(18),
        alignSelf: 'center',
        fontFamily: Fonts.type.sfuiDisplaySemibold,
    },

    right: {
        flex: 1,
        alignItems: 'center',
    },

    bagIcon: {
        marginLeft: Metrics.WIDTH * 0.04,
        color: Colors.snow,
    },

    heartIcon: {
        color: Colors.snow,
        alignSelf: 'center'
    },

    heartBg: {
        width: Metrics.WIDTH * 0.054,
        height: Metrics.WIDTH * 0.054,
        borderRadius: Metrics.WIDTH * 0.027,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: Colors.snow,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: Metrics.WIDTH * 0.032
    },
  headerTxt: {
    color: Colors.snow
  },

  leftMsg: {
    fontFamily: Fonts.type.sfuiDisplayRegular,
    fontSize: Fonts.moderateScale(14),
    color: Colors.snow,
    marginLeft: 5,
    marginTop: 2
  },

  content: {
    width: Metrics.WIDTH,
		height: Metrics.HEIGHT * 0.5,
  },

  iconSize: {
    width: (Metrics.HEIGHT * 0.03),
    fontSize: Fonts.moderateScale(30),
    color: Colors.snow
  },

  styleGradient: {
    height: (Metrics.HEIGHT * 0.038),
    width: (Metrics.WIDTH * 0.48),
    borderRadius: (Metrics.HEIGHT * 0.02),
  },

  leftBtnRadius: {
    borderTopLeftRadius: I18nManager.isRTL ? 0 : (Metrics.HEIGHT * 0.02),
    borderBottomLeftRadius: I18nManager.isRTL ? 0 : (Metrics.HEIGHT * 0.02),
    borderTopRightRadius: I18nManager.isRTL ? (Metrics.HEIGHT * 0.02) : 0,
    borderBottomRightRadius: I18nManager.isRTL ? (Metrics.HEIGHT * 0.02) : 0,
  },


  rightBtnRadius: {
   borderTopRightRadius: I18nManager.isRTL ? 0 : (Metrics.HEIGHT * 0.02),
   borderBottomRightRadius: I18nManager.isRTL ? 0 : (Metrics.HEIGHT * 0.02),
   borderTopLeftRadius: I18nManager.isRTL ? (Metrics.HEIGHT * 0.02) : 0,
   borderBottomLeftRadius: I18nManager.isRTL ? (Metrics.HEIGHT * 0.02) : 0,
 },



  segmentTabSec: {
    height: (Metrics.HEIGHT * 0.038),
    width: (Metrics.WIDTH * 0.48),
    borderRadius: (Metrics.HEIGHT * 0.02),
    backgroundColor: Colors.transparent,
  },

  selectedSegmentTab: {
    height: (Metrics.HEIGHT * 0.038),
    width: (Metrics.WIDTH * 0.16),
    backgroundColor: Colors.transparent,
    borderColor: Colors.transparent,
    alignItems: 'center',
    justifyContent: 'center',
		overflow: 'visible'
  },

  segmentTab: {
    height: (Metrics.HEIGHT * 0.038),
    width: (Metrics.WIDTH * 0.16),
    backgroundColor: '#e6e6e6',
    borderColor: Colors.transparent,
    alignItems: 'center',
    justifyContent: 'center',
		overflow: 'hidden'
  },

  activeTab: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: Colors.snow
  },

  normalTab: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: '#666666'
  },

  listHeader: {
    width:(Metrics.WIDTH),
    height:(Metrics.HEIGHT*0.065),
    marginLeft:(Metrics.WIDTH*0.05),
    alignItems:'flex-start',
    justifyContent:'center'
  },

  listHeaderText: {
    color:'#9f9f9f',
    fontFamily:Fonts.type.sfuiDisplayRegular,
    fontSize:Fonts.moderateScale(18)
  },

  listMain: {
    backgroundColor: 'white',
    paddingLeft: (Metrics.WIDTH*0.005)
  },
  ListContentMain: {
		backgroundColor: Colors.white,
		marginLeft: Metrics.HEIGHT * 0.007,
		marginRight: Metrics.HEIGHT * 0.007,
		...Platform.select({
		  ios: {
			shadowOpacity: 0.4,
			borderWidth: 1
		  },
		  android: {
			shadowOpacity: 0.1
		  }
		}),
		shadowColor: "gray",
		shadowOffset: { width: 2, height: 2 },
		shadowRadius: 4,
		elevation: 3,
		borderColor: "#e8e8e8",
		marginBottom: Metrics.HEIGHT * 0.03,
		borderRadius: 2,
		marginTop: Metrics.HEIGHT * 0.02,
		height:"94%"
	  },
	

  listGenderMain: {
    justifyContent: 'space-between',
    height:(Metrics.HEIGHT*0.06),
    borderColor: '#e5e5e5',
    borderBottomWidth: 0.5
  },
  contentOne: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT * 0.8,
	},

  listTitle: {
    color:'#4c4c4c',
    fontFamily:Fonts.type.sfuiDisplayRegular,
    fontSize:Fonts.moderateScale(18)
  },

  listAgeMain: {
    justifyContent: 'space-between',
    height:(Metrics.HEIGHT*0.1),
    borderColor: '#e5e5e5',
    borderBottomWidth: 1
  },
  listKilo: {
    height:(Metrics.HEIGHT*0.15),
    borderColor: '#e5e5e5',
    borderBottomWidth: 1
  },

  listAgeRight: {
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  listRightText: {
    
    color:'#a5a5a5',
    fontFamily:Fonts.type.sfuiDisplayRegular,
    fontSize:Fonts.moderateScale(18)
  },

 
  bottomView: {
		width: Metrics.WIDTH,
		alignItems: 'center',
		justifyContent: 'center',
		height: Metrics.HEIGHT * 0.094,
	},

  arrowForword: {
    color:'#c7c7cc',
    paddingLeft: (Metrics.WIDTH*0.02),
    fontSize: Fonts.moderateScale(30)
  },

  markerStyle: {
    height:(Metrics.HEIGHT*0.035),
    width: (Metrics.HEIGHT*0.035),
    backgroundColor:'#f87362',
    borderWidth: 0.5,
    borderColor: '#fa6982'
  },

  containerStyle: {
    width:"100%",
    //height:5
  },

  trackStyle: {
    height: (Metrics.HEIGHT*0.005),
    borderRadius: 4
  },

  listDistanceMain: {
    justifyContent: 'space-between',
    height:(Metrics.HEIGHT*0.08),
    borderColor: Colors.transparent
  },

  listDistanceRight: {
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  arrowBack: {
    color:'#c7c7cc',
    paddingRight: (Metrics.WIDTH*0.02),
    fontSize: Fonts.moderateScale(30)
  },
  HeaderBg: {
		backgroundColor: "#23292e",
		borderBottomWidth: 1
	  },
});

export default styles;
