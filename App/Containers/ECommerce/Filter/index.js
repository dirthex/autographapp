import React, { Component } from "react";
import {
  View,
  Image,
  StatusBar,
  Platform,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  ListView,
  I18nManager,
  BackHandler, AsyncStorage,
  Modal
} from "react-native";
import FloatingLabelInput from '../FloatingLabelInput';
import {
  Container,
  Header,
  Content,
  Button,
  Left,
  Right,
  Body,
  Title,
  Icon,
  List,
  ListItem,
  Text,
  Item,
  Input,
  Segment
} from "native-base";

import LinearGradient from "react-native-linear-gradient";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { Actions } from "react-native-router-flux";
import { Images, Metrics, Fonts, Colors } from "../../../Themes/";
import styles from "./styles";
import AntDesign from "react-native-vector-icons/AntDesign";


// Screen Styles

import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Dropdown } from 'react-native-material-dropdown';
import Slider from "react-native-slider";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Config from "../Config/config";
import axios from "axios";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multiSliderValue: [this.props.min, this.props.max],
      first: this.props.min,
      second: this.props.max,
      screenLink: "",
      segment: 1,
      distance: 10,
      multiSliderKilometrage: [0, 0],
      multiSliderPrice: [0, 0],
      multiSliderAnnee: [0, 0],
      multiSliderCylindre: [0, 0],
      isSelectedKilometrage: false,
      isSelectedPrice: false,
      isSelectedAnnee: false,

      distance: 30,
      minDistance: 10,
      titre: '',
      maxDistance: 250000,
      prix: 0,
      minPrix: 0,
      maxPrix: 50000,
      annee: 2015,
      minAnnee: 1960,
      maxAnnee: 2019,
      marques: [],
      marque: [],
      modelArray: [],
      modelsArray: [],
      idmarque: '',
      loading:true,
      categorie: [
        {
          value: 'voiture',
        },
        {
          value: 'moto',
        },
        {
          value: 'bateau',
        },
      ],
      typecarburant: [
        {
          value: 'diesel',
        },
        {
          value: 'essence',
        },
        {
          value: 'gpl',
        },
        {
          value: 'electrique',
        },
        {
          value: 'hybride',
        },
      ],
      typevitesse: [
        {
          value: 'manuelle',
        },
        {
          value: 'automatique',
        },

      ],
      position: [
        {
          value: 'Toutes les positions',
        },
        {
          value: 'guadeloupe',
        },
        {
          value: 'martinique',
        },
        {
          value: 'guyane',
        },
        

      ],
      positionChoisi:"",
      categorieChoisi: "",
      titre: "",
     
      model: "",
      prixMin: "",
      prixMax: "",
      anneeCh: 0,
      kilome: "",
      carb: "",
      vitesse: "",
      clylindre: "",
      modalVisible: true,
      catvalue:"",
      titrevalue:"",
      marqueChoisivalue: "",
      modelvalue: "",
      prixMinvalue: "",
      prixMaxvalue: "",
      anneeChvalue: "",
      kilomMinvalue: "",
      kilomMaxvalue:"",
      carbvalue: "",
      vitessevalue: "",
      clylindreminvalue: "",
      cylindremaxvalue:"",
      posvalue:"",
      nommarque:""


    };
  }

  componentWillMount() {

    const { navigation } = this.props;
    this.state.categorieChoisi= navigation.getParam('cat', null);
    var md = this.state.categorieChoisi;
    this.getMarques(md);
    this.state.titre= navigation.getParam('tite', null);
    this.state.nommarque= navigation.getParam('marque', null);
    console.log("maaarque + " + this.state.nommarque)
    this.state.model= navigation.getParam('model', null);
    
    this.state.multiSliderPrice[0]= navigation.getParam('prixMin', null);
    if(this.state.multiSliderPrice[0] =="")this.state.multiSliderPrice[0]=0
    this.state.multiSliderPrice[1]= navigation.getParam('prixMax', null);
    if(this.state.multiSliderPrice[1] =="")this.state.multiSliderPrice[1]=89000
    this.state.anneeCh= navigation.getParam('anneeMin', null);
    this.state.multiSliderKilometrage[0]= navigation.getParam('kilmin', null);
    if(this.state.multiSliderKilometrage[0] =="all")this.state.multiSliderKilometrage[0]=0
    this.state.multiSliderKilometrage[1]= navigation.getParam('kilmax', null);
    if(this.state.multiSliderKilometrage[1] =="all")this.state.multiSliderKilometrage[1]=120000

    this.state.carbvalue= navigation.getParam('carb', null);
    this.state.vitessevalue= navigation.getParam('vitesse', null);
    this.state.multiSliderCylindre[0]= navigation.getParam('cymin', null);
    if(this.state.multiSliderCylindre[0]=="all")this.state.multiSliderCylindre[0]=0;
    console.log("cylindre0" +this.state.multiSliderCylindre[0])
    this.state.multiSliderCylindre[1]= navigation.getParam('cymax', null);
    if(this.state.multiSliderCylindre[1]=="all")this.state.multiSliderCylindre[1]=1000;
    console.log("cylindre0" +this.state.multiSliderCylindre[1])
    this.state.positionChoisi = navigation.getParam('pos', null);
    this.state.idmarque = navigation.getParam('marqua',null)
    var n = this.state.idmarque;
    this.getMarqueById(n);
    this.getModels(n);
   

console.log("this.state.nommarque" + "1" +this.state.nommarque + "2"+this.state.model)
 

    AsyncStorage.multiGet([
      "ArrivedFrom",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("dta" + data[0][1]);

      this.setState({
        ecran: data[0][1],

      });
    })
    BackHandler.addEventListener('hardwareBackPress', () => {

      if (this.state.ecran == "Annonce") {
        this.props.navigation.navigate("Annonces");
      }

      return true;

    });
  }

  getValueofCategorie = (value) => {

    this.setState({ categorieChoisi: value });
    this.getMarques(value)

  }

  getMarques(value) {
    console.log("vaaalue" + value)
    this.setState({
      marques: [],
      marque: [],



    });
    axios.get(Config.SERVER_URL + '/api/v1/annonces/1/' + value
    ).then((res) => {
      this.setState({

        marques: res.data.marques,



      });


      for (var i = 0; i < this.state.marques.length; i++) {
        this.state.marque.push({
          id: this.state.marques[i].id,
          value: this.state.marques[i].nom,

        })
      }











    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
  }
  getMarqueById(id){
    
    console.log("categorie" + this.state.categorieChoisi)
   //this.setState({loading:true})
    
    axios.get(Config.SERVER_URL + '/api/v1/annonces/1/' + this.state.categorieChoisi
    ).then((res) => {
     // console.log("**************"+ JSON.stringify(res.data.marques))
      this.setState({

        marques: res.data.marques,



      });


      for (var i = 0; i < this.state.marques.length; i++) {
        if (this.state.marques[i].id == this.state.idmarque) {
          this.state.nommarque = this.state.marques[i].nom
        
        }
      }
      console.log("MAARques++++++++"+this.state.marques)
   /*   for (var i = 0; i < this.state.marques.length; i++) {
        this.state.marque.push({
          id: this.state.marques[i].id,
          value: this.state.marques[i].nom,

        })
      }*/
    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
    
  
    
 
    console.log("++++++++++++++++++++++++" + this.state.nommarque)
  }

  getValueOfMarque = (value) => {
    for (var i = 0; i < this.state.marque.length; i++) {
      if (this.state.marque[i].value == value) {
        this.state.idmarque = this.state.marque[i].id
      }
    }
    console.log("ididiidid" + this.state.idmarque)
   // this.setState({ nommarque: value })
    console.log("mammamamamam" + this.state.nommarque)
    this.getModels(this.state.idmarque);

  }
  getValueofPosition=(value) =>{
    this.setState({ positionChoisi: value })
  }
  getModels(id) {
    
    this.setState({
      modelArray: [],
      modelsArray: [],



    });
    axios.get(Config.SERVER_URL + '/api/v1/marque/' + id
    ).then((res) => {
      this.setState({

        modelsArray: res.data,



      });


      for (var i = 0; i < this.state.modelsArray.length; i++) {
        this.state.modelArray.push({
          id: this.state.modelsArray[i].id,
          value: this.state.modelsArray[i].nom,

        })
      }











    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });

  }
  getValueOfCarburant(value) {
    this.setState({ carb: value })
  }
  getValueOfVitesse(value) {
    this.setState({ vitesse: value })
  }
  getValueOfModel(value) {
    console.log("vvvaaalue" + value)
    for (var i = 0; i < this.state.marque.length; i++) {
      if (this.state.marque[i].value == value) {
        this.state.idmarque = this.state.marque[i].id
       
      }
    }
    this.setState({model:value})
    console.log("thiiiis.statettte.id" + this.state.model)
  }
  navigateToAnnonce() {
    
    if(this.state.categorieChoisi =="bateau"){
    this.props.navigation.navigate("Annonces", {
      pos:this.state.positionChoisi,
      cat: this.state.categorieChoisi,
      titre: this.state.titre,
      marque: null,
      model: null,
      prixMin: this.state.multiSliderPrice[0],
      prixMax: this.state.multiSliderPrice[1],
      anneeMin: null,
      anneeMax: null,
      kmmin :null,
      kmmax : null,
      cylindremin : null,
     clylindremax : null,
       carbu: null,
      vitesse: null,
   

    })
  }
  else if(this.state.categorieChoisi =="voiture"){
    console.log("maaaaqrye" +this.state.nommarque)
    this.props.navigation.navigate("Annonces", {
      pos:this.state.positionChoisi,
      cat: this.state.categorieChoisi,
      titre: this.state.titre,
      marque: this.state.idmarque,
      nomarque:this.state.nommarque,
      model: this.state.model,
      prixMin: this.state.multiSliderPrice[0],
      prixMax: this.state.multiSliderPrice[1],
      anneeMin: this.state.anneeCh,
    
      kmmin :this.state.multiSliderKilometrage[0],
      kmmax : this.state.multiSliderKilometrage[1],
      cylindremin : null,
      clylindremax : null,
       carbu: this.state.carb,
      vitesse: this.state.vitesse,
   

    })
  }
  else {
    this.props.navigation.navigate("Annonces", {
      pos:this.state.positionChoisi,
      cat: this.state.categorieChoisi,
      titre: this.state.titre,
      marque: this.state.idmarque,
      nomarque:this.state.nommarque,
      model: this.state.model,
      prixMin: this.state.multiSliderPrice[0],
      prixMax: this.state.multiSliderPrice[1],
      anneeMin: this.state.anneeCh,
      anneeMax: this.state.multiSliderAnnee[1],
      kmmin :this.state.multiSliderKilometrage[0],
      kmmax : this.state.multiSliderKilometrage[1],
      cylindremin : this.state.multiSliderCylindre[0],
      cylindremax : this.state.multiSliderCylindre[1],
       carbu: this.state.carb,
      vitesse: this.state.vitesse,
   

    })
  }
   this.setState({modalVisible:false})
  }
  onChangeText(text) {
    this.setState({ gender: text });
    // console.warn(this.state.gender);
  }
  multiSliderValuesChange = values => {
    this.setState({ multiSliderPrice: values });
  };
  multiSliderValuesChangeAnnee = values => {
    this.setState({ multiSliderAnnee: values });
  }
  multiSliderValuesKilometrage = values => {
    this.setState({ multiSliderKilometrage: values });
  }
  multiSliderValuesCC = values => {
    this.setState({ multiSliderCylindre: values });
  }





  _renderFooter = () => {


    // Sign Up

    return (
      <TouchableOpacity
        style={styles.facebookBtnBg}
        onPress={() => this.navigateToAnnonce()}>
        <Text style={styles.loginWithFacebookTxt}>Rechercher</Text>
      </TouchableOpacity>
    );

  };



  _renderActiveComponent = () => {
    const { segment } = this.state;

    // Login

    // SignUp

    return (
      <Content>

        <View style={styles.ListContentMain}>



        <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
            <Dropdown
              label="Position"
              data={this.state.position}
              textColor={'#959595'}
              onChangeText={value => this.getValueofPosition(value)}
              value= {this.state.positionChoisi}
            />
          </View>
          <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
            <Dropdown
              label="Categories"
              data={this.state.categorie}
              textColor={'#959595'}
              onChangeText={value => this.getValueofCategorie(value)}
              value={this.state.categorieChoisi}
            />
          </View>
          {this.state.categorieChoisi == "bateau"  ? (

            <View>
              <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                <FloatingLabelInput
                  label="Titre"
                  value={this.state.titre}
                  keyboardType="default"
                  returnKeyType="done"
                  maxLength={20}

                  selectionColor={'#0000ff'}
                  autoCapitalize="none"
                  onChangeText={titre => this.setState({ titre })}
                     
                />
              </View>
              <ListItem style={styles.listKilo}>
                <View style={{ flex: 1, flexDirection: 'column' }}>

                  <View style={{ alignSelf: 'flex-start' }}>
                    <Text style={{ color: "#ff6347", fontSize: Fonts.moderateScale(16) }}>Prix</Text>
                  </View>
                  <View>
                    <MultiSlider

                      values={this.state.multiSliderPrice}
                      sliderLength={360}
                      onValuesChange={this.multiSliderValuesChange}
                      min={0}
                      max={89000}
                      step={1}
                      allowOverlap
                      snapped
                      selectedStyle={{ backgroundColor: "#fa6982" }}
                      unselectedStyle={{ backgroundColor: "#cecece" }}
                    //markerStyle={styles.markerStyle}
                    //containerStyle={styles.containerStyle}
                    //trackStyle={styles.trackStyle}

                    />
                  </View>
                  <View style={styles.textCon}>
                    <Text style={styles.colorGrey}>{this.state.multiSliderPrice[0]} € </Text>
                    <Text style={styles.colorYellow}>

                    </Text>
                    <Text style={styles.colorGrey}>{this.state.multiSliderPrice[1]} € </Text>
                  </View>
                </View>

              </ListItem>
            </View>
          ) : this.state.categorieChoisi == "moto" ? (
            <View>
              <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                <FloatingLabelInput
                  label="Titre"
                  value={this.state.titre}
                  keyboardType="default"
                  returnKeyType="done"
                  maxLength={20}

                  selectionColor={'#0000ff'}
                  autoCapitalize="none"
                  onChangeText={titre => this.setState({ titre })}
                />
              </View>
              <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                <Dropdown
                  label="Marques"
                  data={this.state.marque}
                  textColor={'#959595'}
                 
                  value={this.state.nommarque}
                  onChangeText={value => this.getValueOfMarque(value)}
                />
              </View>
              <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                <Dropdown
                  label="Model"
                  data={this.state.modelArray}
                  textColor={'#959595'}
                  onChangeText={value => this.getValueOfModel(value)}
                  value={this.state.model}
                />
              </View>
              <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                    <FloatingLabelInput
                      label="Année Model"
                      value={this.state.anneeCh}
                      keyboardType="default"
                      returnKeyType="done"
                      maxLength={20}

                      selectionColor={'#0000ff'}
                      autoCapitalize="none"
                      onChangeText={anneeCh => this.setState({ anneeCh })}
                    />
                  </View>
              <ListItem style={styles.listKilo}>
                <View style={{ flex: 1, flexDirection: 'column' }}>

                  <View style={{ alignSelf: 'flex-start' }}>
                    <Text style={{ color: "#ff6347", fontSize: Fonts.moderateScale(16) }}>Prix</Text>
                  </View>
                  <View>
                    <MultiSlider

                      values={this.state.multiSliderPrice}
                      sliderLength={360}
                      onValuesChange={this.multiSliderValuesChange}
                      min={0}
                      max={89000}
                      step={1}
                      allowOverlap
                      snapped
                      selectedStyle={{ backgroundColor: "#fa6982" }}
                      unselectedStyle={{ backgroundColor: "#cecece" }}
                    //markerStyle={styles.markerStyle}
                    //containerStyle={styles.containerStyle}
                    //trackStyle={styles.trackStyle}

                    />
                  </View>
                  <View style={styles.textCon}>
                    <Text style={styles.colorGrey}>{this.state.multiSliderPrice[0]} € </Text>
                    <Text style={styles.colorYellow}>

                    </Text>
                    <Text style={styles.colorGrey}>{this.state.multiSliderPrice[1]} € </Text>
                  </View>
                </View>

              </ListItem>
              
              <ListItem style={styles.listKilo}>
                <View style={{ flex: 1, flexDirection: 'column' }}>

                  <View style={{ alignSelf: 'flex-start' }}>
                    <Text style={{ color: "#ff6347", fontSize: Fonts.moderateScale(16) }}>KM</Text>
                  </View>
                  <View>
                    <MultiSlider

                      values={this.state.multiSliderKilometrage}
                      sliderLength={360}
                      onValuesChange={this.multiSliderValuesKilometrage}
                      min={0}
                      max={100000}
                      step={1}
                      allowOverlap
                      snapped
                      selectedStyle={{ backgroundColor: "#fa6982" }}
                      unselectedStyle={{ backgroundColor: "#cecece" }}
                    //markerStyle={styles.markerStyle}
                    //containerStyle={styles.containerStyle}
                    //trackStyle={styles.trackStyle}

                    />
                  </View>
                  <View style={styles.textCon}>
                    <Text style={styles.colorGrey}>{this.state.multiSliderKilometrage[0]} km</Text>
                    <Text style={styles.colorYellow}>
          
                    </Text>
                    <Text style={styles.colorGrey}>{this.state.multiSliderKilometrage[1]} km</Text>
                  </View>
                </View>

              </ListItem>
              <ListItem style={styles.listKilo}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>

                      <View style={{ alignSelf: 'flex-start' }}>
                        <Text style={{ color: "#ff6347", fontSize: Fonts.moderateScale(16) }}>Cylindre</Text>
                      </View>
                      <View>
                      <MultiSlider

                          values={this.state.multiSliderCylindre}
                          sliderLength={360}
                          onValuesChange={this.multiSliderValuesCC}
                          min={50}
                          max={1000}
                          step={1}
                          allowOverlap
                          snapped
                          selectedStyle={{ backgroundColor: "#fa6982" }}
                          unselectedStyle={{ backgroundColor: "#cecece" }}
                        //markerStyle={styles.markerStyle}
                        //containerStyle={styles.containerStyle}
                        //trackStyle={styles.trackStyle}

                        />
                      </View>
                      <View style={styles.textCon}>
                        <Text style={styles.colorGrey}>{this.state.multiSliderCylindre[0]} CC</Text>
                        <Text style={styles.colorYellow}>
                          
                        </Text>
                        <Text style={styles.colorGrey}>{this.state.multiSliderCylindre[1]} CC</Text>
                      </View>
                    </View>

                  </ListItem>



              



              <View style={{ height: 10 }} />


            </View>
          ) : (
                <View>
                  <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                    <FloatingLabelInput
                      label="Titre"
                      value={this.state.titre}
                      keyboardType="default"
                      returnKeyType="done"
                      maxLength={20}

                      selectionColor={'#0000ff'}
                      autoCapitalize="none"
                      onChangeText={titre => this.setState({ titre })}
                    />
                  </View>
                  <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                    <Dropdown
                      label="Marques"
                      data={this.state.marque}
                      textColor={'#959595'}

                     value={this.state.nommarque}
                      onChangeText={value => this.getValueOfMarque(value)}
                    />
                  </View>
                  <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                    <Dropdown
                      label="Model"
                      data={this.state.modelArray}
                      textColor={'#959595'}
                      value={this.state.model}
                      onChangeText={value => this.getValueOfModel(value)}
                    />
                  </View>
                  <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                    <FloatingLabelInput
                      label="Année Model"
                      value={this.state.anneeCh}
                    
                      returnKeyType="done"
                      maxLength={20}
                      keyboardType={'numeric'}
                      selectionColor={'#0000ff'}
                      autoCapitalize="none"
                      onChangeText={anneeCh => this.setState({ anneeCh })}
                    />
                  </View>
                  <ListItem style={styles.listKilo}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>

                      <View style={{ alignSelf: 'flex-start' }}>
                        <Text style={{ color: "#ff6347", fontSize: Fonts.moderateScale(16) }}>Prix</Text>
                      </View>
                      <View>
                        <MultiSlider

                          values={this.state.multiSliderPrice}
                          sliderLength={360}
                          onValuesChange={this.multiSliderValuesChange}
                          min={0}
                          max={89000}
                          step={1}
                          allowOverlap
                          snapped
                          selectedStyle={{ backgroundColor: "#fa6982" }}
                          unselectedStyle={{ backgroundColor: "#cecece" }}
                        //markerStyle={styles.markerStyle}
                        //containerStyle={styles.containerStyle}
                        //trackStyle={styles.trackStyle}

                        />
                      </View>
                      <View style={styles.textCon}>
                        <Text style={styles.colorGrey}>{this.state.multiSliderPrice[0]} € </Text>
                        <Text style={styles.colorYellow}>

                        </Text>
                        <Text style={styles.colorGrey}>{this.state.multiSliderPrice[1]} € </Text>
                      </View>
                    </View>

                  </ListItem>
                 
                  <ListItem style={styles.listKilo}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>

                      <View style={{ alignSelf: 'flex-start' }}>
                        <Text style={{ color: "#ff6347", fontSize: Fonts.moderateScale(16) }}>Kilometrage</Text>
                      </View>
                      <View>
                      <MultiSlider

                          values={this.state.multiSliderKilometrage}
                          sliderLength={360}
                          onValuesChange={this.multiSliderValuesKilometrage}
                          min={0}
                          max={100000}
                          step={1}
                          allowOverlap
                          snapped
                          selectedStyle={{ backgroundColor: "#fa6982" }}
                          unselectedStyle={{ backgroundColor: "#cecece" }}
                        //markerStyle={styles.markerStyle}
                        //containerStyle={styles.containerStyle}
                        //trackStyle={styles.trackStyle}

                        />
                      </View>
                      <View style={styles.textCon}>
                        <Text style={styles.colorGrey}>{this.state.multiSliderKilometrage[0]} KM</Text>
                        <Text style={styles.colorYellow}>
                          
                        </Text>
                        <Text style={styles.colorGrey}>{this.state.multiSliderKilometrage[1]} KM</Text>
                      </View>
                    </View>

                  </ListItem>
                  


                  <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                <Dropdown
                  label="Carburant"
                  data={this.state.typecarburant}
                  textColor={'#959595'}
                  onChangeText={value => this.getValueOfCarburant(value)}
                  value={this.state.carbvalue}
                />
              </View>
              <View style={{ width: Metrics.WIDTH * 0.94, alignSelf: 'center' }}>
                <Dropdown
                  label="Boite à vitesse"
                  data={this.state.typevitesse}
                  textColor={'#959595'}
                  onChangeText={value => this.getValueOfVitesse(value)}
                  value={this.state.vitessevalue}
                />
              </View>




                  <View style={{ height: 10 }} />


                </View>

              )}
        </View>

      </Content>
    );

  };

  render() {
    var that = this;
    let BG_Image = {
      uri:
        'https://antiqueruby.aliansoftware.net/Images/signin/ic_back01_sone.png',
    };
    StatusBar.setBarStyle('light-content', true);

    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#0e1130', true);
      StatusBar.setTranslucent(true);
    }

    return (
     
      <Container style={styles.container}>
        <Header style={styles.HeaderBg} transparent>
          <Left style={styles.left}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Entypo name="menu" color="#FFFFFF" size={30} />
            </TouchableOpacity>
          </Left>

          <Body style={styles.body}>
            <Text style={styles.headerTitle}></Text>
          </Body>

          <Right style={styles.right}>
            <TouchableOpacity>
              <MaterialIcons
                name="search"
                color="#FFFFFF"
                size={25}
                style={{ marginRight: 10 }}
              />
            </TouchableOpacity>

            <TouchableOpacity>
              <MaterialIcons name="more-vert" color="#FFFFFF" size={25} />
            </TouchableOpacity>
          </Right>
        </Header>
  
        <KeyboardAwareScrollView>
          <View style={styles.contentOne}>




            {this._renderActiveComponent()}

          </View>

          {}

        </KeyboardAwareScrollView>



        <View style={styles.bottomView}>{this._renderFooter()}</View>

      </Container>
   
    );
  }
}
