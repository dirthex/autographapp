import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	StatusBar,
	Linking,
	TouchableOpacity,
	Platform,
	ImageBackground,
	BackHandler,
	ScrollView,
	AsyncStorage,
	TextInput,
	Animated,
	I18nManager,
	Dimensions,
	Alert,
	Button
} from 'react-native';
import { CachedImage } from "react-native-cached-image";
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import PhotoUpload from 'react-native-photo-upload'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {
	Content,
	Container,

	Icon,
	Right,
	Item,
	Input,
	Header,
	Left,
	Body,
	Title,
	Segment,
	Label,
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import { Fonts, Metrics, Colors, Images } from '../../../Themes/';
import Globals from '../Globals';
//import TextField from '../TextField';
import { TextField } from 'react-native-material-textfield';
import DatePicker from 'react-native-datepicker';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Config from "../Config/config";
import axios from "axios";

import { SocialIcon } from 'react-native-elements'
import ImagePicker from 'react-native-image-crop-picker';
const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 16.265;
const LONGITUDE = -61.551;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;
export default class Login extends Component {
	constructor(props) {
		super(props);
		this.onChangeText = this.onChangeText.bind(this);
		this.state = {
			region: {
				latitude: LATITUDE,
				longitude: LONGITUDE,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA,
			},
			markers: {
				coordinate: {
					latitude: 16.265,
					longitude:  -61.551,
				},
				key: id,

			},
			data: [],
			date1: '',
			date: '',
			segment: 1,
			email: '',
			password: '',
			firstName: '',
			titre: '',
			emailRegister: '',
			birthday: '',
			gender: 'null',
			passwordRegister: '',
			confirmPassword: '',
			isLoading: false,
			photos: [],
			photo: "",
			toke: "",
			categorie: [
				{
					value: 'voiture',
				},
				{
					value: 'moto',
				},
				{
					value: 'bateau',
				},
			],
			typecarburant: [
				{
					value: 'diesel',
				},
				{
					value: 'essence',
				},
				{
					value: 'gpl',
				},
				{
					value: 'electrique',
				},
				{
					value: 'hybride',
				},
			],
			typevitesse: [
				{
					value: 'manuelle',
				},
				{
					value: 'automatique',
				},

			],
			marques: [],
			marque: [],
			models: [],
			model: [],
			idmarque: "",
			ImageSourceviewarray: [],
			position: [
				{
					value: 'Toutes les positions',
				},
				{
					value: 'guadeloupe',
				},
				{
					value: 'martinique',
				},
				{
					value: 'guyane',
				},


			],
			categorieSelectionne: "",
			marqueSelectionne: "",
			modelSelectionne: "",
			carburantSelectionne: "",
			vitesseSelectionne: "",
			principalPhoto: "",
			photo1: "",
			photo2: "",
			photo3: "",
			photo4: "",

		};
	}
	openDialog = (show) => {
		this.setState({ showDialog: show });
	}

	openConfirm = (show) => {
		this.setState({ showConfirm: show });
	}

	openProgress = () => {
		this.setState({ showProgress: true });

		setTimeout(
			() => {
				this.setState({ showProgress: false });
			},
			4000,
		);
	}

	optionYes = () => {
		this.openConfirm(false);
		// Yes, this is a workaround :(
		// Why? See this https://github.com/facebook/react-native/issues/10471
		setTimeout(
			() => {
				Alert.alert("The YES Button touched!");
			},
			300,
		);
	}

	optionNo = () => {
		this.openConfirm(false);
		// Yes, this is a workaround :(
		// Why? See this https://github.com/facebook/react-native/issues/10471
		setTimeout(
			() => {
				Alert.alert("The NO Button touched!");
			},
			300,
		);
	}
	onMapPress(e) {
		this.setState({
			markers:
			{
				coordinate: e.nativeEvent.coordinate,
				key: id++,

			},
		});
	}
	takePics = () => {
		const config = {
			headers: {
				"Content-Type": "multipart/form-data; charset=utf-8;"
			}
		};
		const formData = new FormData();

		ImagePicker.openPicker({
			width: 200,
			height: 200, compressImageMaxHeight: 400,
			compressImageMaxWidth: 400, cropping: true, multiple: true
		})
			.then(response => {
				let tempArray = []
				console.log("responseimage-------" + JSON.stringify(response))
				this.setState({ ImageSource: response })
				console.log("responseimagearray" + this.state.ImageSource)
				response.forEach((item) => {
					let image = {
						uri: item.path,
						// width: item.width,
						// height: item.height,
					}
					console.log("imagpath==========" + JSON.stringify(image))
					tempArray.push(image)
					this.setState({ ImageSourceviewarray: tempArray })
					// console.log('savedimageuri====='+item.path);


				})
				console.log("imagpat            h==========" + JSON.stringify(this.state.ImageSourceviewarray[1].uri))

				formData.append('qqfile',
					{ uri: this.state.ImageSourceviewarray[0].uri, name: 'photo1', type: 'image/jpg' });
				axios.post(Config.SERVER_URL + '/uploadimage', formData, config).then(
					response => {
						this.setState({ photo1: response.data.newUuid })
						console.log("***************************1111")
						console.log({ response });
					},
					error => {
						console.log({ error });
					}
				);
				const formData1 = new FormData();
				formData1.append('qqfile',
					{ uri: this.state.ImageSourceviewarray[1].uri, name: 'photo2', type: 'image/jpg' });
				axios.post(Config.SERVER_URL + '/uploadimage', formData1, config).then(
					response => {
						this.setState({ photo2: response.data.newUuid })
						console.log({ response });
					},
					error => {
						console.log({ error });
					}
				);
				const formData2 = new FormData();
				formData2.append('qqfile',
					{ uri: this.state.ImageSourceviewarray[2].uri, name: 'photo3', type: 'image/jpg' });
				axios.post(Config.SERVER_URL + '/uploadimage', formData2, config).then(
					response => {
						this.setState({ photo3: response.data.newUuid })
						console.log({ response });
					},
					error => {
						console.log({ error });
					}
				);
				const formData3 = new FormData();
				formData3.append('qqfile',
					{ uri: this.state.ImageSourceviewarray[3].uri, name: 'photo4', type: 'image/jpg' });
				axios.post(Config.SERVER_URL + '/uploadimage', formData3, config).then(
					response => {
						this.setState({ photo4: response.data.newUuid })
						console.log("**********4" + this.state.photo4)
						console.log({ response });
					},
					error => {
						console.log({ error });
					}
				);
			}).catch((err) => { console.log("openCamera catch" + err.toString()) });
		console.log("*******************" + this.state.photo)
		console.log(this.state.photo1)
		console.log(this.state.photo2)
		console.log(this.state.photo3)
	}
	takePic = () => {
		const config = {
			headers: {
				"Content-Type": "multipart/form-data; charset=utf-8;"
			}
		};
		const formData = new FormData();
		ImagePicker.openPicker({
			width: 200,
			height: 200, compressImageMaxHeight: 400,
			compressImageMaxWidth: 400, cropping: true, multiple: false
		})
			.then(response => {
				this.setState({ ImageSource: response })
				let image = {
					uri: response.path,
					
				}
				
			
				this.setState({ photo: image })
			

			
				formData.append('qqfile',
					{ uri: this.state.photo.uri, name: 'principal', type: 'image/jpg' });


				axios.post(Config.SERVER_URL + '/uploadimage', formData, config).then(
					response => {
						this.setState({ principalPhoto: response.data.newUuid })
						console.log({ response });
					},
					error => {
						console.log({ error });
					}
				);
				
			}).catch((err) => { console.log("openCamera catch" + err.toString()) });




	}
	getValueofCaregorie(value) {
	
		this.setState({ categorieSelectionne: value })
		this.getMarques(value)
		
		
	}
	
	
	getValueofCarburant(value) {
		this.setState({ carburantSelectionne: value })
	}
	getValueofMarque(value) {
		this.setState({ marqueSelectionne: value })
		for (var i = 0; i < this.state.marque.length; i++) {
			if (this.state.marque[i].value == value) {
				this.state.idmarque = this.state.marque[i].id
			}
		}
		this.getModels(this.state.idmarque)
	}
	getValueofModele(value) {
		this.setState({ modelSelectionne: value })
	}
	getValueofVitesse(value) {
		this.setState({ vitesseSelectionne: value })
	}
	
	getMarques(value) {
		console.log("vaaalue" + value)
		this.setState({
			marques: [],
			marque: [],



		});
		axios.get(Config.SERVER_URL + '/api/v1/annonces/1/' + value
		).then((res) => {
			this.setState({

				marques: res.data.marques,



			});
			for (var i = 0; i < this.state.marques.length; i++) {
				this.state.marque.push({
					id: this.state.marques[i].id,
					value: this.state.marques[i].nom,

				})
			}


		}).catch((error) => {
			this.setState({
				error: 'Error retrieving data',
				loading: false
			});
		});
	}
	getModels(id) {

		this.setState({
			model: [],
			models: [],



		});
		axios.get(Config.SERVER_URL + '/api/v1/marque/' + id
		).then((res) => {
			this.setState({

				models: res.data,



			});


			for (var i = 0; i < this.state.models.length; i++) {
				this.state.model.push({
					id: this.state.models[i].id,
					value: this.state.models[i].nom,

				})
			}











		}).catch((error) => {
			this.setState({
				error: 'Error retrieving data',
				loading: false
			});
		});

	}

	componentWillMount() {
		var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('MonCompte');
			return true;
		});
	}
	async loadJWT() {
		try {
			const value = await AsyncStorage.getItem('token');
			console.log("ommmmmmmmmmmae))))" + value)
			if (value !== null) {
				this.setState({
					token: value,
					loading: false
				});
				return Promise.resolve(value)
			} else {
				this.setState({
					loading: false
				});
				return Promise.resolve("error");
			}
		} catch (error) {
			console.log('AsyncStorage Error: ' + error.message);
			return Promise.resolve("error")
		}
	}
	getToken = async () => {
		const userToken = await AsyncStorage.getItem('token');
		const token = userToken;
		this.state.token = userToken;
		return Promise.resolve(token);
	}
	AddAnnonce() {
		if(this.state.categorieSelectionne==""){
			this.openDialog(true)
		}
		else if(this.state.categorieSelectionne=="voiture" && ( this.state.carburantSelectionne == "" || this.state.marqueSelectionne == ""
		|| this.state.modelSelectionne == "" || this.state.titre == "" || this.state.annee == "" || this.state.kilometrage == ""
		|| this.state.carburantSelectionne == "" || this.state.vitesseSelectionne == "" || this.state.description == "" ||
		this.state.prix == ""))
		{	this.openDialog(true)
		}
		else if (this.state.categorieSelectionne=="moto"  && (this.state.marqueSelectionne == ""
		|| this.state.modelSelectionne == "" || this.state.titre == "" || this.state.annee == "" || this.state.kilometrage == ""
		|| this.state.description == "" ||
		this.state.prix == "" || this.state.cylindre =="")) {
			this.openDialog(true)
		}
		else if(this.state.categorieSelectionne=="bateau" && (this.state.titre =="" || this.state.description=="" || this.state.prix=="" ) )
	{
		this.openDialog(true)
	}
		else {
			console.log("*************+++++++++++++++++***********" + this.state.categorieSelectionne)
			this.getToken().then(token => {
				const headers = {
					'Authorization': 'Bearer ' + token
				};

				console.log("phhhhhhhhhhhhhhhhhhhhhotototooto" + token)
				if (this.state.categorieSelectionne == "voiture") {
					this.state.data = {
						titre: this.state.titre,
						cat: this.state.categorieSelectionne,
						marqueselect: this.state.idmarque,
						marqueselectmoto: null,
						modelselect: this.state.modelSelectionne,
						modelselectmoto: null,
						annee: this.state.annee,
						carburant: this.state.carburantSelectionne,
						btv: this.state.vitesseSelectionne,
						kilo: this.state.kilometrage,
						kilomoto: null,
						cylindre: null,
						prixmeth: 1,
						prixGuadloupe: 0,
						prixMartinique: 0,
						prixGuyan: 0,
						prixuni: this.state.prix || 0,
						desc: this.state.description,
						principaleimage: this.state.principalPhoto,
						secondesimage: (this.state.photo1 + ',' + this.state.photo2 + ',' + this.state.photo3 + ',' + this.state.photo4),
						hidenlat: this.state.markers.coordinate.latitude || "16.248351",
						hidenlong: this.state.markers.coordinate.longitude || "-61.559078",


					}
				} else if (this.state.categorieSelectionne == "moto") {
					this.state.data = {
						titre: this.state.titre,
						cat: this.state.categorieSelectionne,
						marqueselect: null,
						marqueselectmoto: this.state.idmarque,
						modelselect: null,
						modelselectmoto: this.state.modelSelectionne,
						annee: this.state.annee,
						carburant: this.state.carburant,
						btv: null,
						kilo: null,
						kilomoto: this.state.kilometrage,
						cylindre: this.state.cylindre,
						prixmeth: 1,
						prixGuadloupe: 0,
						prixMartinique: 0,
						prixGuyan: 0,
						prixuni: this.state.prix || 0,
						desc: this.state.description,
						principaleimage: this.state.principalPhoto,
						secondesimage: (this.state.photo1 + ',' + this.state.photo2 + ',' + this.state.photo3 + ',' + this.state.photo4),
						hidenlat: this.state.markers.coordinate.latitude || "16.248351",
						hidenlong: this.state.markers.coordinate.longitude || "-61.559078",


					}
				}
				else {
					this.state.data = {
						titre: this.state.titre,
						cat: this.state.categorieSelectionne,

						prixmeth: 1,
						prixGuadloupe: 0,
						prixMartinique: 0,
						prixGuyan: 0,
						prixuni: this.state.prix || 0,
						desc: this.state.description,
						principaleimage: this.state.principalPhoto,
						secondesimage: (this.state.photo1 + ',' + this.state.photo2 + ',' + this.state.photo3 + ',' + this.state.photo4),
						hidenlat: this.state.markers.coordinate.latitude || "16.248351",
						hidenlong: this.state.markers.coordinate.longitude || "-61.559078",


					}
				}

				axios.post(Config.SERVER_URL + '/api/v1/deposer_annonce', this.state.data

					, { headers })
					.then(response => {
						console.log("repossssssssse" + JSON.stringify(response.data));
						console.log({ response });
						if (response.data == "OK") {
							Alert.alert("Nouvelle annonce ajoutée");

						}

						else {
							Alert.alert("Annonce n'a pas été crée , veuillez réessayer");
						}

						// this.props.navigation.navigate('signIn')

					})
					.catch((error) => {
						console.log("teeest");
						console.log(error);
						//this.onLoginFail();

					});
			});
		}

	}

	handleTextChange = newText => this.setState({ value1: newText });


	onChangeText(text) {
		this.setState({ gender: text });
		// console.warn(this.state.gender);
	}


	deletePic() {

		this.setState({ photo: "" })
	}
	savePic() {
		console.log("saaave " + this.state.photo.uri)

	}

	deletePics(item) {
		var array = this.state.ImageSourceviewarray;
		for (var i = 0; i < array.length; i++) {
			if (array[i].uri === item.uri) {
				array.splice(i, 1);
			}
			this.setState({ ImageSourceviewarray: array })
		}
		console.log("**************" + JSON.stringify(this.state.ImageSourceviewarray))
	}

	_handleBagNavigation() {
		AsyncStorage.multiSet([
			['ArrivedFrom', "ECommerceLogin"],
		]);
		this.props.navigation.navigate("ECommerceMyBag");
	}

	_handleWishListNavigation() {
		AsyncStorage.multiSet([
			['ArrivedForWishList', "ECommerceLogin"],
		]);
		this.props.navigation.navigate("ECommerceWishList");
	}

	_renderFooter = () => {


		// Sign Up

		return (
			<TouchableOpacity
				style={styles.facebookBtnBg}
				onPress={() => this.AddAnnonce()}>
				<Text style={styles.loginWithFacebookTxt}>Déposer votre annonce</Text>
			</TouchableOpacity>
		);

	};

	_renderActiveComponent = () => {
		const { segment } = this.state;

		// Login

		// SignUp

		return (
			<Content>



				<View style={styles.mainListRenderRow}>
					{this.state.photo ? (

						<View style={styles.Foodimg}>
							{this.savePic()}
							<TouchableOpacity

							>
								<CachedImage
									source={this.state.photo}
									style={styles.Foodimg}
								>
									<TouchableOpacity
										onPress={() => this.deletePic()}
									>
										<FontAwesome
											name="close"
											size={15}
											color={"white"}
											style={styles.heartListIcon}
										/>
									</TouchableOpacity>
								</CachedImage>
							</TouchableOpacity>
						</View>
					) : (
							<View style={styles.Foodimg}>

								<Text style={styles.boxedTitel}>
									Image principale</Text>

							</View>
						)}
					<View>
						<View style={{ alignItems: "flex-end" }}>


							<TouchableOpacity
								onPress={() => this.takePic()}
							>
								<Entypo name="camera" color="#000000" size={50} />
							</TouchableOpacity>

						</View>

					</View>
				</View>

				<View style={styles.mainListRenderRow}>
					{this.state.ImageSourceviewarray.length > 0 ? (

						<View style={{
							flexDirection: "row", justifyContent: "space-between",
							height: Metrics.HEIGHT * 0.2,
							flexWrap: 'wrap',
							flex: 1
						}}>

							{this.state.ImageSourceviewarray.map((item, index) => {
								return (
									<TouchableOpacity>
										<CachedImage
											source={{ uri: item.uri }}
											style={{
												width: 100,
												height: 100,
												borderWidth: 1.5,
												borderColor: 'white',


											}}


										>
											<TouchableOpacity
												onPress={() => this.deletePics(item)}
											>
												<FontAwesome
													name="close"
													size={15}
													color={"white"}
													style={styles.heartListIcon}
												/>
											</TouchableOpacity>
										</CachedImage>
									</TouchableOpacity>
								);
							})}
						</View>
					) : (
							<View style={styles.Foodimg}>

								<Text style={styles.boxedTitel}>
									Images secondaires</Text>

							</View>
						)}
					<View style={{ alignItems: "flex-end" }}>


						<TouchableOpacity
							onPress={() => this.takePics()}
						>
							<Entypo name="camera" color="#000000" size={50} />
						</TouchableOpacity>

					</View>
					<View>


					</View>
				</View>

				<View style={styles.ListContentMain}>

					<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>




						{/*	<TouchableOpacity
                style={styles.searchView}
              
            >
             <GooglePlacesAutocomplete
			 query={{ key: 'AIzaSyCBpOPV2_7lrgbVwJySGC8JW5ATM5Yrncw' }}
			 
  placeholder='Où'
  minLength={2}
  key="AIzaSyCBpOPV2_7lrgbVwJySGC8JW5ATM5Yrncw"
  autoFocus={false}
  returnKeyType={'default'}
  fetchDetails={true}
  
 
  currentLocation={false}
/>
   
              <Text style={styles.searchText}>Search</Text>            </TouchableOpacity>*/}

					</View>

					<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
						<Dropdown
							itemCount={30}
							label="Categories"
							data={this.state.categorie}
							containerStyle={styles.dropDownstyle}
							textColor={'#959595'}
							onChangeText={value => this.getValueofCaregorie(value)}
						/>
					</View>
					{this.state.categorieSelectionne == "bateau" ? (
						<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
							<View style={{ height: 2 }} />

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Titre"
								value={this.state.titre}
								keyboardType="default"
								returnKeyType="done"
								maxLength={20}

								selectionColor={'#0000ff'}
								autoCapitalize="none"
								onChangeText={titre => this.setState({ titre })}
							/>

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Prix"

								keyboardType={'numeric'}
								returnKeyType="done"
								maxLength={15}
								selectionColor={'#959595'}

								//secureTextEntry={true}
								onChangeText={prix =>
									this.setState({ prix })
								}
							/>
							<View style={{ height: 10 }} />

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Description"
								multiline={true}
								keyboardType="default"
								returnKeyType="done"
								maxLength={35}
								selectionColor={'#959595'}



								onChangeText={description =>
									this.setState({ description })
								}
							/>
						</View>
					) : this.state.categorieSelectionne == "voiture" ? (
						<View>
							<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
								<Dropdown
									itemCount={10}
									label="Marques"
									data={this.state.marque}
									containerStyle={styles.dropDownstyle}
									textColor={'#959595'}
									onChangeText={value => this.getValueofMarque(value)}
								/>
							</View>
							<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
								<Dropdown
									label="Model"
									itemCount={10}
									data={this.state.model}
									containerStyle={styles.dropDownstyle}
									textColor={'#959595'}
									onChangeText={value => this.getValueofModele(value)}
								/>
							</View>
							<View style={{ height: 10 }} />

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Titre"
								value={this.state.titre}
								keyboardType="default"
								returnKeyType="done"
								autoCapitalize="none"
								onChangeText={titre => this.setState({ titre })}
							/>

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Année-modèle"
								characterRestriction={4}
								keyboardType={'numeric'}
								returnKeyType="done"
								selectionColor={'#959595'}
								maxLength={10}

								autoCapitalize="none"
								onChangeText={annee =>
									this.setState({ annee })
								}
							/>





							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Kilométrage"

								keyboardType={'numeric'}
								returnKeyType="done"
								maxLength={15}
								selectionColor={'#959595'}
								autoCapitalize="none"
								onChangeText={kilometrage =>
									this.setState({ kilometrage })
								}
							/>

							<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
								<Dropdown
									itemCount={10}
									label="Carburant"
									data={this.state.typecarburant}
									containerStyle={styles.dropDownstyle}
									containerStyle={styles.dropDownstyle}
									textColor={'#959595'}
									onChangeText={value => this.getValueofCarburant(value)}
								/>
							</View>


							<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
								<Dropdown
									itemCount={10}
									label="Boite de vitesse"
									data={this.state.typevitesse}
									containerStyle={styles.dropDownstyle}
									containerStyle={styles.dropDownstyle}
									textColor={'#959595'}
									onChangeText={value => this.getValueofVitesse(value)}
								/>
							</View>

							<View style={{ height: 10 }} />

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Description"
								multiline={true}
								keyboardType="default"
								returnKeyType="done"
								maxLength={35}
								selectionColor={'#959595'}



								onChangeText={description =>
									this.setState({ description })
								}
							/>

							<View style={{ height: 10 }} />

							<TextField
								containerStyle={styles.formInput}
								textColor={'#959595'}
								lineWidth={1}
								tintColor={'#959595'}
								baseColor={'#959595'}
								label="Prix"

								keyboardType={'numeric'}
								returnKeyType="done"
								maxLength={15}
								selectionColor={'#959595'}

								//secureTextEntry={true}
								onChangeText={prix =>
									this.setState({ prix })
								}
							/>


						</View>
					) : (
								<View>
									<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
										<Dropdown
											itemCount={10}
											label="Marques"
											data={this.state.marque}
											containerStyle={styles.dropDownstyle}
											textColor={'#959595'}
											onChangeText={value => this.getValueofMarque(value)}
										/>
									</View>
									<View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
										<Dropdown
											itemCount={10}
											label="Model"
											data={this.state.model}
											containerStyle={styles.dropDownstyle}
											textColor={'#959595'}
											onChangeText={value => this.getValueofModele(value)}
										/>
									</View>
									<View style={{ height: 10 }} />

									<TextField
										containerStyle={styles.formInput}
										textColor={'#959595'}
										lineWidth={1}
										tintColor={'#959595'}
										baseColor={'#959595'}
										label="Titre"
										keyboardType="default"
										
										returnKeyType="done"
										maxLength={15}
										selectionColor={'#959595'}
										autoCapitalize="none"
										onChangeText={titre =>
											this.setState({ titre })
										}
									/>
									<TextField
										containerStyle={styles.formInput}
										textColor={'#959595'}
										lineWidth={1}
										tintColor={'#959595'}
										baseColor={'#959595'}
										label="Année-modèle"
										characterRestriction={4}
										keyboardType={'numeric'}
										returnKeyType="done"
										selectionColor={'#959595'}
										maxLength={10}

										autoCapitalize="none"
										onChangeText={annee =>
											this.setState({ annee })
										}
									/>





									<TextField
										containerStyle={styles.formInput}
										textColor={'#959595'}
										lineWidth={1}
										tintColor={'#959595'}
										baseColor={'#959595'}
										label="Kilométrage"

										keyboardType={'numeric'}
										returnKeyType="done"
										maxLength={15}
										selectionColor={'#959595'}
										autoCapitalize="none"
										onChangeText={kilometrage =>
											this.setState({ kilometrage })
										}
									/>


									<View style={{ height: 10 }} />

									<TextField
										containerStyle={styles.formInput}
										textColor={'#959595'}
										lineWidth={1}
										keyboardType={'numeric'}
										tintColor={'#959595'}
										baseColor={'#959595'}
										label="Cylindre"
										keyboardType={'numeric'}
										returnKeyType="done"
										maxLength={35}
										selectionColor={'#959595'}



										onChangeText={cylindre =>
											this.setState({ cylindre })
										}
									/>


									<View style={{ height: 10 }} />

									<TextField
										containerStyle={styles.formInput}
										textColor={'#959595'}
										lineWidth={1}
										tintColor={'#959595'}
										baseColor={'#959595'}
										label="Description"
										multiline={true}
										keyboardType="default"
										returnKeyType="done"
										maxLength={35}
										selectionColor={'#959595'}



										onChangeText={description =>
											this.setState({ description })
										}
									/>

									<View style={{ height: 10 }} />

									<TextField
										containerStyle={styles.formInput}
										textColor={'#959595'}
										lineWidth={1}
										tintColor={'#959595'}
										baseColor={'#959595'}
										label="Prix"

										keyboardType={'numeric'}
										returnKeyType="done"
										maxLength={15}
										selectionColor={'#959595'}

										//secureTextEntry={true}
										onChangeText={prix =>
											this.setState({ prix })
										}
									/>


								</View>
							)}

					<View style={styles.styleMpasView}>
					<MapView
						provider={this.props.provider}
						style={styles.MapView}
						initialRegion={this.state.region}
						onPress={e => this.onMapPress(e)}
					>

						<Marker
							key={this.state.markers.key}
							coordinate={this.state.markers.coordinate}

						/>



					</MapView>
					
					</View>




					<Dialog
						title="Erreur"
						animationType="fade"
						contentStyle={
							{
								alignItems: "center",
								justifyContent: "center",
							}
						}
						onTouchOutside={() => this.openDialog(false)}
						visible={this.state.showDialog}
					>
						<Image
							source={

								Images.croix

							}
							style={
								{
									width: 99,
									height: 87,
									backgroundColor: "black",
									marginTop: 10,
									resizeMode: "contain",
									backgroundColor: "white"
								}
							}
						/>

						<Text style={{ marginVertical: 30 }}>
							Tous les champs sont obligatoires
					    </Text>

						<Button
							onPress={() => this.openDialog(false)}
							style={{ marginTop: 10 }}
							title="CLOSE"
						/>


					</Dialog>


				</View>

			</Content>
		);

	};

	render() {
		var that = this;
		let BG_Image = {
			uri:
				'https://antiqueruby.aliansoftware.net/Images/signin/ic_back01_sone.png',
		};
		StatusBar.setBarStyle('light-content', true);

		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor('#0e1130', true);
			StatusBar.setTranslucent(true);
		}

		return (
			<Container style={styles.container}>
				<Header style={styles.HeaderBg} transparent>
					<Left style={styles.left}>
						<TouchableOpacity
							onPress={() => this.props.navigation.openDrawer()}
						>
							<Entypo name="menu" color="#FFFFFF" size={30} />
						</TouchableOpacity>
					</Left>


					<Right style={styles.right}>
						<SocialIcon
							type='facebook'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.facebook) }}
						/>
						<SocialIcon
							type='instagram'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.instagram) }}
						/>
						<SocialIcon
							type='youtube'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.youtube) }}
						/>
					</Right>
				</Header>


				<KeyboardAwareScrollView>
					<View style={styles.contentOne}>




						{this._renderActiveComponent()}

					</View>

					{}

				</KeyboardAwareScrollView>



				<View style={styles.bottomView}>{this._renderFooter()}</View>

			</Container>
		);
	}
}
