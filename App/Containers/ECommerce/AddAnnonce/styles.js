import { Platform, StyleSheet, Dimensions } from 'react-native';
import { Fonts, Metrics, Colors } from '../../../Themes/';

const styles = StyleSheet.create({
	container: {
		height: Metrics.HEIGHT,
		width: Metrics.WIDTH,
		backgroundColor: Colors.snow,
	},

	header: {
		backgroundColor: '#23292e',
		borderBottomWidth: 0,
		...Platform.select({
			ios: {
				paddingTop: Metrics.HEIGHT * 0.02,
				height: Metrics.HEIGHT * 0.1,
			},
			android: {
				paddingTop: Metrics.HEIGHT * 0.02,
				height: Metrics.HEIGHT * 0.1,
			},
		}),
		elevation: 0,
		paddingLeft: Metrics.WIDTH * 0.05,
		paddingRight: Metrics.WIDTH * 0.05,
	},

	left: {
		flex: 1,
		backgroundColor: Colors.transparent,
	},

	backArrow: {
		justifyContent: 'center',
		alignItems: 'center',
	},

	body: {
		flex: 2,
		alignItems: 'center',
		backgroundColor: Colors.transparent,
	},

	textTitle: {
		color: Colors.snow,
		fontSize: Fonts.moderateScale(20),
		alignSelf: 'center',
		fontFamily: Fonts.type.helveticaNeueLight,
		...Platform.select({
			ios: {},
			android: {
				paddingTop: Metrics.WIDTH * 0.02,
			},
		}),
	},

	right: {
		flex: 1,
		backgroundColor: Colors.transparent,
		...Platform.select({
			ios: {},
			android: {
				height: Metrics.HEIGHT * 0.06,
				marginTop: -(Metrics.WIDTH * 0.03),
			},
		}),
	},

	heartBg: {
		width: Metrics.WIDTH * 0.054,
		height: Metrics.WIDTH * 0.054,
		borderRadius: Metrics.WIDTH * 0.027,
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: Colors.snow,
		alignItems: 'center',
		justifyContent: 'center',
	},

	bagIcon: {
		marginLeft: Metrics.WIDTH * 0.04,
		color: Colors.snow,
	},
	input: {    
		borderBottomWidth: 1.5, 
		marginLeft: 20,
		color: '#333',       
	  },

	heartIcon: {
		color: Colors.snow,
		alignSelf: 'center',
	},
	HeaderBg: {
		backgroundColor: "#23292e",
		borderBottomWidth: 1
	  },
	

	content: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT * 0.8,
	},

	bottomView: {
		width: Metrics.WIDTH,
		alignItems: 'center',
		justifyContent: 'center',
		height: Metrics.HEIGHT * 0.094,
	},

	divider: {
		backgroundColor: '#d8d8d8',
		width: Metrics.WIDTH,
		height: Metrics.WIDTH * 0.003,
	},

	loginSignUpTxt: {
		fontSize: Fonts.moderateScale(16),
		fontFamily: Fonts.type.sfuiDisplayRegular,
	},

	facebookBtnBg: {
		backgroundColor: '#dddbca',
		width: Metrics.WIDTH * 0.96,
		alignSelf: 'center',
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: Metrics.WIDTH * 0.025,
		paddingBottom: Metrics.WIDTH * 0.025,
		
	},

	loginWithFacebookTxt: {
		color: Colors.snow,
		fontSize: Fonts.moderateScale(17),
		// fontFamily: Fonts.type.sfuiDisplayLight,
		marginLeft: Metrics.WIDTH * 0.02,
	},

	loginSignUpContent: {
		...Platform.select({
			ios: {
				height: Metrics.HEIGHT * 0.714,
			},
			android: {
				height: Metrics.HEIGHT * 0.7,
			},
		}),
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
	},

	textInput: {
		height: Metrics.HEIGHT * 0.07,
		alignSelf: 'center',
		width: Metrics.WIDTH * 0.9,
		fontSize: Fonts.moderateScale(14),
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: '#959595',
		marginLeft: 15,
		paddingLeft: 15,
		marginTop: Metrics.HEIGHT * 0.02,
		backgroundColor: 'red',
	},

	item: {
		alignSelf: 'center',
		width: Metrics.WIDTH * 0.94,
		justifyContent: 'center',
	},
	ListContentMain: {
		backgroundColor: "#f5f5f5",
		marginLeft: Metrics.HEIGHT * 0.02,
		marginRight: Metrics.HEIGHT * 0.02,
		...Platform.select({
		  ios: {
			shadowOpacity: 0.4,
			borderWidth: 1
		  },
		  android: {
			shadowOpacity: 0.1
		  }
		}),
		shadowColor: "gray",
		shadowOffset: { width: 2, height: 2 },
		shadowRadius: 4,
		elevation: 3,
		borderColor: "#e8e8e8",
		marginBottom: Metrics.HEIGHT * 0.03,
		borderRadius: 2,
		marginTop: Metrics.HEIGHT * 0.02,
		
	  },
	

	floatingView: {
		alignSelf: 'center',
		width: Metrics.WIDTH * 0.94,
		justifyContent: 'center',
		marginTop: Metrics.HEIGHT * 0.015,
	},
	dropDownstyle:{marginLeft:5,marginRight:5},
	floatingInput:{
		marginLeft:5,marginRight:5,
		width:Metrics.WIDTH * 0.8
	},
	inputemail: {
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: '#959595',
		fontSize: Fonts.moderateScale(15),
	},
	textLabel: {
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: '#959595',
		fontSize: Fonts.moderateScale(15),
		marginLeft: 10,
		fontSize: 10,
	},

	forgotPasswordTxt: {
		color: '#0691ce',
		fontSize: Fonts.moderateScale(16),
		fontFamily: Fonts.type.sfuiDisplayRegular,
	},
	mainListRenderRow: {
		alignSelf: "center",
		backgroundColor: "#fff",
		width: Metrics.WIDTH * 0.93,
		borderRadius: 2,
		marginTop: Metrics.HEIGHT * 0.02,
		shadowColor: "#f9f9f9",
		shadowOffset: { width: 1, height: 1 },
		shadowRadius: 2,
		elevation: 2,
		shadowOpacity: 0.1,
		marginBottom: Metrics.HEIGHT * 0.01
	  },
	  heartListIcon :{
		alignSelf: 'flex-end',
	  },
	  welcomeText: {
        fontSize: 20,
        margin: 10,
        textAlign: "center",
	},
	boxedTitel : {
		marginTop: 20, height: Metrics.HEIGHT*0.1, fontSize: 20, 
		fontWeight: 'bold', textAlign: "center",
	},
    exampleText: {
        fontSize: 20,
        marginBottom: 25,
        textAlign: "center",
    },
    instructionsText: {
        color: "#333333",
        fontSize: 16,
        marginBottom: 40,
        textAlign: "center",
    },
	  Foodimg: {
		width: Metrics.WIDTH * 0.93,
		...Platform.select({
		  ios: {
			height: Metrics.HEIGHT * 0.3,
			borderRadius: 2
		  },
		  android: {
			height: Metrics.HEIGHT * 0.2,
			borderRadius: 2
		  }
		}),
		resizeMode:'stretch'
	  },
	  MapView: {
		width: Metrics.WIDTH*0.92,
		...Platform.select({
		  ios: {
			height: Metrics.HEIGHT * 0.7
		  },
		  android: {
			height: Metrics.HEIGHT * 0.7
		  }
		}),
		marginTop: Metrics.HEIGHT * 0.05,
		marginBottom: Metrics.HEIGHT * 0.1
	  },
	loginBg: {
		backgroundColor: '#0691ce',
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
		height: Metrics.HEIGHT * 0.06,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 5,
	},
	styleMpasView :{
		height: Metrics.HEIGHT * 0.5,
		marginBottom: Metrics.HEIGHT * 0.2,
		paddingBottom: Metrics.HEIGHT * 0.2
	},
	alertBg: {
		width: Metrics.WIDTH * 0.03,
		height: Metrics.WIDTH * 0.03,
		borderRadius: Metrics.WIDTH * 0.015,
		backgroundColor: '#ff0000',
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: -(Metrics.WIDTH * 0.018),
	},

	labelInput: {
		color: '#959595',
	  },
	  formInput: {    
		marginLeft:20,marginRight:20,
		      
	  },
	  input: {
		borderWidth: 0
	  },
	alertTxt: {
		fontSize: Fonts.moderateScale(8),
		fontFamily: Fonts.type.sfuiDisplayMedium,
		color: Colors.snow,
	},

	dropdown: {
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
		height: Metrics.HEIGHT * 0.07,
		backgroundColor: 'red',
	},

	picker: {
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
		height: Metrics.HEIGHT * 0.07,
		backgroundColor: 'green',
	},

	activeTab: {
		color: Colors.snow,
		fontFamily: Fonts.type.sfuiDisplayRegular,
		fontSize: Fonts.moderateScale(14),
	},

	normalTab: {
		color: '#0691ce',
		fontFamily: Fonts.type.sfuiDisplayRegular,
		fontSize: Fonts.moderateScale(14),
	},

	segmentTab: {
		...Platform.select({
			ios: {
				width: Metrics.WIDTH * 0.467,
				height: 31,
			},
			android: {
				width: Metrics.WIDTH * 0.465,
				height: 29,
			},
		}),
		backgroundColor: '#FFF',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
	},

	segmentSelectedTab: {
		...Platform.select({
			ios: {
				width: Metrics.WIDTH * 0.467,
				height: 31,
			},
			android: {
				width: Metrics.WIDTH * 0.465,
				height: 29,
			},
		}),
		backgroundColor: '#0691ce',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
	},

	contentOne: {
		width: Metrics.WIDTH,
	},

	segmentSelectedTabOne: {
		width: Metrics.WIDTH * 0.47,
		height: Metrics.HEIGHT * 0.05,
		backgroundColor: '#0691ce',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
		borderRadius: Metrics.HEIGHT * 0.007,
	},

	segmentTabOne: {
		width: Metrics.WIDTH * 0.47,
		height: Metrics.HEIGHT * 0.05,
		backgroundColor: 'transparent',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
	},

	segmentTabSecOne: {
		width: Metrics.WIDTH * 0.94,
		height: Metrics.HEIGHT * 0.05,
		marginVertical: Metrics.HEIGHT * 0.03,
		borderRadius: Metrics.HEIGHT * 0.007,
		backgroundColor: Colors.snow,
		borderColor: '#0691ce',
		padding: 0,
		borderWidth: 1,
		alignSelf: 'center',
	},

	activeTabOne: {
		color: Colors.snow,
		//  fontFamily: Fonts.type.helveticaNeueLight,
		fontSize: Fonts.moderateScale(15),
	},

	normalTabOne: {
		color: '#0691ce',
		//  fontFamily: Fonts.type.helveticaNeueLight,
		fontSize: Fonts.moderateScale(15),
	},

	segmentTabSec: {
		padding: 0,
		marginTop: 0,
		backgroundColor: Colors.snow,
		borderWidth: 1,
		borderRadius: 5,
		borderColor: '#0691ce',
		width: Metrics.WIDTH * 0.94,
		height: 32,
		alignSelf: 'center',
		marginTop: Metrics.WIDTH * 0.02,
	},
	map:{
		flex:1
	},

	forgotPasswordBg: {
		alignSelf: 'flex-end',
		marginTop: Metrics.HEIGHT * 0.015,
		marginRight: Metrics.HEIGHT * 0.015,
	},
});

export default styles;
