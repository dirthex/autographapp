import React, { PureComponent } from "react";
import {
  View,
  Text,
  Image,
  StatusBar,
  Platform,
  TouchableOpacity,
  BackHandler,
  ListView,
  Linking,
  ImageBackground,
  ScrollView,
  Picker,
  I18nManager,
  AsyncStorage,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Alert
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Swipeout from 'react-native-swipeout';
import { withNavigation } from 'react-navigation';
import {
  Container,
  Button,
  Right,
  Left,
  ListItem,
  Content,
  Body,
  Header,
  Spinner
} from "native-base";
// Screen Styles
import styles from "./styles";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import Dropdown from "../../../Components/Dropdown/dropdown";
import { Fonts, Metrics, Colors, Images } from "../../../Themes";
import { CachedImage } from "react-native-cached-image";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "../../../Components/react-native-scrollable-tab-view";
import axios from "axios";
import Config from "../Config/config";
import { isTSThisType } from "@babel/types";
import { SocialIcon } from 'react-native-elements'

// var screenLink = "";


class CategoryProduct extends PureComponent {
   swipeoutBtns = [
    {
      text: 'Button'
    }
  ]
  signal = axios.CancelToken.source();
  constructor(props) {
    super(props);

    _isMounted = false,
      this.page = 1;
    this.state = {
      // loading: false, // user list loading
      isRefreshing: false,
      isLoading: true,
      voiture: [],
      voitures: [],
      token: '',
      loading: true


    };
    this.onChangeText = this.onChangeText.bind(this);
    this._handleProductDetailClick = this._handleProductDetailClick.bind(this);
    this.props.navigation.addListener('willFocus', () => {

      console.log("caaat" + this.state.categorieChoisi)


      //this.getblogs(id);
    })
  }

  componentWillUnmount() {
    //this._isMounted = false;
    this.signal.cancel('Api is being canceled');

  }
  componentDidMount() {
    this.getToken().then(token => {
      this.getCars(token);
    });
    //this._isMounted = true;
    /*   if (this._isMounted) {
         this.setState({ categorieChoisi: "voiture" }),
           this.setState({ titre: "all" }),
           this.setState({ marqueChoisi: "all" }),
           this.setState({ model: "all" }),
           this.setState({ prixMin: "all" }),
           this.setState({ prixMax: "all" }),
           this.setState({ anneemin: "all" }),
           this.setState({ anneemax: "all" }),
           this.setState({ kilmin: "all" }),
           this.setState({ kilmax: "all" }),
           this.setState({ cylindremin: "all" }),
           this.setState({ cylindremax: "all" }),
           this.setState({ carb: "all" }),
           this.setState({ vitesse: "all" });
       }*/
  }


  getToken = async () => {
    const userToken = await AsyncStorage.getItem('token');
    const token = userToken;
    this.state.token = userToken;
    return Promise.resolve(token);
  }
  componentWillMount() {
    const { navigation } = this.props;
    this.state.token = navigation.getParam('token', null);








    var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('MonCompte');
			return true;
		});

    console.log("teeest" + this.state.ecran)
  }

  _backPress(backFrom) {
    const { screenLink } = this.state;

    if (screenLink == "ECommerceMenu") {
      if (backFrom == "fromHardwareBack") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (backFrom == "fromLeftIcon") {
        this.props.navigation.openDrawer();
      }
    }
    if (screenLink == "ECommerceMyBag") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (screenLink == "ECommerceWishList") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (screenLink == "ECommerceMain") {
      this.props.navigation.navigate("ECommerceMenu");
    }
  }

  _handleBagNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "ECommerceCategoryProduct"]]);
    this.props.navigation.navigate("ECommerceMyBag");
  }

  _handleWishListNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate("ECommerceWishList");
  }

  onChangeText(text) {
    this.setState({ user_category_id: text });
  }

  onCheckBoxPress(id) {
    this.setState({
      selectedLots: id
    });
  }

  _handleDropDownClick(size) {
    this.setState({ selectedSize: size });
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _openDropDown(id) {
    this.onCheckBoxPress(id);
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _handleProductDetailClick() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate("AnnonceDetails", {
      screen: "Annonces",

    }
    )

  }

  _handleLEFT(screenLink) {
    if (screenLink == "ECommerceMenu") {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Ionicons
            name="ios-menu"
            size={30}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this._backPress("fromLeftIcon")}
        >
          <FontAwesome
            name={I18nManager.isRTL ? "angle-right" : "angle-left"}
            size={35}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    }
  }





  getCars(token) {
    console.log("***************")
    console.log(this.state.token)
    this.setState({ voiture: [] })
    this.setState({ voitures: [] })
    const headers = {
      'Authorization': 'Bearer ' + token
    };
    this.setState({ loading: true });
    axios.get(Config.SERVER_URL + '/api/v1/profile/annonces', {
      headers
    }
    ).then((res) => {
      console.log("*****")

      this.setState({

        voitures: res.data,



      });

      for (var i = 0; i < this.state.voitures[0].related_vec.length; i++) {
        console.log("**********" + this.state.voitures[0].related_vec[i].principalPhoto)
        if (this.state.voitures[0].related_vec[i].principalPhoto.includes("https")) {
          this.state.voiture.push({
            id: this.state.voitures[0].related_vec[i].id,
            productTitle: this.state.voitures[0].related_vec[i].titre,
            productImage: this.state.voitures[0].related_vec[i].principalPhoto,
            price: this.state.voitures[0].related_vec[i].prixunqiue,


          })
        } else {
          this.state.voiture.push({
            id: this.state.voitures[0].related_vec[i].id,
            productTitle: this.state.voitures[0].related_vec[i].titre,
            productImage: Config.SERVER_URL + this.state.voitures[0].related_vec[i].principalPhoto,
            price: this.state.voitures[0].related_vec[i].prixunqiue,

          })
        }
      }
      console.log("*****+++++++++++++++******" + JSON.stringify(this.state.voiture))


      this.setState({ loading: false });






    }).catch((err) => {

      this.setState({ loading: false });

    });

  }


  renderSeparator = () => {
    return (
      <View
        style={{

        }}
      />
    );
  };


  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };


  _handleDisLike(item, id, like, index) {
    this.getCars()
    this.state.cars.push(item);
    let tmp = this.state.cars;
    if (tmp.includes(id)) {
      tmp.splice(tmp.indexOf(id), 1);
    }

    var newData = this.state.cars.slice(); //copy array
    newData.splice(index, 1); //remove element
    for (var i = 0; i < newData.length; i++) {
      newData[i].index = i;
    }
    // this.setState({datas: newData});

    this.setState({ cars: newData, carsId: tmp });

    AsyncStorage.multiSet([[
      "cars",
      JSON.stringify(this.state.cars)
    ]]);
    for (i = 0; i < this.state.cars.length; i++) {
      if (this.state.cars[i].id == id) {

        this.state.cars[i].isLike = true;


      }
    }
    this.setState({
      cars: this.state.cars
    });

  }
  delete(id) {
    const headers = {
      'Authorization': 'Bearer ' + this.state.token
    };
    axios.get(Config.SERVER_URL + '/api/v1/deleteannonce/' + id, {
      headers
    }
    ).then((res) => {
      console.log("*****")

      if (res.data == 200) {
        Alert.alert("Votre annonce a été bien supprimé")
        this.getToken().then(token => {
          this.getCars(token);
        });
      }
      else if (res.data == 403) {
        this.props.navigation.navigate("ECommerceMenu")
      }
      else {
        Alert.alert("Votre annonce n'a pas été supprimé , veuillez réssayer plutard")
      }
      console.log(res.data)









    }).catch((err) => {

      this.setState({ loading: false });

    });

  }
  renderItem = ({ item, index }) => {
    swipeoutBtns = [
      {
        text: 'Supprimer',
        backgroundColor:"#e30613",
        onPress:() => this.delete(item.id)
      }
    ]

    return (
      <Swipeout  right={swipeoutBtns}>
      <View style={styles.mainListRenderRow}>

        <View style={styles.row} key={index}>
          <TouchableOpacity
            style={styles.rowBg}

          >
            <CachedImage
              source={{ uri: item.productImage }}
              style={styles.productImage}
            >
              {item.isLike == true ? (
                <TouchableOpacity
                  onPress={() => this._handleLike( item.id)}
                >
                  <FontAwesome
                    name="heart"
                    size={18}
                    color={"red"}
                    style={styles.heartListIcon}
                  />
                </TouchableOpacity>
              ) : (
                  <TouchableOpacity
                    onPress={() => this._handleDisLike(item, item.id, item.like, index)}
                  >
                    <FontAwesome
                      name="heart"
                      size={18}
                      color={"#cecece"}
                      style={styles.heartListIcon}
                    />
                  </TouchableOpacity>
                )}
            </CachedImage>

            <View style={styles.productDetailBg}>
              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={styles.productTitleTxt}>
                  {item.productTitle}
                </Text>

              </View>
              <View
                style={[
                  styles.detailFieldRow,
                  {
                    paddingTop: Metrics.WIDTH * 0.01,
                    paddingBottom: Metrics.WIDTH * 0.01
                  }
                ]}
              >
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                  <Text style={styles.priceTxt}>{item.price}€</Text>
                  <TouchableOpacity
                    style={styles.closeIconBg}
                    onPress={() => this.delete(item.id)}
                  >
                    <FontAwesome
                      name="close"
                      size={12}
                      style={styles.closeImg}
                    />
                  </TouchableOpacity>
                </View>
              </View>







            </View>

          </TouchableOpacity>

        </View>

      </View>
      </Swipeout>
    );
  };
  render() {
    const { screenLink } = this.state;
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#0e1130", true);
      StatusBar.setTranslucent(true);
    }
    if (this.state.loading == true) {
      return (
        <View>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>



          <View style={styles.imageOverlay}>
            <Spinner color="black" />
          </View>

        </View>
      );
    }
    else {


      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <View
            style={{
              backgroundColor: "#fff"
            }}
          >



            <View style={{ backgroundColor: "#e9e9e9" }}>


              <FlatList
                data={this.state.voiture}
                renderItem={this.renderItem}

                keyExtractor={(item, index) => item.id.toString()}
                ItemSeparatorComponent={this.renderSeparator}


                ListEmptyComponent={
                  <View style={{ justifyContent: "center" }}>
                    <Text style={{ alignSelf: "center" }}>
                      No Data Found.
                              </Text>
                  </View>
                }

              />




            </View>


          </View>




        </View>

      );
    }
  }
}
export default withNavigation(CategoryProduct);