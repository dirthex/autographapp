import React, { Component } from 'react';
import Entypo from "react-native-vector-icons/Entypo";
import {
	Text,
	View,
	Image,
	StatusBar,
	TouchableOpacity,
	Platform,
	ImageBackground,
	Linking,
	BackHandler,
	ScrollView,
	ListView,
	I18nManager,
	AsyncStorage
} from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {
	Content,
	Container,
	Button,
	Icon,
	Right,
	Item,
	Input,
	Header,
	Left,
	Body,
	Title,
	Segment,
	Label,
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import { Fonts, Metrics, Colors, Images } from '../../../Themes/';
import { SocialIcon } from 'react-native-elements'
export default class MyAccount extends Component {
	constructor(props) {
		super(props);

		const rowHasChanged = (r1, r2) => r1 !== r2;
		const ds = new ListView.DataSource({ rowHasChanged });

		const dataObjects = [
			{
				id: 1,
				title: 'Details',
				itemImg: Images.ic_details,
			},
			{
				id: 2,
				title: 'Mes annonces',
				itemImg: Images.ic_order,
			},
			{
				id: 3,
				title: 'Déposer une annonce',
				itemImg: Images.ic_address,
			},
			{
				id: 4,
				title: 'Notification',
				notification: '3',
				itemImg: Images.ic_notification,
			},
			{
				id: 5,
				title: 'Code promo',
				itemImg: Images.ic_get_reward,
			},
			{
				id: 6,
				title: 'Logout',
				itemImg: Images.logoutIconc	,
			},
		];

		this.state = {
			dataSource: ds.cloneWithRows(dataObjects),
		};
	}

	componentWillMount() {
		var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('ECommerceMenu');
			return true;
		});
	}

	_handleNotificationBack() {
		AsyncStorage.multiSet([["ArrivedForNotification", "ECommerceMyAccount"]]);
		this.props.navigation.navigate("ECommerceNotification");
	}
	async deleteJWT() {
		try {
		  await AsyncStorage.removeItem('token')
			.then(
			  () => {
				this.setState({
				  token: ''
				})
			  }
			);
		} catch (error) {
		  console.log('AsyncStorage Error: ' + error.message);
		}
	  }

	alertItemName = rowData => {
		var name = rowData.title;

		if (name === 'Details') {
			this.props.navigation.navigate('ECommerceMyInformation');
		} else if (name === 'Mes annonces') {
			this.props.navigation.navigate('mesannonces');
		} else if (name === 'Déposer une annonce') {
			this.props.navigation.navigate('AddAnnonce');
		} else if (name === 'Notification') {
			//this._handleNotificationBack();
		} else if (name === 'Get Reward') {
			this.props.navigation.navigate('ECommerceInviteFriends');
		}else if (name === 'Code promo') {
			this.props.navigation.navigate('CodePromo')
			
		}
		 else if (name === 'Logout') {
			this.deleteJWT().then(this.props.navigation.navigate('ECommerceLogin'))
			
		}
	};

	_handleBagNavigation() {
		AsyncStorage.multiSet([
			['ArrivedFrom', "ECommerceMyAccount"],
		]);
		this.props.navigation.navigate("ECommerceMyBag");
	}

	_handleWishListNavigation() {
		AsyncStorage.multiSet([
			['ArrivedForWishList', "ECommerceMyAccount"],
		]);
		this.props.navigation.navigate("ECommerceWishList");
	}

	_renderItem(rowData) {
		return (
			<TouchableOpacity
				style={styles.rowMain}
				onPress={() => this.alertItemName(rowData)}>
				<View style={styles.imageContainer}>
					<Image source={rowData.itemImg} style={styles.itemImgStyle} />
					{rowData.notification ? (
						<View style={styles.notificationCircle}>
							<Text style={styles.notification}>3</Text>
						</View>
					) : null}
				</View>
				<Text style={styles.itemText}>{rowData.title}</Text>
			</TouchableOpacity>
		);
	}

	onBackClick() {
		this.props.navigation.navigate('ECommerceMenu');
	}

	render() {
		StatusBar.setBarStyle('light-content', true);

		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor('#23292e', true);
			StatusBar.setTranslucent(true);
		}

		return (
			<Container style={styles.container}>
				<Header style={styles.HeaderBg} transparent>
					<Left style={styles.left}>
						<TouchableOpacity
							onPress={() => this.props.navigation.openDrawer()}
						>
							<Entypo name="menu" color="#FFFFFF" size={30} />
						</TouchableOpacity>
					</Left>

					<Body style={styles.body}>
						<Text style={styles.headerTitle}>Mon compte</Text>
					</Body>

					<Right style={styles.right}>
						<SocialIcon
							type='facebook'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.facebook) }}
						/>
						<SocialIcon
							type='instagram'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.instagram) }}
						/>
						<SocialIcon
							type='youtube'
							iconSize={15}
							style={{ height: 30, width: 30 }}
							onPress={() => { Linking.openURL(Config.youtube) }}
						/>
					</Right>
				</Header>


				<View>
					<ListView
						contentContainerStyle={styles.content}
						dataSource={this.state.dataSource}
						renderRow={this._renderItem.bind(this)}
						enableEmptySections
						scrollEnabled={false}
						pageSize={4}
					/>
				</View>
			</Container>
		);
	}
}
