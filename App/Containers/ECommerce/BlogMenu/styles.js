import { Platform, StyleSheet } from "react-native";
import { Metrics, Fonts, Colors } from "../../../Themes";

const styles = StyleSheet.create({


  HeaderBg: {
    backgroundColor: "#000000",
    borderBottomWidth: 1
  },
  myrecipeslistbg: {
    marginHorizontal: 5,
    height: Metrics.HEIGHT * 0.45,
    backgroundColor: "#FFFFFF",
    width: Metrics.WIDTH * 0.5,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 5
  },

  myrecipeslistimagesbg: {
    height: Metrics.HEIGHT * 0.32,
    resizeMode: "contain",
    width: Metrics.WIDTH * 0.5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5
  },
  carsImg: {
    height: Metrics.HEIGHT * 0.32,
    resizeMode: "cover",
    width: Metrics.WIDTH * 0.5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5
  },

  descriptiontext: {
    marginTop: 5,
    marginLeft: 5,
    paddingHorizontal: 5,
    fontFamily: Fonts.type.robotoMedium,
    fontSize: Fonts.moderateScale(16),
    color: "#191919",
    fontWeight: 'bold',
  },
  prix:{
    marginTop: 5,
    marginLeft: 5,
    paddingHorizontal: 5,
    fontFamily: Fonts.type.robotoMedium,
    fontSize: Fonts.moderateScale(12),
    color: "#191919"
  },
  myrecipesbg: {
    height: Metrics.HEIGHT * 0.47,
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 5,
    marginTop: Metrics.HEIGHT * 0.03,
    flexDirection: "row"
  },
  left: {
    flex: 1,
    marginLeft: 5
  },

  body: {
    flex: 3
  },

  right: {
    flex: 1,
    marginRight: 5,
    flexDirection: "row"
  },

  headerTitle: {
    color: "#FFFFFF",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(20),
    textAlign: "left",
    alignSelf: "flex-start"
  },

  tabUnderLine: {
    backgroundColor: "#ffff00",
    height: 2
  },

  tabText: {
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(14)
  },

  topView: {
    backgroundColor: "#000000",
    height: Metrics.HEIGHT * 0.32,
    alignItems: "center",
    justifyContent: "center",
    width: Metrics.WIDTH
  },

  topViewText: {
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(20),
    color: Colors.snow,
    textAlign: "center",
    alignSelf: "center",
    width: Metrics.WIDTH * 0.8
  },


  ImgBG: {
    backgroundColor: "#263238",
    width: Metrics.WIDTH * 0.32,
    height: Metrics.HEIGHT * 0.15
  },
  description:{
    fontFamily: Fonts.type.robotoMedium,
    fontSize: Fonts.moderateScale(10),
    color: "#191919",
    width: Metrics.WIDTH * 0.9,
    marginLeft: Metrics.HEIGHT * 0.02,
  },
 

  TopStoriesTitleText: {
    fontFamily: Fonts.type.robotoMedium,
    fontSize: Fonts.moderateScale(16),
    color: "#191919",
    width: Metrics.WIDTH * 0.5,
    marginLeft: Metrics.HEIGHT * 0.02,
    height: Metrics.HEIGHT * 0.12
  },
  TopStoriesNameText: {
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(12),
    color: "#626262",
    width: Metrics.WIDTH * 0.3,
    marginLeft: Metrics.HEIGHT * 0.02
  },

  TopStoriesTime: {
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(12),
    color: "#626262"
  },

  BorderHorizontal: {
    width: Metrics.WIDTH,
    height: 1,
    backgroundColor: "#e0e0e0"
  },
  

 

  ImageBG: {
    backgroundColor: "#000000",
    height: Metrics.HEIGHT * 0.25,
    resizeMode: "cover",
    width: Metrics.WIDTH * 0.93,
    borderRadius: 2,
    marginTop: Metrics.HEIGHT * 0.002
  },

  SportLifeStyleBG: {
    width: Metrics.WIDTH * 0.25,
    height: Metrics.HEIGHT * 0.04,
    borderRadius: 4,
    marginTop: Metrics.HEIGHT * 0.02,
    marginLeft: Metrics.HEIGHT * 0.02,
    justifyContent: "center"
  },

  RecentUpdateNameText: {
    fontFamily: Fonts.type.robotoMedium,
    fontSize: Fonts.moderateScale(12),
    color: "#fff",
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center"
  },

  RecentUpdateTitleText: {
    marginTop: Metrics.HEIGHT * 0.01,
    marginLeft: Metrics.HEIGHT * 0.02,
    fontFamily: Fonts.type.robotoMedium,
    fontSize: Fonts.moderateScale(18),
    color: "#1d262a",
    width: Metrics.WIDTH * 0.85
  },
  slidesec: {
    height: Metrics.HEIGHT * 0.3,
    width:"100%"
  },
  sliderImage: {
    resizeMode: "contain",
    height: Metrics.HEIGHT * 0.3,
    width: Metrics.WIDTH,
    
    
  },
  sliderImagePub: {
    resizeMode: "contain",
    height: Metrics.HEIGHT * 0.3,
    width: Metrics.WIDTH,
    backgroundColor:"#dddbca"
    
    
  },
  
  itemImage: {
    width: Metrics.WIDTH * 0.445,
    height: Metrics.WIDTH * 0.64
  },

  itemTitle: {
    width: Metrics.WIDTH * 0.445,
    color: "#0e1130",
    fontSize: Fonts.moderateScale(15),
    fontFamily: Fonts.type.helveticaNeueLight,
    marginTop: Metrics.HEIGHT * 0.015,
    textAlign: "left",
    marginLeft: 5
  },

  itemPrice: {
    width: Metrics.WIDTH * 0.445,
    color: "#ff0000",
    fontSize: Fonts.moderateScale(15),
    fontFamily: Fonts.type.helveticaNeueBold,
    marginTop: Metrics.HEIGHT * 0.01,
    marginBottom: Metrics.HEIGHT * 0.01,
    textAlign: "left",
    marginLeft: 5
  },
  newArrivalView: {
    flexDirection: "row",
    padding: Metrics.HEIGHT * 0.01
  },
  
 
  swiperView: {
    height: Metrics.HEIGHT * 0.425,
    backgroundColor: "#FFFFFF",
    shadowOpacity: 0.1,
    marginLeft: Metrics.HEIGHT * 0.01,
    marginRight: Metrics.HEIGHT * 0.01,
    shadowColor: "gray",
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 4,
    elevation: 3,
    borderColor: "#e8e8e8",
    marginBottom: Metrics.HEIGHT * 0.02,
    borderRadius: 2
  },
  TopStoriesTimeRecentUpdate: {
    color: "#000000",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(12),
    marginLeft: Metrics.HEIGHT * 0.002
  },
  menuIcon: {
    width: Metrics.WIDTH * 0.06,
    height: Metrics.WIDTH * 0.06,
    resizeMode: "contain"
  },

  imgContainer: {
    height: Metrics.HEIGHT,
    resizeMode: "cover"
  },

  listProfileContainer: {
    height: Metrics.HEIGHT * 0.9,
    backgroundColor: "transparent"
  },

  profileDataBg: {
    flexDirection: "row",
    marginTop: Metrics.HEIGHT * 0.07,
    alignItems: "center",
    marginLeft: Metrics.WIDTH * 0.08,
    width: Metrics.WIDTH * 0.45,
    alignSelf: "center",
    marginLeft: -(Metrics.WIDTH * 0.02),
    justifyContent: "center"
  },

  profileImg: {
    width: Metrics.WIDTH * 0.15,
    height: Metrics.WIDTH * 0.15,
    borderRadius: Metrics.WIDTH * 0.075
  },

  circles: {
    borderRadius: Metrics.WIDTH * 0.08,
    borderColor: "#fff",
    borderWidth: 2
  },

  nameTxt: {
    textAlign: "center",
    color: "white",
    fontFamily: Fonts.type.sfuiDisplayBold,
    fontSize: Fonts.moderateScale(15),
    marginLeft: -(Metrics.WIDTH * 0.03),
    marginTop: Metrics.WIDTH * 0.05
  },

  addressTxt: {
    textAlign: "center",
    color: "#a6a7ab",
    fontFamily: Fonts.type.sfuiDisplayRegular,
    fontSize: Fonts.moderateScale(13),
    marginLeft: -(Metrics.WIDTH * 0.03)
  },

  bottomViewBg: {
    flexDirection: "row",
    height: Metrics.HEIGHT * 0.1,
    backgroundColor: "#0691ce",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: Metrics.WIDTH * 0.05,
    paddingRight: Metrics.WIDTH * 0.02
  },

  bottomImageDataBg: {
    flexDirection: "row",
    alignItems: "center"
  },

  bottomImage: {
    width: Metrics.WIDTH * 0.12,
    height: Metrics.WIDTH * 0.12,
    borderRadius: Metrics.WIDTH * 0.06,
    borderWidth: 1,
    borderColor: "#fff"
  },

  bottomNameTxt: {
    color: "#919191",
    fontSize: Fonts.moderateScale(13),
    fontFamily: "SFUIDisplay-Regular"
  },

  profileBg: {
    height: Metrics.HEIGHT * 0.28,
    alignItems: "center"
  },
  sliderImage: {
    resizeMode: "contain",
    height: Metrics.HEIGHT * 0.15,
    width: Metrics.WIDTH,
    
    
  },
  settingIcon: {
    width: Metrics.WIDTH * 0.05,
    height: Metrics.WIDTH * 0.05,
    resizeMode: "contain",
    marginLeft: Metrics.WIDTH * 0.06
  },
  slidesec: {
    height: Metrics.HEIGHT * 0.15,
    width:"100%"
  }, 
  slidesecPub: {
    justifyContent:"center",
    height: Metrics.HEIGHT * 0.5,
    width:Metrics.WIDTH 
  },

  bellIcon: {
    color: "#595b6a",
    marginRight: Metrics.WIDTH * 0.06
  },

  cartCountBg: {
    marginTop: -(Metrics.HEIGHT * 0.04),
    marginLeft: -(Metrics.WIDTH * 0.03),
    width: Metrics.WIDTH * 0.04,
    height: Metrics.WIDTH * 0.04,
    borderRadius: Metrics.WIDTH * 0.02,
    backgroundColor: "#ff0000",
    alignItems: "center",
    justifyContent: "center"
  },

  cartItemCount: {
    color: Colors.snow,
    fontSize: Fonts.moderateScale(10),
    fontFamily: Fonts.type.sfuiDisplayRegular
  },

  bottomTxt: {
    color: Colors.snow,
    fontSize: Fonts.moderateScale(22)
  },

  bottomPriceTxt: {
    color: Colors.snow,
    fontSize: Fonts.moderateScale(20)
  },

  menuListSec: {
    height: Metrics.HEIGHT * 0.62
  },

  container: {
    backgroundColor: "#11142a",
    justifyContent: "center",
    alignItems: "center"
  },

  menuStyleSec: {
    backgroundColor: "transparent"
  },

  titleContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  titleCollapse: {
    flexDirection: "row"
  },

  headerTitleMenu: {
    flexDirection: "row",
    alignItems: "center"
  },

  HeaderExpandMenu: {
    paddingTop: Metrics.WIDTH * 0.03,
    paddingBottom: Metrics.WIDTH * 0.03,
    color: Colors.snow,
    fontSize: Fonts.moderateScale(20),
    textAlign: "center"
  },

  mybodyText: {
    paddingTop: 5,
    paddingBottom: 5,
    color: "#adafc1",
    fontSize: Fonts.moderateScale(15),
    textAlign: "center",
    paddingLeft: Metrics.WIDTH * 0.04
  },

  submenutitleSec: {
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "transparent",
    justifyContent: "space-between"
  },

  subTitleTxt: {
    fontSize: Fonts.moderateScale(15),
    width: Metrics.WIDTH * 0.71,
    color: "#adafc1",
    textAlign: "center",
    paddingLeft: Metrics.WIDTH * 0.02
  },

  titleTxt: {
    width: Metrics.WIDTH * 0.71,
    fontSize: Fonts.moderateScale(18),
    color: "white",
    textAlign: "center"
  },

  subTitleBg: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: Metrics.WIDTH * 0.04,
    marginRight: 10
  },

  nextArrow: {
    paddingRight: Metrics.WIDTH * 0.07,
    marginRight: Metrics.WIDTH * 0.07
  },

  searchViewBg: {
    backgroundColor: "#e9e9e9",
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT * 0.08,
    justifyContent: "center",
    alignItems: "center"
  },

  searchView: {
    backgroundColor: Colors.snow,
    borderRadius: 5,
    width: Metrics.WIDTH * 0.95,
    height: Metrics.HEIGHT * 0.06,
    alignItems: "center",
    flexDirection: "row"
  },

  searchText: {
    color: "#8e8e93",
    fontSize: Fonts.moderateScale(15),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    marginLeft: Metrics.WIDTH * 0.03
  },

  coverImageStyle: {
    width: Metrics.WIDTH * 0.5,
    height: Metrics.HEIGHT * 0.3425,
    borderWidth: Fonts.moderateScale(1),
    borderColor: "#fff",
    alignItems: "center"
  },

  categoryBtn: {
    height: Metrics.HEIGHT * 0.04,
    paddingHorizontal: Metrics.WIDTH * 0.025,
    bottom: Metrics.HEIGHT * 0.015,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: "#fff",
    fontSize: Fonts.moderateScale(13),
    fontFamily: Fonts.type.sfuiDisplayLight,
    textAlign: "center"
  },

  headerView: {
    height: Metrics.HEIGHT * 0.3,
    backgroundColor: "#dddbca",
    paddingTop: Metrics.WIDTH * 0.05,
    alignItems: "center",
    paddingLeft: 20,
    flexDirection: "row"
  },

  imageProfile: {
    height: Metrics.WIDTH * 0.35,
    width: Metrics.WIDTH * 0.35,
    marginLeft: Metrics.WIDTH * 0.09
    
  },

  nameText: {
    color: Colors.snow,
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(16),
    fontWeight: "bold"
  },

  nameTextDes: {
    color: Colors.snow,
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(14)
  },

  menuView: {
    height: Metrics.HEIGHT * 0.55,
    marginTop: Metrics.HEIGHT * 0.03
  },

  menuName: {
    color: "#212121",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(15),
    paddingVertical: Metrics.WIDTH * 0.04,
    marginLeft: Metrics.HEIGHT * 0.01
  },
  mainView: {
    flex: 1
  },

  HeaderBg: {
		backgroundColor: "#23292e",
		borderBottomWidth: 1
	  },

  leftheader: {
    flex: 1
  },

  body: {
    flex: 3
  },

  right: {
    flex: 1
  },

  headertitle: {
    textAlign: "center",
    justifyContent: "center",
    alignSelf: "center",
    color: "#ffffff",
    fontWeight: "normal",
    ...Platform.select({
      ios: {
        fontSize: Fonts.moderateScale(14)
      },
      android: {
        fontSize: Fonts.moderateScale(16)
      }
    }),
    fontFamily: Fonts.type.sfuiDisplaySemibold
  },

  MainListBg: {
    backgroundColor: "#f5f5f5",
    flex: 1
  },

  mainListRenderRow: {
    alignSelf: "center",
    backgroundColor: "#fff",
    width: Metrics.WIDTH * 0.93,
    borderRadius: 2,
    marginTop: Metrics.HEIGHT * 0.02,
    shadowColor: "#f9f9f9",
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 2,
    elevation: 2,
    shadowOpacity: 0.1,
    marginBottom: Metrics.HEIGHT * 0.01
  },

  Foodimg: {
    width: Metrics.WIDTH * 0.93,
    ...Platform.select({
      ios: {
        height: Metrics.HEIGHT * 0.22,
        borderRadius: 2
      },
      android: {
        height: Metrics.HEIGHT * 0.3,
        borderRadius: 2
      }
    })
  },
  slidpic: {
    resizeMode:"contain",
    width: Metrics.WIDTH * 0.93,
    ...Platform.select({
      ios: {
        height: Metrics.HEIGHT * 0.22,
        borderRadius: 2
      },
      android: {
        height: Metrics.HEIGHT * 0.5,
        borderRadius: 2
      }
    })
  },

  FoodDetailsText: {
    color: "#262628",
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    fontSize: Fonts.moderateScale(16),
    marginTop: Metrics.HEIGHT * 0.02,
    marginLeft: Metrics.HEIGHT * 0.02
  },
  pubtext:{
    textAlign:"center",
    color: "#262628",
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    fontSize: Fonts.moderateScale(16),
    marginTop: Metrics.HEIGHT * 0.01,
    marginBottom:Metrics.HEIGHT * 0.01,
    paddingRight:50
  },

  FoodANameText: {
    color: "#000000",
    fontFamily: Fonts.type.sfuiDisplayRegular,
    fontSize: Fonts.moderateScale(10),
    marginLeft: Metrics.HEIGHT * 0.02
  },

  ratingStar: {
    height: Metrics.HEIGHT * 0.025,
    width: Metrics.HEIGHT * 0.025,
    marginLeft: Metrics.HEIGHT * 0.01
  },

  reviewText: {
    color: "#d4d6da",
    fontFamily: Fonts.type.sfuiDisplayRegular,
    fontSize: Fonts.moderateScale(14),
    marginLeft: Metrics.HEIGHT * 0.01
  },

  hearticon: {
    position: "absolute",
    top: 0,
    alignSelf: "flex-end"
  },
  itemHeader: {
    height: 35,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ccc',
    backgroundColor: 'blue',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
},
item: {
    height: 60,
    //borderBottomWidth: StyleSheet.hairlineWidth,
    //borderBottomColor: '#ccc',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
},

contentContainer: {
    paddingTop: 20 + 44,
},

thumbnail: {
    padding: 6,
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ccc',
    overflow: 'hidden',
},

textContainer: {
    padding: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
}



});

export default styles;
