import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Text,
  Image,
  TouchableOpacity,
  BackHandler,
  Linking,
  ScrollView,
  ListView,
  ImageBackground,
  Container,
  AsyncStorage,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  TouchableWithoutFeedback
} from "react-native";
import Moment from 'moment';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Swiper from "react-native-swiper";
import { Header, Right, Left, Body, Content, Spinner } from "native-base";
import { Metrics, Colors, Fonts, Images } from "../../../Themes";
import styles from "./styles";
import { CachedImage } from "react-native-cached-image";
import Annonces from "../Annonces/index";
import Blog from "../Blogs/index";

import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "../../../Components/react-native-scrollable-tab-view";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import Config from "../Config/config";
import axios from "axios";


import { SocialIcon } from 'react-native-elements'


//const rowHasChanged = (r1, r2) => r1 !== r2;
//const ds = new ListView.DataSource({ rowHasChanged });


dataSourceObjects = [];


export default class BlogMenu extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = {

      //dataSourceRecentUpdate: ds.cloneWithRows(RecentUpdate),
      blogs: [],
      blog: [],
      blogsMoto: [],
      blogMoto: [],
      blogsBateau: [],
      blogBateau: [],
      loading: false, // user list loading
      isRefreshing: false,
      numPage: "",
      isLoading: true,
      pub: [],
      pubs: [],
      firstImage: null,
      pubHaut: [],
      pubMilieu: [],
      pubMilieuS: [],
    };
    this.props.navigation.addListener('willFocus', () => {

      //  this.getBlog();
      // this.getMagazine();
      //this.getOffre();

    });
    //this.headerAnnonce();



  }
  getPub(page) {
    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/voiture'
    ).then((res) => {


      this.setState({
        pub: res.data.hautSite.fulfillmentValue.related_pub,
        pubHaut: res.data.basSite.fulfillmentValue.related_pub,
        pubMilieu: res.data.pub,

        loading: false,
      })

      
      
      if (this.state.pub && this.state.pub[0].image) {
        this.state.pubs.push({
          image: Config.SERVER_URL + this.state.pub[0].image,
          lien: this.state.pub[0].lien
        })
      }
      if (this.state.pubHaut && this.state.pubHaut[0].image) {
        this.state.pubs.push({
          image: Config.SERVER_URL + this.state.pubHaut[0].image,
          lien: this.state.pubHaut[0].lien
        })
      }

      for (var i = 0; i < this.state.pubMilieu.length; i++) {

        if (this.state.pubMilieu[i].type != "video") {

          this.state.pubMilieuS.push({
            related_pub : this.state.pubMilieu[i].related_pub
          })
        }
      }
      console.log("sous liste" + this.state.pubMilieuS)


      this.setState({ isLoading: false })

    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  getBlog(page) {
    console.log("dkhel 622")

    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/voiture'
    ).then((res) => {

      this.setState({

        blogs: res.data.blogs,


        loading: false,



      });



      for (var i = 0; i < this.state.blogs.length; i++) {
        this.state.blog.push({
          id: this.state.blogs[i].id,
          FoodName: this.state.blogs[i].titre,
          Foodimg: Config.SERVER_URL + this.state.blogs[i].cover,
          Description: this.state.blogs[i].resume
        })
      }



      this.setState({ isLoading: false })








    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
    console.log("first imafe" + JSON.stringify(this.state.firstImage))

  }
  onRefresh() {
    this.setState({ isRefreshing: true })
    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/voiture'
    ).then((res) => {
      this.setState({

        blogs: res.data.blogs,
        isRefreshing: false,


      });



      for (var i = 0; i < this.state.blogs.length; i++) {
        this.state.blog.push({
          id: this.state.blogs[i].id,
          FoodName: this.state.blogs[i].titre,
          Foodimg: Config.SERVER_URL + this.state.blogs[i].cover,
          Description: this.state.blogs[i].resume
        })
      }









    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
  }

  handleLoadMore = () => {
    if (!this.state.loading) {
      this.page = this.page + 1; // increase page by 1
      // method for API call 
      console.log("dkhel 622")

      this.getBlog(this.page);


    }
  };
  getBlogMoto(page) {
    console.log("dkhel 622")

    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/moto'
    ).then((res) => {
      this.setState({

        blogsMoto: res.data.blogs,
        loading: false,



      });


      for (var i = 0; i < this.state.blogsMoto.length; i++) {
        this.state.blogMoto.push({
          id: this.state.blogsMoto[i].id,
          FoodName: this.state.blogsMoto[i].titre,
          Foodimg: Config.SERVER_URL + this.state.blogsMoto[i].cover,
          Description: this.state.blogsMoto[i].resume
        })
      }


      this.setState({ isLoading: false })






    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  onRefreshMoto() {
    this.setState({ isRefreshing: true })
    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/moto'
    ).then((res) => {
      this.setState({

        blogsMoto: res.data.blogs,
        isRefreshing: false,


      });



      for (var i = 0; i < this.state.blogsMoto.length; i++) {
        this.state.blogMoto.push({
          id: this.state.blogsMoto[i].id,
          FoodName: this.state.blogsMoto[i].titre,
          Foodimg: Config.SERVER_URL + this.state.blogsMoto[i].cover,
          Description: this.state.blogsMoto[i].resume
        })
      }









    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
  }

  handleLoadMoreMoto = () => {
    if (!this.state.loading) {
      this.page = this.page + 1; // increase page by 1
      // method for API call 
      console.log("dkhel 622")

      this.getBlogMoto(this.page);


    }
  };
  getBlogBateau(page) {
    console.log("dkhel 622")

    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/bateau'
    ).then((res) => {
      this.setState({

        blogsBateau: res.data.blogs,
        loading: false,



      });


      for (var i = 0; i < this.state.blogsBateau.length; i++) {
        this.state.blogBateau.push({
          id: this.state.blogsBateau[i].id,
          FoodName: this.state.blogsBateau[i].titre,
          Foodimg: Config.SERVER_URL + this.state.blogsBateau[i].cover,
          Description: this.state.blogsBateau[i].resume
        })
      }



      this.setState({ isLoading: false })





    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  onRefreshBateau() {
    this.setState({ isRefreshing: true })
    axios.get(Config.SERVER_URL + '/api/v1/blogs/' + this.page + '/bateau'
    ).then((res) => {
      this.setState({

        blogsBateau: res.data.blogs,
        isRefreshing: false,


      });



      for (var i = 0; i < this.state.blogsBateau.length; i++) {
        this.state.blogBateau.push({
          id: this.state.blogsBateau[i].id,
          FoodName: this.state.blogsBateau[i].titre,
          Foodimg: Config.SERVER_URL + this.state.blogsBateau[i].cover,
          Description: this.state.blogsBateau[i].resume
        })
      }









    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
  }

  handleLoadMoreBateau = () => {
    if (!this.state.loading) {
      this.page = this.page + 1; // increase page by 1
      // method for API call 
      console.log("dkhel 622")

      this.getBlogBateau(this.page);


    }
  };
  NavigateToBlogDetails(item) {
    AsyncStorage.multiSet([["ArrivedFrom", "BlogMenu"]]);
    this.props.navigation.navigate("BlogDetails", {
      id: item.id,
      screen: ''
    })
  }
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: '100%',
          backgroundColor: '#CED0CE'
        }}
      />
    );
  };

  componentDidMount() {

    const { navigation } = this.props;

    this.state.numPage = navigation.getParam('numero', '');
    console.log("nuuuum page" + this.state.numPage);

    this.getBlog(this.page);
    this.getBlogMoto(this.page);
    this.getBlogBateau(this.page);
    this.getPub(this.page);


    BackHandler.addEventListener('hardwareBackPress', () => {

      this.props.navigation.navigate("ECommerceMenu");
      return true;


    });
  }
  componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);



  }


  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.removeItem("header");
  }

  backPressed = () => {
    this.props.navigation.navigate("NewsCategory");
    return true;
  };


  renderItemVoiture = ({ item, index }) => {
    console.log("********************" + index )
    var that = this;
    var pub = [];
    if (this.state.pubMilieuS.length >= index + 1)
      pub = this.state.pubMilieuS[index].related_pub;

    console.log(pub + "**********" +this.state.pubMilieuS.length)

    return (
      <View>
      <View style={styles.mainListRenderRow}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate("BlogDetails", {
          id: item.id,
          screen: 'voiture'
        })}>
          <View style={styles.Foodimg}>
            <Image source={{ uri: item.Foodimg }} style={styles.Foodimg} />


          </View>
          <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
          <Text style={styles.FoodANameText}>{item.Description} </Text>
          <Text style={styles.FoodANameText}> </Text>

        </TouchableOpacity>

      </View>
      {pub.length > 0 ?(
        <View style={styles.mainListRenderRow}>
      <View style={styles.slidesecPub}>
        
        <Swiper

          showsButtons={false}
          autoplay={true}
          autoplayTimeout={2.5}
          activeDot={<View style={styles.activeDot} />}
          dot={<View style={styles.dot} />}
        >
          
          {pub.map((item, index) => {

            

            return (
              <TouchableOpacity   onPress={() => { Linking.openURL(item.lien) }}>
              <View style={styles.slide} key={index}>

                <Image source={{ uri:Config.SERVER_URL + item.image }} style={styles.slidpic} />

              </View>
              </TouchableOpacity>

            );
          })}
        </Swiper>
        <Text style={styles.pubtext}>PUBLICITÉ</Text>
         
      </View>
      </View>
      ):null}
      
</View>

    );
  };
  renderItemMoto = ({ item, index }) => {
    var that = this;

    return (
      <View style={styles.mainListRenderRow}>
        <View style={styles.Foodimg}>
          <Image source={{ uri: item.Foodimg }} style={styles.Foodimg} />


        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate("BlogDetails", {
          id: item.id,
          screen: 'moto'
        })}>
          <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
          <Text style={styles.FoodANameText}>{item.Description} </Text>
          <Text style={styles.FoodANameText}> </Text>

        </TouchableOpacity>

      </View>

    );
  };
  renderItemBateau = ({ item, index }) => {
    var that = this;

    return (
      <View style={styles.mainListRenderRow}>
        <View style={styles.Foodimg}>
          <Image source={{ uri: item.Foodimg }} style={styles.Foodimg} />


        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate("BlogDetails", {
          id: item.id,
          screen: 'bateau'
        })}>
          <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
          <Text style={styles.FoodANameText}>{item.Description} </Text>
          <Text style={styles.FoodANameText}> </Text>

        </TouchableOpacity>

      </View>

    );
  };




  navigateToFiltre() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate(
      "Filter",
      {
        searchTxt: ""
      }
    );

  }
  headerAnnonce() {
    AsyncStorage.multiSet([["header", "Annonce"]]);
  }

  render() {

    if (this.state.isLoading == true) {
      return (
        <View>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>



          <View style={styles.imageOverlay}>
            <Spinner color="black" />
          </View>

        </View>
      );
    }

    else if (this.state.numPage == "1") {
      { console.log("dkhhel 1") }
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={1}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Voitures">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>
                  {this.state.pubs.length > 0 ? (
                    <View style={styles.slidesec}>

                      <Swiper

                        showsButtons={false}
                        autoplay={true}
                        autoplayTimeout={2.5}
                        activeDot={<View style={styles.activeDot} />}
                        dot={<View style={styles.dot} />}

                      >


                        {this.state.pubs.map((item, index) => {



                          return (
                            <TouchableWithoutFeedback onPress={() => { Linking.openURL(item.lien) }}>
                              <View style={styles.slide} key={index}>

                                <Image source={{ uri: item.image }} style={styles.sliderImage} />

                              </View>
                            </TouchableWithoutFeedback>
                          );

                        })}
                      </Swiper>

                    </View>
                  ) : null}
                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blog}
                        renderItem={this.renderItemVoiture}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}

                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>
            </View>
            <View tabLabel={'Motos'}>

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blogMoto}
                        renderItem={this.renderItemMoto}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefreshMoto.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        //ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMoreMoto.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>


            </View>
            <View tabLabel={'Bateaux'}>
              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blogBateau}
                        renderItem={this.renderItemBateau}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefreshBateau.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        //ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMoreBateau.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>

            </View>

          </ScrollableTabView>
        </View>
      );
    }
    else if (this.state.numPage == "2") {
      { console.log("dkhhel 2") }
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>


            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={2}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Voitures">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  {this.state.pubs.length > 0 ? (
                    <View style={styles.slidesec}>

                      <Swiper

                        showsButtons={false}
                        autoplay={true}
                        autoplayTimeout={2.5}
                        activeDot={<View style={styles.activeDot} />}
                        dot={<View style={styles.dot} />}
                        onPress={() => { Linking.openURL(Config.facebook) }}
                      >


                        {this.state.pubs.map((item, index) => {



                          return (
                            <TouchableWithoutFeedback onPress={() => { Linking.openURL(item.lien) }}>
                              <View style={styles.slide} key={index}>

                                <Image source={{ uri: item.image }} style={styles.sliderImage} />

                              </View>
                            </TouchableWithoutFeedback>

                          );
                        })}
                      </Swiper>

                    </View>
                  ) : null}

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blog}
                        renderItem={this.renderItemVoiture}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}

                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>
            </View>
            <View tabLabel={'Motos'}>

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blogMoto}
                        renderItem={this.renderItemMoto}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefreshMoto.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        //ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMoreMoto.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>


            </View>
            <View tabLabel={'Bateaux'}>
              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blogBateau}
                        renderItem={this.renderItemBateau}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefreshBateau.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        //ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMoreBateau.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>

            </View>

          </ScrollableTabView>
        </View>
      );
    }
    else {
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>

            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={0}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Voitures">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>
                  {this.state.pubs.length > 0 ? (
                    <View style={styles.slidesec}>

                      <Swiper

                        showsButtons={false}
                        autoplay={true}
                        autoplayTimeout={2.5}
                        activeDot={<View style={styles.activeDot} />}
                        dot={<View style={styles.dot} />}

                      >



                        {this.state.pubs.map((item, index) => {



                          return (
                            <TouchableWithoutFeedback onPress={() => { Linking.openURL(item.lien) }}>
                              <View style={styles.slide} key={index}>

                                <Image source={{ uri: item.image }} style={styles.sliderImage} />

                              </View>
                            </TouchableWithoutFeedback>

                          );
                        })}
                      </Swiper>

                    </View>
                  ) : null}
                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blog}
                        renderItem={this.renderItemVoiture}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}

                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>
            </View>
            <View tabLabel={'Motos'}>

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blogMoto}
                        renderItem={this.renderItemMoto}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefreshMoto.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        //ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMoreMoto.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>


            </View>
            <View tabLabel={'Bateaux'}>
              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.mainView}>


                    <View style={styles.MainListBg}>

                      <FlatList
                        contentContainerStyle={styles.listContent}

                        data={this.state.blogBateau}
                        renderItem={this.renderItemBateau}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefreshBateau.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        //ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMoreBateau.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>

                            </Text>
                          </View>
                        }

                      />




                    </View>

                  </View>
                </ScrollView>

              </View>

            </View>

          </ScrollableTabView>
        </View>
      );
    }



  }
}

