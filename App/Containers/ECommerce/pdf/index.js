import React from 'react';
import { StyleSheet, Linking, Dimensions, View, TouchableOpacity, Text, AsyncStorage, BackHandler } from 'react-native';
import Entypo from "react-native-vector-icons/Entypo";
import Pdf from 'react-native-pdf';
import styles from "./styles";
import { Images, Metrics, Fonts, Colors } from "../../../Themes/";
import {
  Content,
  Container,
  Right,
  Header,
  Left,
  Body,
  Title,
  Spinner,

} from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { SocialIcon } from 'react-native-elements'
export default class PDFExample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pdf: "",
      titre: "",
      ecran: "",
      pageScroll: "",
      numPage: ""


    };
    this.props.navigation.addListener('willFocus', () => {

      const { navigation } = this.props;
      // this.state.pdf = navigation.getParam('pdf', '136');
      this.setState({ pdf: navigation.getParam('pdf', null) })
      this.state.titre = navigation.getParam('titre', null);
      this.state.numPage = navigation.getParam('screen', null)
      //  this.getBlog();
      // this.getMagazine();
      //this.getOffre();

    });
  }
  componentWillMount() {
    AsyncStorage.setItem("scroll", "page");

    AsyncStorage.multiGet([
      "ArrivedFrom",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("dta" + data[0][1]);

      this.setState({
        ecran: data[0][1],

      });
    })
    BackHandler.addEventListener('hardwareBackPress', () => {
      //  AsyncStorage.multiSet([["srcoll", "page"]]);

      if (this.state.ecran == "home")
        this.props.navigation.navigate("ECommerceMenu", {
          page: this.state.ecran
        });
      if (this.state.ecran == "Magazine")
        this.props.navigation.navigate("magazine");
      return true;

    });
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.removeItem("ArrivedFrom");

  }
  render() {


    const source = { uri: this.state.pdf, cache: true };
    console.log("ahhahaha" + this.state.pdf)
    //const source = require('./test.pdf');  // ios only
    //const source = {uri:'bundle-assets://test.pdf'};

    //const source = {uri:'file:///sdcard/test.pdf'};
    //const source = {uri:"data:application/pdf;base64,..."};

    return (
      <View style={styles.mainview} >
        <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
          <Left style={styles.left}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Entypo name="menu" color="#FFFFFF" size={30} />
            </TouchableOpacity>
          </Left>

          <Right style={styles.right}>
            <SocialIcon
              type='facebook'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.facebook) }}
            />
            <SocialIcon
              type='instagram'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.instagram) }}
            />
            <SocialIcon
              type='youtube'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.youtube) }}
            />
          </Right>
        </Header>
        <View style={styles.likeview}>
          <Text style={styles.fashion}>
            {this.state.titre}
          </Text>
        </View>
        <View style={{ height: "1%", backgroundColor: Colors.white }}></View>
        <Pdf
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }}
          onError={(error) => {
            console.log(error);
          }}
          style={styles.pdf} />

      </View>
    )
  }
}
