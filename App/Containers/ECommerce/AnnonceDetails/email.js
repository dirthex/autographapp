import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StatusBar,
    Linking,
    TouchableOpacity,
    Platform,
    Button,
    BackHandler,
    Alert,

} from 'react-native';
import { TextField } from 'react-native-material-textfield';

import Entypo from "react-native-vector-icons/Entypo";

import {
    Content,
    Container,


    Right,

    Header,
    Left,

} from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import { Fonts, Metrics, Colors, Images } from '../../../Themes/';

import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import Config from "../Config/config";
import axios from "axios";

import { SocialIcon } from 'react-native-elements'



export default class email extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            message: "",
            idEmail: "",
        }
        const { navigation } = this.props;
        this.props.navigation.addListener('willFocus', () => {

            this.state.idEmail = navigation.getParam('id', null);

            console.log("scrididididideeName" + this.state.idEmail)

            //this.getblogs(id);
        })
    }
    openDialog = (show) => {
		this.setState({ showDialog: show });
	}

	openConfirm = (show) => {
		this.setState({ showConfirm: show });
	}

	openProgress = () => {
		this.setState({ showProgress: true });

		setTimeout(
			() => {
				this.setState({ showProgress: false });
			},
			4000,
		);
	}

	optionYes = () => {
		this.openConfirm(false);
		// Yes, this is a workaround :(
		// Why? See this https://github.com/facebook/react-native/issues/10471
		setTimeout(
			() => {
				Alert.alert("The YES Button touched!");
			},
			300,
		);
	}

	optionNo = () => {
		this.openConfirm(false);
		// Yes, this is a workaround :(
		// Why? See this https://github.com/facebook/react-native/issues/10471
		setTimeout(
			() => {
				Alert.alert("The NO Button touched!");
			},
			300,
		);
	}
    _renderFooter = () => {


        // Sign Up

        return (
            <TouchableOpacity
                style={styles.facebookBtnBg}
                onPress={() => this.sendemail()}>
                <Text style={styles.loginWithFacebookTxt}>Envoyer votre email</Text>
            </TouchableOpacity>
        );

    };
    componentWillUnmount(){
        BackHandler.addEventListener('hardwareBackPress', function () {
			this.props.navigation.navigate('AnnonceDetails',{
                idAnnonce:this.state.idEmail
            });
			return true;
		});
    }
    sendemail() {
        if(this.state.email =="" || this.state.description ==""){
            this.openDialog(true)
        }
        else{
        axios.post(Config.SERVER_URL + '/api/v1/sendEmailAnnonce', {
            email: this.state.email,
            url: Config.SERVER_URL + '/api/v1/annonce/' + this.state.idEmail,
            id: this.state.idEmail,
            text: this.state.description
        }).then(response => {
            console.log("********"+response.data)
            console.log("********"+response.status)
            if(response.status=="200"){
                Alert.alert("Un email a été envoyé.")
            }
            else {
                Alert.alert("Une erreur est survenue.Veuillez réssayer plutard")
            }
        }).catch((error) => {
            console.log(error)
        })
    }
    }
    _renderActiveComponent = () => {
        const { segment } = this.state;

        // Login

        // SignUp

        return (


            <View style={{ width: Metrics.WIDTH * 0.8, alignSelf: 'center' }}>
                <View style={{ height: 2 }} />

                <TextField
                    containerStyle={styles.formInput}
                    textColor={'#959595'}
                    lineWidth={1}
                    tintColor={'#959595'}
                    baseColor={'#959595'}
                    label="Email"
                    value={this.state.email}
                    keyboardType="default"
                    returnKeyType="done"
                    maxLength={20}

                    selectionColor={'#0000ff'}
                    autoCapitalize="none"
                    onChangeText={email => this.setState({ email })}
                />


                <View style={{ height: 10 }} />

                <TextField
                    containerStyle={styles.formInput}
                    textColor={'#959595'}
                    lineWidth={1}
                    tintColor={'#959595'}
                    baseColor={'#959595'}
                    label="Description"
                    multiline={true}
                    keyboardType="default"
                    returnKeyType="done"
                    maxLength={35}
                    selectionColor={'#959595'}



                    onChangeText={description =>
                        this.setState({ description })
                    }
                />
            </View>
        );



    };

    render() {


        return (
            <Container style={styles.container}>
                <Header style={styles.HeaderBg} transparent>
                    <Left style={styles.left}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.openDrawer()}
                        >
                            <Entypo name="menu" color="#FFFFFF" size={30} />
                        </TouchableOpacity>
                    </Left>


                    <Right style={styles.right}>
                        <SocialIcon
                            type='facebook'
                            iconSize={15}
                            style={{ height: 30, width: 30 }}
                            onPress={() => { Linking.openURL(Config.facebook) }}
                        />
                        <SocialIcon
                            type='instagram'
                            iconSize={15}
                            style={{ height: 30, width: 30 }}
                            onPress={() => { Linking.openURL(Config.instagram) }}
                        />
                        <SocialIcon
                            type='youtube'
                            iconSize={15}
                            style={{ height: 30, width: 30 }}
                            onPress={() => { Linking.openURL(Config.youtube) }}
                        />
                    </Right>
                </Header>


                <KeyboardAwareScrollView>
                    <View style={styles.contentOne}>




                        {this._renderActiveComponent()}

                    </View>

                    {}

                </KeyboardAwareScrollView>



                <View style={styles.bottomView}>{this._renderFooter()}</View>
                <Dialog
						title="Erreur"
						animationType="fade"
						contentStyle={
							{
								alignItems: "center",
								justifyContent: "center",
							}
						}
						onTouchOutside={() => this.openDialog(false)}
						visible={this.state.showDialog}
					>
						<Image
							source={

								Images.croix

							}
							style={
								{
									width: 99,
									height: 87,
									backgroundColor: "black",
									marginTop: 10,
									resizeMode: "contain",
									backgroundColor: "white"
								}
							}
						/>

						<Text style={{ marginVertical: 30 }}>
							Tous les champs sont obligatoires
					    </Text>

						<Button
							onPress={() => this.openDialog(false)}
							style={{ marginTop: 10 }}
							title="CLOSE"
						/>


					</Dialog>


				

            </Container>
        );
    }
}