import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  StyleSheet,
  Platform,
  ImageBackground,
  TextInput,
  I18nManager,
  Dimensions,
  AsyncStorage,
  ScrollView,

} from "react-native";
import ViewMoreText from 'react-native-view-more-text';
import call from 'react-native-phone-call'
import Swiper from "react-native-swiper";
import axios from "axios";
import { Container, Spinner } from "native-base";
import styles from "./styles";
import { Metrics, Fonts, Colors, Images } from "../../../Themes";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import Ionicons from "react-native-vector-icons/Ionicons";
import Fontisto from "react-native-vector-icons/Fontisto";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import { CachedImage } from "react-native-cached-image";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Config from "../Config/config";
import { WebView } from "react-native-webview";
import EvilIcons from "react-native-vector-icons/EvilIcons";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;



export default class RecipesFavorite extends Component {
  constructor(props) {

    super(props);
    this.state = {
      id: "",
      model: "",
      carburant: "",
      boite: "",
      kilometrage: "",
      annee: "",
      cylendree: "",
      prixunique: "",
      prixGuadloupe: "",
      prixMartinique: "",
      prixGuyan: "",
      description: "",
      titre: "",
      categorie: "",
      lat: "",
      long: "",
      principalPhoto: "",
      photos: [],
      marque: "",
      ecran: "",
      screenName: "",
      details: {},
      isLoading: false,
      photosAnnonce: [],
      resultrelative: [],
      nomEtablissement: "",
      imageEtablissement: "",
      lien: "",
      siren: "",
      siret: "",
      tel: "",
      email: "",
      
    };
    const { navigation } = this.props;
    this.props.navigation.addListener('willFocus', () => {
      var idAnnonce = navigation.getParam('idAnnonce', null);
      this.state.idEmail = navigation.getParam('idAnnonce', null);
      this.state.screenName = navigation.getParam('screenName',null);
    console.log("screeName" + this.state.screenName)
      this.getAnnonceDetail(idAnnonce);

      //this.getblogs(id);
    })

    
    // this.state.details = navigation.getParam('item', null);
    
    this.setState({ details: navigation.getParam('item', null) });


  }
  componentDidMount() {
    AsyncStorage.multiGet([
      "ArrivedFrom",

    ]).then(data => {


      this.setState({
        ecran: data[0][1],

      });
    })
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.state.screenName == "home") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (this.state.screenName == "annonce") {
        this.props.navigation.navigate("Annonces");
      }


      return true;

    });
  }
  componentWillUnmount() {
    //AsyncStorage.multiSet([["ArrivedFrom", "AnnonceDetails"]]);

  }
  /*
    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
    }
    */
  navigateToUri() {
    this.props.navigation.navigate("webview", {
      lien: this.state.lien,
    });

  }
  getAnnonceDetail(idAnnonce) {
    console.log("************"+idAnnonce);
    this.setState({
      isLoading: true,
      photosAnnonce: [],
      id: "",
      model: "",
      carburant: "",
      boite: "",
      kilometrage: "",
      annee: "",
      cylendree: "",
      prixunique: "",
      prixGuadloupe: "",
      prixMartinique: "",
      prixGuyan: "",
      description: "",
      titre: "",
      lat: 37.814376,
      long:  -122.419728,
      principalPhoto: "",
      photos: [],
      marque: "",
      resultrelative: [],
      nomEtablissement: "",
      imageEtablissement: "",
      lien: "",
      siren: "",
      siret: "",
      tel: "",
      email: "",

    })
    axios.get(Config.SERVER_URL + '/api/v1/annonce/' + idAnnonce
    ).then((res) => {
      console.log("****************")
      console.log({res})
      this.setState({

        id: res.data.annonce.id,
        model: res.data.annonce.model,
        carburant: res.data.annonce.carburant,
        boite: res.data.annonce.boite,
        kilometrage: res.data.annonce.kilometrage,
        annee: res.data.annonce.annee,
        cylendree: res.data.annonce.cylendree,
        prixunique: res.data.annonce.prixunqiue,
        prixGuadloupe: res.data.annonce.prixGuadloupe,
        prixMartinique: res.data.annonce.prixMartinique,
        prixGuyan: res.data.annonce.prixGuyan,
        description: res.data.annonce.description,
        titre: res.data.annonce.titre,
        lat: res.data.annonce.lat,
        long: res.data.annonce.long,
        principalPhoto: res.data.annonce.principalPhoto,
        photos: res.data.secimages,
        marque: res.data.marqueNom.nom,
        resultrelative: res.data.resultrelative,
        nomEtablissement: res.data.etablissement.nom,
        imageEtablissement: Config.SERVER_URL + res.data.etablissement.image,
        lien: res.data.etablissement.lien,
        categorie:res.data.annonce.categorie
      });
      
      console.log("usseer details" + this.state.description + this.state.titre)
      if (res.data.annonce.user.type == 2) {
        this.setState({
          siren: res.data.annonce.user.related_concess.siren,
          siret: res.data.annonce.user.related_concess.siret,
          tel: res.data.annonce.user.related_concess.tel,
          email: res.data.annonce.user.related_concess.email,
          avatar: Config.SERVER_URL + res.data.annonce.user.related_concess.avatar,
        })
      }
      else {
        this.setState({
          tel: res.data.annonce.user.tel,
          email: res.data.annonce.user.email,
          avatar: Config.SERVER_URL + res.data.annonce.user.avatar,
        })
      }

      for (var i = 0; i < this.state.photos.length; i++) {
        if(this.state.photos[i].includes("https")){
        this.state.photosAnnonce.push({
          img: this.state.photos[i]
        })
      } else {
        this.state.photosAnnonce.push({
          img:Config.SERVER_URL + this.state.photos[i]
        })
      }
      }


      this.setState({ isLoading: false })





    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  renderViewMore(onPress) {
    return (
      <Text style={styles.lireLasuite} onPress={onPress}>Lire la suite</Text>
    )
  };
  renderViewLess(onPress) {
    return (
      <Text style={styles.lireLasuite2} onPress={onPress}>Reduire</Text>
    )
  };
 
  makeCall = (number) => {
    const args = {
      number: number, // String value with the number to call
      prompt: true // Optional boolean property. Determines if the user should be prompt prior to the call 
    }
    call(args).catch(console.error)
  }

  componentWillMount() {
  
  }



  backPressed() {
    if (this.state.ecran == "Menu") {
      this.props.navigation.navigate("ECommerceMenu");
    }
    if (this.state.ecran == "Annonce") {
      this.props.navigation.navigate("Annonces");
    }

  }
  render() {
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#000000', true);
      StatusBar.setTranslucent(true);
    }
  
    if (this.state.isLoading == true) {

      return (
        <View style={styles.imageOverlay}>
          <Spinner color="black" />
        </View>
      );
    } 
    else{
      return (
        <View style={styles.mainView}>
          <Container>


            <View style={{ flex: 1 }}>
              <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={true}
                enableAutomaticScroll={true}
                enableAutoAutomaticScrol="true"
                enableOnAndroid={true}
                extraHeight={80}
                extraScrollHeight={80}
              >

                <View style={styles.slidesec}>

                  <Swiper

                    showsButtons={false}
                    autoplay={true}
                    autoplayTimeout={2.5}
                    activeDot={<View style={styles.activeDot} />}
                    dot={<View style={styles.dot} />}
                  >

                    {this.state.photosAnnonce.map((item, index) => {

                      console.log("iteeeeeeeeeem" + item.img)

                      return (
                        <View style={styles.slide} key={index}>

                          <Image source={{ uri: item.img }} style={styles.sliderImage} />

                        </View>

                      );
                    })}
                  </Swiper>

                </View>

                <View>
                  <Text style={styles.FoodName}>
                    {this.state.titre}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginLeft: Metrics.HEIGHT * 0.03,
                      marginTop: Metrics.HEIGHT * 0.01
                    }}
                  >
                    <Text style={styles.FoodNameText}>{this.state.prixunique}€</Text>

                  </View>
                  <View
                    style={[
                      styles.DividerHorizontal,
                      { marginTop: Metrics.HEIGHT * 0.02 }
                    ]}
                  />
                  <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>


                    <Text style={styles.FoodNameTextTitle}>Critères</Text>
                   {this.state.categorie == "voiture" ?(
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                      <View style={{ flexDirection: 'column', flex: 1 }}>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.MarqueTitle}>Marque</Text>
                          <Text style={styles.details}>{this.state.marque}</Text>
                        </View>

                        <View style={{ marginTop: Metrics.HEIGHT * 0.0002 }}>
                          <Text style={styles.MarqueTitle}>Année-Modele</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.details}>{this.state.annee}</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.MarqueTitle}>Carburant</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.details}>{this.state.carburant}</Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'column', flex: 1, alignSelf: "flex-end" }}>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.MarqueTitle}>Modele</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.details}>{this.state.model}</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.MarqueTitle}>Kilometrage</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.details}>{this.state.kilometrage}</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.MarqueTitle}>Boite à vitesse</Text>
                        </View>
                        <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                          <Text style={styles.details}>{this.state.boite}</Text>
                        </View>
                      </View>

                    </View>
                   ):this.state.categorie =="moto"?(
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                    <View style={{ flexDirection: 'column', flex: 1 }}>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.MarqueTitle}>Marque</Text>
                        <Text style={styles.details}>{this.state.marque}</Text>
                      </View>

                      <View style={{ marginTop: Metrics.HEIGHT * 0.0002 }}>
                        <Text style={styles.MarqueTitle}>Année-Modele</Text>
                      </View>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.details}>{this.state.annee}</Text>
                      </View>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.MarqueTitle}>Cylindre</Text>
                      </View>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.details}>{this.state.cylendree}</Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: "flex-start" }}>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.MarqueTitle}>Modele</Text>
                      </View>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.details}>{this.state.model}</Text>
                      </View>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.MarqueTitle}>Kilometrage</Text>
                      </View>
                      <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                        <Text style={styles.details}>{this.state.kilometrage}</Text>
                      </View>
                      
                    </View>

                  </View>
                
                   ):(
                     <View>
                       </View>
                   )}
                 
                  </View>
                  <View
                    style={[
                      styles.DividerHorizontal,
                      { marginTop: Metrics.HEIGHT * 0.02 }
                    ]}
                  />
                  <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                    <Text style={styles.FoodNameTextTitle}>Description</Text>

                    <ViewMoreText
                      numberOfLines={6}
                      renderViewMore={this.renderViewMore}
                      renderViewLess={this.renderViewLess}
                      textStyle={{
                        fontSize: Fonts.moderateScale(15),
                        marginLeft: Metrics.HEIGHT * 0.03,
                        lineHeight: 25,
                        marginTop: Metrics.HEIGHT * 0.01
                      }}
                    >
                      <Text style={styles.DescriptionText}>
                        
                        {this.state.description}

                      </Text>
                    </ViewMoreText>
                  </View>
                  <View style={styles.titleviewbg}>
                    <Text style={styles.descriptiontext}>Trouver un financement</Text>

                  </View>
                  <View style={styles.mainListRenderRow}>
                    <View style={styles.row} >
                      <TouchableOpacity
                        style={styles.rowBg}
                        onPress={() => this.navigateToUri()}

                      >
                        <Image
                          style={{  width: 98, height: 78 , resizeMode:"contain" }}
                          source={{ uri: this.state.imageEtablissement }} />

                      </TouchableOpacity>

                    </View>
                  </View>


                  <View
                    style={[
                      styles.DividerHorizontal,
                      { marginTop: Metrics.HEIGHT * 0.02 }
                    ]}
                  />
                  <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>


                    <MapView
                      style={styles.MapView}
                      provider={PROVIDER_GOOGLE}
                      region={{
                        latitude: parseFloat(this.state.lat),
                        longitude: parseFloat(this.state.long),
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                      }}
                      initialRegion={{
                        latitude: parseFloat(this.state.lat),
                        longitude: parseFloat(this.state.long),
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                      }}
                    >

                      <MapView.Marker
                        coordinate={{
                          latitude: parseFloat(this.state.lat),
                          longitude: parseFloat(this.state.long),
                          latitudeDelta: 0.015,
                          longitudeDelta: 0.0121
                        }}
                      >
                        <Entypo name="location-pin" size={35} color="#f05522" />
                      </MapView.Marker>


                    </MapView>

                    <View style={styles.mainListRenderRow}>
                      <View style={styles.row2} >
                        <TouchableOpacity
                          style={styles.rowBg2}
                          

                        >
                          <Image
                            style={{  width: 98, height: 78 }}
                            source={{ uri: this.state.avatar }} />

                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row',alignItems:"center", flex: 1 }}>
                          <View style={{ flexDirection: 'column', flex: 1 }}>
                            <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                              <Text style={styles.MarqueTitle}>N° SIREN</Text>
                              <Text style={styles.details}>{this.state.siren}</Text>
                            </View>
                          </View>
                          <View style={{ flexDirection: 'column', flex: 1, alignSelf: "flex-end" }}>
                            <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                              <Text style={styles.MarqueTitle}>N° SIRET</Text>
                            </View>
                            <View style={{ marginTop: Metrics.HEIGHT * 0.002 }}>
                              <Text style={styles.details}>{this.state.siret}</Text>
                            </View>
                          </View>
                        </View>


                      </View>
                    </View>

                    <View
                      style={[
                        styles.DividerHorizontal,
                        { marginTop: Metrics.HEIGHT * 0.02 }
                      ]}
                    />

                    {/*    <MapView
              style={styles.MapView}
              provider={PROVIDER_GOOGLE}
              region={{
                latitude: this.state.lat,
                longitude: this.state.long,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
              }}
              initialRegion={{
                latitude: this.state.lat,
                longitude: this.state.long,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
              }}
            >
              <MapView.Marker
                coordinate={{
                  latitude: this.state.lat,
                  longitude: this.state.long,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121
                }}
              >
                <Entypo name="location-pin" size={35} color="#f05522" />
              </MapView.Marker>
              <MapView.Marker
                coordinate={{
                  latitude: this.state.lat,
                  longitude: this.state.long,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121
                }}
              >
                <View
                  style={{
                    backgroundColor: "black",
                    height: Metrics.HEIGHT * 0.03,
                    width: Metrics.HEIGHT * 0.03,
                    borderRadius: Metrics.HEIGHT * 0.03,
                    alignSelf: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#fff",
                      height: Metrics.HEIGHT * 0.014,
                      width: Metrics.HEIGHT * 0.014,
                      borderRadius: Metrics.HEIGHT * 0.007,
                      alignSelf: "center",
                      justifyContent: "center"
                    }}
                  />
                </View>
              </MapView.Marker>

             

            </MapView>
           

*/}




                  </View>


















                  {this.state.resultrelative.length > 0 ? (
                    <View>
                      <View style={[styles.titleviewbg, { marginTop: 10 }]}>
                        <Text style={styles.descriptiontext}>PRODUITS SIMILAIRES</Text>

                      </View>
                      <View>
                        <ScrollView
                          style={styles.myrecipesbg}
                          horizontal={true}
                          showsHorizontalScrollIndicator={false}
                        >
                          {this.state.resultrelative.map((item, index) => {
                            return (
                              <TouchableOpacity
                                key={index}
                                style={styles.myrecipeslistbg}
                                onPress={() => this.getAnnonceDetail(item.id)

                                }
                              >
                                <Image
                                  source={{ uri: item.principalPhoto }}
                                  style={styles.myrecipeslistimagesbg}
                                />
                                <Text style={styles.descriptiontext}>
                                  {item.titre}
                                </Text>
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "center"
                                  }}
                                >
                                  
                                  
                                  <Text
                                    style={[
                                      
                                      {
                                        marginLeft: 15,
                                        marginTop: 10,
                                      
                                          color: "#ff0000",
                                          fontSize: Fonts.moderateScale(13),
                                          fontFamily: Fonts.type.sfuiDisplaySemibold,
                                          fontWeight:"bold",
                                      
                                         
                                      
                                      }
                                    ]}
                                  >
                                    {item.prixunqiue}€
                                  </Text>
                                </View>
                            
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <Entypo
                                    name="gauge"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.kilometrage}KM
                                  </Text>
                                </View>
                            
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  <Entypo
                                    name="calendar"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.prixunqiue}
                                  </Text>
                                </View>
                             {item.boite ?(
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <EvilIcons
                                    name="gear"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.boite}
                                  </Text>
                                </View>
                             ):(
                              <View
                              style={{
                                flexDirection: "row",
                                alignItems: "flex-start"
                              }}
                            >
                              
                              <EvilIcons
                                name="gear"
                                color="#263238"
                                size={18}
                                style={{
                                  marginLeft: 5,
                                  marginTop: 10,
                                  paddingHorizontal: 5
                                }}
                              />
                              <Text
                                style={[
                                  styles.details,
                                  {
                                    marginLeft: 5,
                                    marginTop: 10
                                  }
                                ]}
                              >
                                {item.cylendree}CC
                              </Text>
                            </View>
                       
                             )
                             }
                              </TouchableOpacity>
                            );
                          })}
                        </ScrollView>
                      </View>


                    </View>
                  ) : null}









                </View>


              </KeyboardAwareScrollView>
              <View style={styles.BookTableMainBg}>
                <TouchableOpacity

                  onPress={() => this.props.navigation.navigate("email",{
                    id:this.state.idEmail
                  })}
                >
                  <Fontisto
                    name="email"
                    size={40}
                    color="#ffffff"
                    style={{ marginLeft: Metrics.WIDTH * 0.2 }}
                  />

                </TouchableOpacity>
                <TouchableOpacity

                  onPress={() => this.makeCall(this.state.tel)}
                >
                  <AntDesign
                    name="phone"
                    size={40}
                    color="#ffffff"
                    style={{ marginRight: Metrics.WIDTH * 0.2 }}
                  />

                </TouchableOpacity>
              </View>
            </View>
          </Container>
        </View>
      );
                        };
  }
}
