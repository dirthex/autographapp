import { Platform, StyleSheet, Dimensions, I18nManager } from "react-native";
import { Images, Metrics, Fonts, Colors } from "../../../Themes/";


const styles = StyleSheet.create({
  mainView: {
    flex: 1
  },
  lireLasuite: {
    textAlign: 'center', 
    textDecorationLine: 'underline', 
    color: "#fff" ,
    backgroundColor:"#23292e",
    height:Metrics.HEIGHT*0.05,
    fontFamily: "SFUIDisplay-Semibold",
    fontSize: Fonts.moderateScale(15),
    lineHeight: 40,
    width:Metrics.WIDTH,
    marginTop:Metrics.HEIGHT*0.16,
    position:"absolute"

  },
  lireLasuite2: {
    textAlign: 'center', 
    textDecorationLine: 'underline', 
    color: "#fff" ,
    backgroundColor:"#23292e",
    height:Metrics.HEIGHT*0.05,
    fontFamily: "SFUIDisplay-Semibold",
    fontSize: Fonts.moderateScale(15),
    lineHeight: 40,
    width:Metrics.WIDTH,
    marginTop:Metrics.HEIGHT*0.01,


  },
  header: {
    // backgroundColor: "#0e1130",
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {
        height: 56
      },
      android: {
        height: 66,
        paddingTop: Metrics.HEIGHT * 0.02
      }
    }),
    elevation: 0,
    paddingLeft: Metrics.WIDTH * 0.05,
    paddingRight: Metrics.WIDTH * 0.05
  },
  MapView: {
    width: Metrics.WIDTH,
    ...Platform.select({
      ios: {
        height: Metrics.HEIGHT * 0.25
      },
      android: {
        height: Metrics.HEIGHT * 0.35
      }
    }),
    marginTop: Metrics.HEIGHT * 0.02
  },

  left: {
    flex: 1,
    marginLeft: 5
  },
  body: {
    flex: 1,
    alignItems: "center"
  },

  right: {
    flex: 0.5
  },
  imageOverlay: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: "center"
  },

  headerTitle: {
    color: "#fff",
    fontFamily: "SFUIDisplay-Semibold",
    fontSize: Fonts.moderateScale(18)
  },
  slidesec: {
    height: Metrics.HEIGHT * 0.3,
    width: "100%"
  },
  sliderImage: {
    resizeMode: "cover",
    height: Metrics.HEIGHT * 0.3,
    width: Metrics.WIDTH,


  },
  myrecipeslistimagesbg: {
    backgroundColor:"#dfdbdb38",
    height: Metrics.HEIGHT * 0.32,
    resizeMode: "contain",
    width: Metrics.WIDTH * 0.5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5
  },
  descriptiontext: {
    marginTop: 5,
    color: "#263238",
    marginLeft: 5,
    paddingHorizontal: 5,
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(15)
  },
  myrecipesbg: {
    height: Metrics.HEIGHT * 0.57,
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 5,
    marginTop: Metrics.HEIGHT * 0.02,
    flexDirection: "row"
  },
  row:{
    flexDirection: 'row',
  },
  row2:{
    flexDirection: 'row',
  },
  contentOne: {
		width: Metrics.WIDTH,
  },
  loginWithFacebookTxt: {
		color: Colors.snow,
		fontSize: Fonts.moderateScale(17),
		// fontFamily: Fonts.type.sfuiDisplayLight,
		marginLeft: Metrics.WIDTH * 0.02,
  },
  HeaderBg: {
		backgroundColor: "#23292e",
		borderBottomWidth: 1
	  },
	
  facebookBtnBg: {
		backgroundColor: '#dddbca',
		width: Metrics.WIDTH * 0.96,
		alignSelf: 'center',
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: Metrics.WIDTH * 0.025,
		paddingBottom: Metrics.WIDTH * 0.025,
		
	},
  bottomView: {
		width: Metrics.WIDTH,
		alignItems: 'center',
		justifyContent: 'center',
		height: Metrics.HEIGHT * 0.094,
	},
  container: {
		height: Metrics.HEIGHT,
		width: Metrics.WIDTH,
		backgroundColor: Colors.snow,
	},
  myrecipeslistbg: {
    marginHorizontal: 5,
   height: Metrics.HEIGHT * 0.55,
    backgroundColor: "#FFFFFF",
    width: Metrics.WIDTH * 0.5,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 5
  },
  formInput: {    
		marginLeft:20,marginRight:20,
		      
	  },
  mainListRenderRow: {
    alignSelf: "center",
    backgroundColor: "#fff",
    width: Metrics.WIDTH ,
    borderRadius: 2,
    marginTop: Metrics.HEIGHT * 0.01,
    shadowColor: "#f9f9f9",
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 2,
    elevation: 2,
    shadowOpacity: 0.2,
    marginBottom: Metrics.HEIGHT * 0.005,
    justifyContent:"center"
    //height:Metrics.HEIGHT*0.20
  },
  titletext: {
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(16),
    textAlign: "center",
    justifyContent: "center",
    color: "#616161",
    fontWeight: "100"
  },
  titleviewbg: {
    height:40,
    backgroundColor: "#dddbca",
    marginTop: 10,
    flexDirection: "row",
    justifyContent:"center"
  },
  productImage: {
    width: Metrics.WIDTH * 0.44,
    resizeMode:"contain"
  },
  rowBg: {
    width: Metrics.WIDTH ,
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: Metrics.WIDTH * 0.04,
    marginBottom: Metrics.WIDTH * 0.04,
    justifyContent:"center"
  },
  rowBg2: {
    flexDirection: 'row',
    alignItems:"flex-start",
    alignSelf: 'flex-start',
    resizeMode:"contain",
    justifyContent:"flex-start"
  },


  leftheader: {
    flex: 1
  },




  HederText: {
    color: "#ffffff",
    ...Platform.select({
      ios: {
        fontSize: Fonts.moderateScale(17)
      },
      android: {
        fontSize: Fonts.moderateScale(18)
      }
    }),

    fontFamily: Fonts.type.robotoRegular,
    textAlign: "left",
    alignSelf: "flex-start"
  },

  FoodBgImg: {
    width: Metrics.WIDTH,
    ...Platform.select({
      ios: {
        height: Metrics.HEIGHT * 0.35
      },
      android: {
        height: Metrics.HEIGHT * 0.4
      }
    })
  },

  FoodFavorite: {
    height: Metrics.HEIGHT * 0.05,
    width: Metrics.WIDTH * 0.05,
    resizeMode: "contain"
  },

  FoodName: {
    marginLeft: Metrics.HEIGHT * 0.03,
    marginTop: Metrics.HEIGHT * 0.02,
    color: "#212121",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(18),
    width: Metrics.WIDTH * 0.8,
    color: "#0e1130",


    fontWeight: 'bold',
  },

  FoodNameTextTitle: {
    marginLeft: Metrics.HEIGHT * 0.03,
    marginTop: Metrics.HEIGHT * 0.02,
    color: "#263238",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(16),
    width: Metrics.WIDTH * 0.8,
    color: "#0e1130",


    fontWeight: 'bold',
  },

  FoodNameTextTitle2: {
    marginTop:10,
    color: "#000",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(16),
    color: "#0e1130",
    fontWeight: 'bold',
  },
  MarqueTitle: {
    marginLeft: Metrics.HEIGHT * 0.03,
    marginTop: Metrics.HEIGHT * 0.02,
    color: "#263238",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(15),
    width: Metrics.WIDTH * 0.8,
    color: "#0e1130",


    fontWeight: 'bold',
  },


  BookTableMainBg: {
    width: Metrics.WIDTH,
    flexDirection: "row",
    justifyContent: 'space-between',

    backgroundColor: "#f05522",

    ...Platform.select({
      ios: {
        height: Metrics.HEIGHT * 0.08
      },
      android: {
        marginTop: Metrics.HEIGHT * 0.01,
        height: Metrics.HEIGHT * 0.05
      }
    })
  },

  BookTableText: {
    color: "#fff",
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    ...Platform.select({
      ios: {
        fontSize: Fonts.moderateScale(15)
      },
      android: {
        fontSize: Fonts.moderateScale(16)
      }
    }),
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center"
  },

  FoodName: {
    marginLeft: Metrics.HEIGHT * 0.03,
    marginTop: Metrics.HEIGHT * 0.02,
    color: "#263238",
    fontSize: Fonts.moderateScale(17),
    width: Metrics.WIDTH * 0.8,
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    fontSize: Fonts.moderateScale(16),
    width: Metrics.WIDTH * 0.8,
    color: "#0e1130",


    fontWeight: 'bold',
  },
  heartListIcon: {
    alignSelf: 'flex-start',

    marginLeft: 7,
    marginTop: Metrics.WIDTH * 0.10,

  },
  details: {
    marginLeft: Metrics.HEIGHT * 0.03,
    marginTop: Metrics.HEIGHT * 0.005,
    color: "#263238",
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    fontSize: Fonts.moderateScale(13),
    width: Metrics.WIDTH * 0.8
  },

  FoodNameText: {
    color: "red",
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    fontWeight:"bold",
    fontSize: Fonts.moderateScale(15)
  },

  DividerHorizontal: {
    width: Metrics.WIDTH,
    backgroundColor: "#BDBDBD",
    height: 1
  },

  CaloriesText: {
    color: "#757575",
    fontSize: 20,
    fontWeight: "bold",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(14),
    textAlign: "center",
    marginLeft: Metrics.HEIGHT * 0.01,
  },

  CaloriesTextNumber: {
    color: "#212121",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(18),
    textAlign: "center",
    textAlign: "center",
    marginTop: Metrics.HEIGHT * 0.01
  },

  IngredientsText: {
    marginTop: Metrics.HEIGHT * 0.02,
    color: "#263238",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(14),
    marginLeft: Metrics.HEIGHT * 0.03
  },

  descriptiontext: {
    marginTop: 5,
    marginLeft: 5,
    paddingHorizontal: 5,
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(14),
    color: "#000",
    fontWeight: 'bold',
  },

  userImg: {
    height: Metrics.HEIGHT * 0.08,
    width: Metrics.HEIGHT * 0.08,
    borderRadius: Metrics.HEIGHT * 0.04,
    alignSelf: "center"
  },

  nameText: {
    textAlign: "center",
    marginLeft: Metrics.HEIGHT * 0.01,
    color: "#616161",
    fontFamily: Fonts.type.robotoRegular,

    fontSize: Fonts.moderateScale(16),
    alignSelf: "center",
    marginTop: Metrics.HEIGHT * 0.01
  },

  timeDurationText: {
    marginLeft: Metrics.HEIGHT * 0.01,
    color: "#616161",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(12)
  },

  DescriptionTextComment: {
    marginLeft: Metrics.HEIGHT * 0.02,
    color: "#616161",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(14),
    width: Metrics.WIDTH * 0.9,
    marginTop: Metrics.HEIGHT * 0.01
  },

  TextInputMainBG: {
    backgroundColor: "#f1f5f7",
    ...Platform.select({
      ios: {
        height: Metrics.HEIGHT * 0.1
      },
      android: {
        height: Metrics.HEIGHT * 0.12
      }
    }),

    width: Metrics.WIDTH,
    flexDirection: "row",
    justifyContent: "space-between"
  },

  infoText: {
    color: "#bdbdbd",
    fontFamily: Fonts.type.robotoRegular,
    ...Platform.select({
      ios: {
        fontSize: Fonts.moderateScale(14)
      },
      android: {
        fontSize: Fonts.moderateScale(16)
      }
    }),
    width: Metrics.WIDTH * 0.8,
    marginLeft: Metrics.HEIGHT * 0.02
  },

  SendText: {
    color: "#ff3d00",
    fontFamily: Fonts.type.robotoRegular,
    fontSize: Fonts.moderateScale(16),
    fontWeight: "bold",
    justifyContent: "center",
    alignSelf: "center",
    textAlign: "center"
  },

  DividerHorizontalComment: {
    width: Metrics.WIDTH,
    backgroundColor: "#E0E0E0",
    height: 1
  }
});

export default styles;
