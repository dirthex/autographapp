import React, { Component } from 'react';
import { StyleSheet, Text, View ,  BackHandler, } from 'react-native';
import { WebView } from 'react-native-webview';
import { Container,   Spinner } from "native-base";
import styles from "./styles";
 
// ...
export default class MyWebComponent extends Component {
    constructor(props) {

        super(props);
        this.state = {
          isLoading:true,
          lien:"",
        };
        const { navigation } = this.props;
        this.props.navigation.addListener('willFocus', () => {
            this.setState({isLoading:true})
          this.state.lien = navigation.getParam('lien', null);
          this.setState({isLoading:false})
    
          //this.getblogs(id);
        })
        

    
      }
      componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress',()=>{   

          this.props.navigation.navigate("AnnonceDetails");
          return true;
         
        
      });
      }
  render() {
    
    console.log("this.State.lien"+ this.state.lien)
    if (this.state.isLoading == true) {

        return (
          <View style={styles.imageOverlay}>
                <Spinner color="black"  />
              </View>
        );
        }else{
    return (
      <WebView source={{ uri: this.state.lien }} />
    );
        }
  }
}