import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Text,
  Image,
  TouchableOpacity,
  BackHandler,
  ScrollView, Linking,

  ListView,
  ImageBackground,
  Container,
  AsyncStorage,
  FlatList,
  RefreshControl,
  ActivityIndicator,


} from "react-native";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import {

  Spinner
} from "native-base";
import Moment from 'moment';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Swiper from "react-native-swiper";
import { Header, Right, Left, Body, Content } from "native-base";
import { Metrics, Colors, Fonts, Images } from "../../../Themes";
import styles from "./styles";
import { CachedImage } from "react-native-cached-image";
import Annonces from "../Annonces/index";
import Blog from "../Blogs/index";
import Magazines from "../Magazines/index";
import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "../../../Components/react-native-scrollable-tab-view";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import Config from "../Config/config";
import axios from "axios";
import { throwStatement } from "@babel/types";

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/EvilIcons';
import { SocialIcon } from 'react-native-elements';
import SplashScreen from 'react-native-splash-screen'
export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.page = 1;
    this.state = {

      //dataSourceRecentUpdate: ds.cloneWithRows(RecentUpdate),
      data: '',
      blogs: [],
      slider: [],
      tempData: [],
      blog: [],
      dataSource: [],
      magazines: [],
      magazine: [],
      Magazines: [],
      Magazine: [],
      offres: [],
      offre: [],
      pageScroll: "",
      idPage: 0,
      isLoading: true,
      numPage: "",
    };
    this.props.navigation.addListener('willFocus', () => {

      const { navigation } = this.props;

      this.state.numPage = navigation.getParam('numero', '');
      console.log("nuuuum page main" + this.state.numPage);

      //this.setState( {pageScroll: navigation.getParam('page', null)})
      // console.log("this.state.pageSCROLL"+ this.state.pageScroll)
      // this.getMagazines();
      //  this.getBlog();
      // this.getMagazine();
      //this.getOffre();
      //this.getSlider();
      console.log("lister")
      //this.getSlider();

    });
    this.headerAnnonce();



  }

  Initialize() {

  }
  componentWillMount() {
    this.setState({ numPage: "" })
    AsyncStorage.getItem(
      "scroll",

    ).then(data => {
      // console.log("catt data[2][1]");


      this.setState({
        pageScroll: data,


      });

      console.log("this.state.pageScroll" + this.state.pageScroll)
    }
    )
    /* setTimeout(() => {
      try {
        this.getSlider();
      
      } catch (error) {
        this.setState({
          isLoading: false,
      
        });
      }
    }, 4000);*/
    AsyncStorage.removeItem("scroll");
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.removeItem("header");
    //this.setState({ pageScroll: "" })
  }

  backPressed = () => {
    this.props.navigation.navigate("NewsCategory");
    return true;
  };

  async componentDidMount() {
    console.log("componentDidMount")

    this.setState({ isLoading: true });
    await new Promise(resolve => { setTimeout(resolve, 1000); this.getSlider(); });
    this.setState({ isLoading: false });



    this.getMagazines(this.page);


    return Promise.resolve();
  }
  getSlider() {
    console.log("dkhaaaaaaaaaaaaaaaaaaal");
    this.setState({ isLoading: true });
    axios.get(Config.SERVER_URL + '/api/v1/home'
    ).then((res) => {
      this.setState({

        slider: res.data.sliders.related_pub,
        blogs: res.data.blogs,
        offres: res.data.annonces.fulfillmentValue,
        magazines: res.data.magazines,
        //isLoading:false


      });
      for (var i = 0; i < this.state.slider.length; i++) {

        this.state.tempData.push({
          id: this.state.slider[i].id,
          image: Config.SERVER_URL + this.state.slider[i].image,
        })


      }
      // console.log("tempData"+this.state.tempData)
      for (var i = 0; i < this.state.blogs.length; i++) {
        this.state.blog.push({
          id: this.state.blogs[i].id,
          TopStoriesTitle: this.state.blogs[i].titre,
          TopStoriesName: this.state.blogs[i].staff.nom,
          TopStoriesTime: this.state.blogs[i].createdAt,
          Desc: this.state.blogs[i].resume,
          Img: Config.SERVER_URL + this.state.blogs[i].cover,
        })
      }
      for (var i = 0; i < this.state.offres.length; i++) {
        this.state.offre.push({
          id: this.state.offres[i].id,
          FoodName: this.state.offres[i].titre,
          prixunqiue: this.state.offres[i].prixunqiue,
          recipetime: this.state.offres[i].createdAt,
          recipeimg: this.state.offres[i].principalPhoto,
          annee:this.state.offres[i].annee,
          boite:this.state.offres[i].boite,
          cylendree:this.state.offres[i].cylendree,
          kilometrage:this.state.offres[i].kilometrage

        })
      }
      for (var i = 0; i < this.state.magazines.length; i++) {
        this.state.magazine.push({
          id: this.state.magazines[i].id,
          FoodName: this.state.magazines[i].numero,

          recipetime: this.state.magazines[i].datepub,
          recipeimg: Config.SERVER_URL + this.state.magazines[i].cover,
          pdf: Config.SERVER_URL + this.state.magazines[i].pdf
        })
      }

      this.setState({ isLoading: false })
      SplashScreen.hide();
    }).catch((error) => {
      SplashScreen.hide();
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });

  }
  getMagazines(page) {
    //console.log("dkhel 622")

    this.setState({ loading: true });
    axios.get(Config.SERVER_URL + '/api/v1/magazines/' + this.page
    ).then((res) => {
      this.setState({

        Magazines: res.data,

        loading: false,

      });


      for (var i = 0; i < this.state.Magazines.length; i++) {
        this.state.Magazine.push({
          id: this.state.Magazines[i].id,
          FoodName: this.state.Magazines[i].numero,
          Foodimg: Config.SERVER_URL + this.state.Magazines[i].cover,
          datePub: this.state.Magazines[i].datepub,
          pdf: Config.SERVER_URL + this.state.Magazines[i].pdf,
        })
      }
      // console.log("blogsyyyy   ajajajaj" + JSON.stringify(this.state.Magazine))








    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: '100%',
          backgroundColor: '#CED0CE'
        }}
      />
    );
  };
  handleLoadMore = () => {
    if (!this.state.loading) {
      this.page = this.page + 1; // increase page by 1
      // method for API call 
      console.log("dkhel 622")

      this.getMagazines(this.page);


    }
  };
  onRefresh() {
    this.setState({ isRefreshing: true })
    axios.get(Config.SERVER_URL + '/api/v1/magazines/' + this.page
    ).then((res) => {
      this.setState({

        Magazines: res.data,
        isRefreshing: false,


      });


      for (var i = 0; i < this.state.Magazines.length; i++) {
        this.state.Magazine.push({
          id: this.state.Magazines[i].id,
          FoodName: this.state.Magazines[i].numero,
          Foodimg: Config.SERVER_URL + this.state.Magazines[i].cover,
          datePub: this.state.Magazines[i].datepub,
          pdf: Config.SERVER_URL + this.state.Magazines[i].pdf,
        })
      }
      //console.log("blogsyyyy   ajajajaj" + JSON.stringify(this.state.Magazine))








    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
  }
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };

  _renderRow(rowData) {
    console.log("render row data")
    return (
      <View>
        <TouchableOpacity

          onPress={() => this.NavigateToBlogDetails(rowData)}
        >
          <View style={{ flexDirection: "row", padding: Metrics.HEIGHT * 0.02 }}>
            <Image source={{ uri: rowData.Img }} style={styles.ImgBG} />
            <View>
              <Text style={styles.TopStoriesTitleText}>
                {rowData.TopStoriesTitle}
              </Text>
              <View
                style={{
                  flexDirection: "row"
                }}
              >
                <Text style={styles.TopStoriesNameText}>
                  {rowData.TopStoriesName}
                </Text>
                <MaterialIcons
                  name="access-time"
                  size={15}
                  color="#828282"
                  style={{ marginTop: Metrics.HEIGHT * 0.002 }}
                />
                <Text style={styles.TopStoriesTime}>
                  {Moment(rowData.TopStoriesTime).format('DD-MM-YYYY')}

                </Text>
              </View>
            </View>
          </View>

          <View style={styles.BorderHorizontal} />
        </TouchableOpacity>
      </View>
    );
  }


  renderItem = ({ item, index }) => {
    var that = this;

    return (

      <View style={styles.listMainviewBg}>
        <ScrollView>
          <View style={styles.FoodimgGrid}>
            <Image source={{ uri: item.Foodimg }} style={styles.FoodimgGrid} />

          </View>
          <TouchableOpacity
            onPress={() => this.NavigateToMagazine(item)}
          >
            <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
            <Text style={styles.FoodANameText}>{Moment(item.datePub).format('DD-MM-YYYY')}</Text>
            <View
              style={{
                flexDirection: "row",
                marginBottom: Metrics.HEIGHT * 0.01,
                marginLeft: Metrics.HEIGHT * 0.01
              }}
            >

              {/*<Text style={[styles.reviewText, { width: Metrics.HEIGHT * 0.09 }]}>
              238 reviews
            </Text>*/}
            </View>

          </TouchableOpacity>
        </ScrollView>
      </View>

    );
  };


  navigateToFiltre() {
    AsyncStorage.multiSet([["ArrivedFrom", "Annonce"]]);
    this.props.navigation.navigate(
      "Filter",
      {
        searchTxt: ""
      }
    );

  }
  headerAnnonce() {
    AsyncStorage.multiSet([["header", "Annonce"]]);
    AsyncStorage.multiSet([["ArrivedFrom", "home"]]);
  }
  NavigateToBlogDetails(rowData) {

    this.props.navigation.navigate("BlogDetails", {
      id: rowData.id,
      screen: "home"
    })
  }
  NavigateToMagazine(item) {
    AsyncStorage.multiSet([["ArrivedFrom", "home"]]);
    this.props.navigation.navigate("pdf", {
      pdf: item.pdf,
      titre: item.FoodName,
      screen: "home"
    }
    )
  }
  renderSwiper = ({ }) => {

  };

  render() {

    /*if(this.state.pageScroll == "pageBlog"){
      i=1;
    }
 if(this.state.pageScroll == "page"){
   i=2;
 }*/


    console.log("paage Scrollllll" + this.state.pageScroll)
    if (this.state.isLoading == true || this.state.slider == "" || this.state.magazine == "" || this.state.offre == ""
      || this.state.blog == "") {
      return (
        <View>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>



            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>



          <View style={styles.imageOverlay}>
            <Spinner color="black" />
          </View>

        </View>
      );
    }

    else if (this.state.pageScroll == "page") {
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>



            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={2}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Accueil">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.slidesec}>

                    <Swiper

                      showsButtons={false}
                      autoplay={true}
                      autoplayTimeout={2.5}
                      activeDot={<View style={styles.activeDot} />}
                      dot={<View style={styles.dot} />}
                    >

                      {this.state.tempData.map((item, index) => {

                        console.log("iteeeeeeeeeem" + item.image)

                        return (
                          <View style={styles.slide} key={index}>

                            <Image source={{ uri: item.image }} style={styles.sliderImage} />

                          </View>

                        );
                      })}
                    </Swiper>
                  </View>
                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#ed6663"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        // marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      DERNIERS ARTICLES
                </Text>
                  </View>

                  <View style={styles.ListContent}>



                    <ListView
                      dataSource={this.ds.cloneWithRows(this.state.blog)}
                      renderRow={this._renderRow.bind(this)}
                    />


                  </View>

                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#50b2fc"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        //marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      LES MEILLEURS OFFRES
                </Text>
                  </View>



                  <ScrollView
                    style={styles.myrecipesbg}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.offre.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() =>
                            this.props.navigation.navigate(
                              "AnnonceDetails", {
                              idAnnonce: item.id,
                              screenName: "home"
                            }
                            )
                          }
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.carsImg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "center"
                                  }}
                                >
                                  
                                  
                                  <Text
                                    style={[
                                      
                                      {
                                        marginLeft: 15,
                                        marginTop: 10,
                                      
                                          color: "#ff0000",
                                          fontSize: Fonts.moderateScale(13),
                                          fontFamily: Fonts.type.sfuiDisplaySemibold,
                                          fontWeight:"bold",
                                      
                                         
                                      
                                      }
                                    ]}
                                  >
                                    {item.prixunqiue}€
                                  </Text>
                                </View>
                            
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <Entypo
                                    name="gauge"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.kilometrage}KM
                                  </Text>
                                </View>
                            
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  <Entypo
                                    name="calendar"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.annee}
                                  </Text>
                                </View>
                             {item.boite ?(
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <EvilIcons
                                    name="gear"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.boite}
                                  </Text>
                                </View>
                             ):(
                              <View
                              style={{
                                flexDirection: "row",
                                alignItems: "flex-start"
                              }}
                            >
                              
                              <EvilIcons
                                name="gear"
                                color="#263238"
                                size={18}
                                style={{
                                  marginLeft: 5,
                                  marginTop: 10,
                                  paddingHorizontal: 5
                                }}
                              />
                              <Text
                                style={[
                                  styles.details,
                                  {
                                    marginLeft: 5,
                                    marginTop: 10
                                  }
                                ]}
                              >
                                {item.cylendree}CC
                              </Text>
                            </View>
                       
                             )
                             }
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>





                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#dddbca"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,

                        textAlign: "center"

                      }}
                    >
                      MAGAZINES
                </Text>
                  </View>




                  <ScrollView
                    style={styles.myrecipesbgmag}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.magazine.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() => this.NavigateToMagazine(item)}
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.myrecipeslistimagesbg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "flex-start"
                            }}
                          >
                            <MaterialCommunityIcons
                              name="clock-outline"
                              color="#263238"
                              size={18}
                              style={{ marginLeft: 5, marginTop: 10 }}
                            />
                            <Text
                              style={[
                                styles.datemag,
                                {
                                  marginLeft: 5,
                                  marginTop: 10
                                }
                              ]}
                            >
                              {Moment(item.recipetime).format('DD-MM-YYYY')}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>

                  <View style ={{height:"2%"}}>
                      </View>


                </ScrollView>
              </View>
            </View>
            <View tabLabel={'Articles'}>
              {this.headerAnnonce()}
              <Blog />
            </View>
            <View tabLabel={'Magazines'}>

              <View style={styles.mainview}>



                <View
                  style={{
                    backgroundColor: "#f5f5f5",
                    height: Metrics.HEIGHT * 0.9
                  }}
                >


                  <View>
                    <View style={styles.mainListContentGrid}>


                      <FlatList
                        contentContainerStyle={styles.listContentGrid}

                        data={this.state.Magazine}
                        renderItem={this.renderItem}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>
                              No Data Found.
                              </Text>
                          </View>
                        }

                      />
                    </View>
                  </View>

                </View>
              </View>


            </View>

          </ScrollableTabView>


        </View>
      );
    }
    else if (this.state.numPage == "page2") {
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>


            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={1}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Accueil">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.slidesec}>

                    <Swiper

                      showsButtons={false}
                      autoplay={true}
                      autoplayTimeout={2.5}
                      activeDot={<View style={styles.activeDot} />}
                      dot={<View style={styles.dot} />}
                    >

                      {this.state.tempData.map((item, index) => {

                        console.log("iteeeeeeeeeem" + item.image)

                        return (
                          <View style={styles.slide} key={index}>

                            <Image source={{ uri: item.image }} style={styles.sliderImage} />

                          </View>

                        );
                      })}
                    </Swiper>
                  </View>
                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#ed6663"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        // marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      DERNIERS ARTICLES
                                      </Text>
                  </View>

                  <View style={styles.ListContent}>



                    <ListView
                      dataSource={this.ds.cloneWithRows(this.state.blog)}
                      renderRow={this._renderRow.bind(this)}
                    />


                  </View>

                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#50b2fc"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        //marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      LES MEILLEURS OFFRES
                                      </Text>
                  </View>



                  <ScrollView
                    style={styles.myrecipesbg}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.offre.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() =>
                            this.props.navigation.navigate(
                              "AnnonceDetails"
                              , {
                                idAnnonce: item.id,
                                screenName: "home"
                              })
                          }
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.carsImg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "center"
                                  }}
                                >
                                  
                                  
                                  <Text
                                    style={[
                                      
                                      {
                                        marginLeft: 15,
                                        marginTop: 10,
                                      
                                          color: "#ff0000",
                                          fontSize: Fonts.moderateScale(13),
                                          fontFamily: Fonts.type.sfuiDisplaySemibold,
                                          fontWeight:"bold",
                                      
                                         
                                      
                                      }
                                    ]}
                                  >
                                    {item.prixunqiue}€
                                  </Text>
                                </View>
                            
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <Entypo
                                    name="gauge"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.kilometrage}KM
                                  </Text>
                                </View>
                            
                             
                          <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                            
                                  <Entypo
                                    name="calendar"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.annee}
                                  </Text>
                                </View>
                             {item.boite ?(
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <EvilIcons
                                    name="gear"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.boite}
                                  </Text>
                                </View>
                             ):(
                              <View
                              style={{
                                flexDirection: "row",
                                alignItems: "flex-start"
                              }}
                            >
                              
                              <EvilIcons
                                name="gear"
                                color="#263238"
                                size={18}
                                style={{
                                  marginLeft: 5,
                                  marginTop: 10,
                                  paddingHorizontal: 5
                                }}
                              />
                              <Text
                                style={[
                                  styles.details,
                                  {
                                    marginLeft: 5,
                                    marginTop: 10
                                  }
                                ]}
                              >
                                {item.cylendree}CC
                              </Text>
                            </View>
                       
                             )
                             }
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>





                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#dddbca"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,

                        textAlign: "center"

                      }}
                    >
                      MAGAZINES
                                      </Text>
                  </View>




                  <ScrollView
                    style={styles.myrecipesbgmag}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.magazine.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() => this.NavigateToMagazine(item)}
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.myrecipeslistimagesbg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "flex-start"
                            }}
                          >
                            <MaterialCommunityIcons
                              name="clock-outline"
                              color="#263238"
                              size={18}
                              style={{ marginLeft: 5, marginTop: 10 }}
                            />
                            <Text
                              style={[
                                styles.datemag,
                                {
                                  marginLeft: 5,
                                  marginTop: 10
                                }
                              ]}
                            >
                              {Moment(item.recipetime).format('DD-MM-YYYY')}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>

                  <View style ={{height:"10%"}}>
                      </View>


                </ScrollView>
              </View>
            </View>
            <View tabLabel={'Articles'}>
              {this.headerAnnonce()}
              <Blog />
            </View>
            <View tabLabel={'Magazines'}>

              <View style={styles.mainview}>



                <View
                  style={{
                    backgroundColor: "#f5f5f5",
                    height: Metrics.HEIGHT * 0.9
                  }}
                >


                  <View>
                    <View style={styles.mainListContentGrid}>


                      <FlatList
                        contentContainerStyle={styles.listContentGrid}

                        data={this.state.Magazine}
                        renderItem={this.renderItem}

                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>
                              No Data Found.
                                                    </Text>
                          </View>
                        }

                      />
                    </View>
                  </View>

                </View>
              </View>


            </View>

          </ScrollableTabView>


        </View>
      );
    }
    else if (this.state.numPage == "") {
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>



            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={0}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Accueil">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.slidesec}>

                    <Swiper

                      showsButtons={false}
                      autoplay={true}
                      autoplayTimeout={2.5}
                      activeDot={<View style={styles.activeDot} />}
                      dot={<View style={styles.dot} />}
                    >

                      {this.state.tempData.map((item, index) => {

                        console.log("iteeeeeeeeeem" + item.image)

                        return (
                          <View style={styles.slide} key={index}>

                            <Image source={{ uri: item.image }} style={styles.sliderImage} />

                          </View>

                        );
                      })}
                    </Swiper>
                  </View>
                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#ed6663"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        // marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      DERNIERS ARTICLES
                                                            </Text>
                  </View>

                  <View style={styles.ListContent}>



                    <ListView
                      dataSource={this.ds.cloneWithRows(this.state.blog)}
                      renderRow={this._renderRow.bind(this)}
                    />


                  </View>

                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#50b2fc"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        //marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      LES MEILLEURS OFFRES
                                                            </Text>
                  </View>



                  <ScrollView
                    style={styles.myrecipesbg}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.offre.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() =>
                            this.props.navigation.navigate(
                              "AnnonceDetails", {
                              idAnnonce: item.id,
                              screenName: 'home'
                            }
                            )
                          }
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.carsImg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "center"
                                  }}
                                >
                                  
                                  
                                  <Text
                                    style={[
                                      
                                      {
                                        marginLeft: 15,
                                        marginTop: 10,
                                      
                                          color: "#ff0000",
                                          fontSize: Fonts.moderateScale(13),
                                          fontFamily: Fonts.type.sfuiDisplaySemibold,
                                          fontWeight:"bold",
                                      
                                         
                                      
                                      }
                                    ]}
                                  >
                                    {item.prixunqiue}€
                                  </Text>
                                </View>
                            
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <Entypo
                                    name="gauge"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.kilometrage}KM
                                  </Text>
                                </View>
                            
                             
                          <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  <Entypo
                                    name="calendar"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.annee}
                                  </Text>
                                </View>
                             {item.boite ?(
                                <View
                                  style={{
                                    flexDirection: "row",
                                    alignItems: "flex-start"
                                  }}
                                >
                                  
                                  <EvilIcons
                                    name="gear"
                                    color="#263238"
                                    size={18}
                                    style={{
                                      marginLeft: 5,
                                      marginTop: 10,
                                      paddingHorizontal: 5
                                    }}
                                  />
                                  <Text
                                    style={[
                                      styles.details,
                                      {
                                        marginLeft: 5,
                                        marginTop: 10
                                      }
                                    ]}
                                  >
                                    {item.boite}
                                  </Text>
                                </View>
                             ):(
                              <View
                              style={{
                                flexDirection: "row",
                                alignItems: "flex-start"
                              }}
                            >
                              
                              <EvilIcons
                                name="gear"
                                color="#263238"
                                size={18}
                                style={{
                                  marginLeft: 5,
                                  marginTop: 10,
                                  paddingHorizontal: 5
                                }}
                              />
                              <Text
                                style={[
                                  styles.details,
                                  {
                                    marginLeft: 5,
                                    marginTop: 10
                                  }
                                ]}
                              >
                                {item.cylendree}CC
                              </Text>
                            </View>
                       
                             )
                             }
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>





                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#dddbca"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,

                        textAlign: "center"

                      }}
                    >
                      MAGAZINES
                                                            </Text>
                  </View>




                  <ScrollView
                    style={styles.myrecipesbgmag}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.magazine.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() => this.NavigateToMagazine(item)}
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.myrecipeslistimagesbg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "flex-start"
                            }}
                          >
                            <MaterialCommunityIcons
                              name="clock-outline"
                              color="#263238"
                              size={18}
                              style={{ marginLeft: 5, marginTop: 10 }}
                            />
                            <Text
                              style={[
                                styles.prix,
                                {
                                  marginLeft: 5,
                                  marginTop: 10
                                }
                              ]}
                            >
                              {Moment(item.recipetime).format('DD-MM-YYYY')}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>




                </ScrollView>
              </View>
            </View>
            <View tabLabel={'Articles'}>
              {this.headerAnnonce()}
              <Blog />
            </View>
            <View tabLabel={'Magazines'}>

              <View style={styles.mainview}>



                <View
                  style={{
                    backgroundColor: "#f5f5f5",
                    height: Metrics.HEIGHT * 0.9
                  }}
                >


                  <View>
                    <View style={styles.mainListContentGrid}>


                      <FlatList
                        contentContainerStyle={styles.listContentGrid}

                        data={this.state.Magazine}
                        renderItem={this.renderItem}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>
                              No Data Found.
                                                                          </Text>
                          </View>
                        }

                      />
                    </View>
                  </View>

                </View>
              </View>


            </View>

          </ScrollableTabView>


        </View>
      );
    }
    else {
      return (
        <View style={styles.mainView}>
          <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
            <Left style={styles.left}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Entypo name="menu" color="#FFFFFF" size={30} />
              </TouchableOpacity>
            </Left>


            <Right style={styles.right}>
              <SocialIcon
                type='facebook'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.facebook) }}
              />
              <SocialIcon
                type='instagram'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.instagram) }}
              />
              <SocialIcon
                type='youtube'
                iconSize={15}
                style={{ height: 30, width: 30 }}
                onPress={() => { Linking.openURL(Config.youtube) }}
              />
            </Right>
          </Header>

          <ScrollableTabView
            initialPage={0}
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={"transparent"}
            tabBarActiveTextColor={"#000000"}
            ref={(tabView) => { this.tabView = tabView; }}
            tabBarInactiveTextColor={"#000000"}
            tabBarTextStyle={styles.tabText}
            style={{
              backgroundColor: "#ffffff"
            }}
            renderTabBar={() => <ScrollableTabBar />}
          >
            <View tabLabel="Accueil">

              <View
                style={{
                  backgroundColor: "#fff"
                }}
              >
                <ScrollView>

                  <View style={styles.slidesec}>

                    <Swiper

                      showsButtons={false}
                      autoplay={true}
                      autoplayTimeout={2.5}
                      activeDot={<View style={styles.activeDot} />}
                      dot={<View style={styles.dot} />}
                    >

                      {this.state.tempData.map((item, index) => {

                        console.log("iteeeeeeeeeem" + item.image)

                        return (
                          <View style={styles.slide} key={index}>

                            <Image source={{ uri: item.image }} style={styles.sliderImage} />

                          </View>

                        );
                      })}
                    </Swiper>
                  </View>
                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#ed6663"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        // marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      DERNIERS ARTICLES
                                                            </Text>
                  </View>

                  <View style={styles.ListContent}>



                    <ListView
                      dataSource={this.ds.cloneWithRows(this.state.blog)}
                      renderRow={this._renderRow.bind(this)}
                    />


                  </View>

                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#50b2fc"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,
                        //marginLeft: Metrics.HEIGHT * 0.02,
                        textAlign: "center"
                      }}
                    >
                      LES MEILLEURS OFFRES
                                                            </Text>
                  </View>



                  <ScrollView
                    style={styles.myrecipesbg}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.offre.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() =>
                            this.props.navigation.navigate(
                              "AnnonceDetails", {
                              idAnnonce: item.id,
                              screenName: 'home'
                            }
                            )
                          }
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.carsImg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                            style={{

                              flexDirection: "row",
                              alignItems: "flex-start"
                            }}
                          >

                            <Text
                              style={[
                                styles.prix,
                                {

                                }
                              ]}
                            >
                              {item.prixunique}€
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>





                  <View style={{
                    marginLeft: Metrics.HEIGHT * 0.01,
                    marginRight: Metrics.HEIGHT * 0.01, heigh: "5%", backgroundColor: "#dddbca"
                  }}>
                    <Text
                      style={{
                        fontFamily: Fonts.type.robotoMedium,
                        fontSize: Fonts.moderateScale(14),
                        color: Colors.white,
                        padding: 10,

                        textAlign: "center"

                      }}
                    >
                      MAGAZINES
                                                            </Text>
                  </View>




                  <ScrollView
                    style={styles.myrecipesbgmag}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.magazine.map((item, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.myrecipeslistbg}
                          onPress={() => this.NavigateToMagazine(item)}
                        >
                          <Image
                            source={{ uri: item.recipeimg }}
                            style={styles.myrecipeslistimagesbg}
                          />
                          <Text style={styles.descriptiontext}>
                            {item.FoodName}
                          </Text>
                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "flex-start"
                            }}
                          >
                            <MaterialCommunityIcons
                              name="clock-outline"
                              color="#263238"
                              size={18}
                              style={{ marginLeft: 5, marginTop: 10 }}
                            />
                            <Text
                              style={[
                                styles.prix,
                                {
                                  marginLeft: 5,
                                  marginTop: 10
                                }
                              ]}
                            >
                              {Moment(item.recipetime).format('DD-MM-YYYY')}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>


                    <View style ={{height:"2%"}}>
                      </View>

                </ScrollView>
              </View>
            </View>
            <View tabLabel={'Articles'}>
              {this.headerAnnonce()}
              <Blog />
            </View>
            <View tabLabel={'Magazines'}>

              <View style={styles.mainview}>



                <View
                  style={{
                    backgroundColor: "#f5f5f5",
                    height: Metrics.HEIGHT * 0.9
                  }}
                >


                  <View>
                    <View style={styles.mainListContentGrid}>


                      <FlatList
                        contentContainerStyle={styles.listContentGrid}

                        data={this.state.Magazine}
                        renderItem={this.renderItem}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => item.id.toString()}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}

                        ListEmptyComponent={
                          <View style={{ justifyContent: "center" }}>
                            <Text style={{ alignSelf: "center" }}>
                              No Data Found.
                                                                          </Text>
                          </View>
                        }

                      />
                    </View>
                  </View>

                </View>
              </View>


            </View>

          </ScrollableTabView>


        </View>
      );
    }

  }
}
