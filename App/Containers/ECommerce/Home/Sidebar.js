import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Text,
  Image,
  TouchableOpacity,
  ListView,
  AsyncStorage
} from "react-native";

import { Content } from "native-base";
import styles from "./styles";
import { Metrics, Fonts, Images, Colors } from "../../../Themes";

import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import { SocialIcon } from 'react-native-elements';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/EvilIcons';
const rowHasChanged = (r1, r2) => r1 !== r2;
const ds = new ListView.DataSource({ rowHasChanged });

const MenuItems = [
  {
    id: 1,
    name: "Home"
  },
  {
    id: 2,
    name: "Annonces"
  },


  {
    id: 5,
    name: "Mon compte"
  },
  {
    id: 6,
    name: "Articles"
  },
  {
    id: 7,
    name: "Magazines"
  },
  {
    id:8,
    name:"Favoris"
  }


];


export default class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      dataSource: ds.cloneWithRows(MenuItems),
      token: "",
      isLoading: true
    };

    this.props.navigation.addListener('willFocus', () => {
      this.loadJWT();

    });

  }
  componentDidMount() {

    this.setState({ isLoading: true });
    this.loadJWT();
    this.setState({ isLoading: false });
  }
  async loadJWT() {
    try {
      const value = await AsyncStorage.getItem('token');

      if (value !== null) {
        this.setState({
          token: value,
          loading: false
        });
        return Promise.resolve(value)
      } else {
        this.setState({
          loading: false
        });
        return Promise.resolve("error");
      }
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message);
      return Promise.resolve("error")
    }
  }
  async deleteJWT() {
    try {
      await AsyncStorage.removeItem('token')
        .then(
          () => {
            this.setState({
              token: ''
            })
          }
        );
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  }
  getToken = async () => {
    const userToken = await AsyncStorage.getItem('token');
    const token = userToken;
    this.state.token = userToken;
    return Promise.resolve(token);
  }
  navigateMoncompte(){
    this.getToken().then(token =>{
      if(token){
        this.props.navigation.navigate("MonCompte");
        }
        else {
          this.props.navigation.navigate("ECommerceLogin");
        }
    })
    
  }
  menuItemClickHandle(id) {
    this.loadJWT();
    this.setState({
      id: id,
      dataSource: ds.cloneWithRows(MenuItems)
    });

    if (id == 1) {
      this.props.navigation.navigate("ECommerceMenu", {
        numero: ''
      });
    } else if (id == 2) {
      this.props.navigation.navigate("Annonces", {
        pos: null,
        cat: null,
        titre: null,
        marque: null,
        model: null,
        prixMin: null,
        prixMax: null,
        anneeMin: null,
        anneeMax: null,
        kmmin: null,
        kmmax: null,
        cylindremin: null,
        clylindremax: null,
        carbu: null,
        vitesse: null,


      })
    } else if (id == 3) {
      this.props.navigation.navigate("AddAnnonce");
    } else if (id == 4) {
      this.props.navigation.navigate("ECommerceWishList");
    } else if (id == 5) {
      
     this.navigateMoncompte();
    } else if (id == 6) {
      this.props.navigation.navigate("BlogMenu");
    } else if (id == 7) {
      this.props.navigation.navigate("magazine");
    } else if (id == 8) {
      this.props.navigation.navigate("favoris");
    }
    else if (id == 9) {
      this.setState({ token: '' })
      this.deleteJWT();
    }
    this.props.navigation.closeDrawer();
  }

  renderIcon = index => {
    if (index == 0) {
      return (
        <FontAwesome5
          name="wpexplorer"
          size={25}
          color="#909090"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    } else if (index == 1) {
      return (
        <View>
          <View style={styles.BGRoadTrip}>
            <FontAwesome5
              name="car"
              size={25}
              color={"#909090"}
              style={{
                alignSelf: "center",
                width: Metrics.WIDTH * 0.09,
                marginLeft: Metrics.HEIGHT * 0.04,
                marginTop: Metrics.HEIGHT * 0.02
              }}
            />
          </View>
        </View>
      );
    } else if (index == 2) {
      return (
        <FontAwesome5
          name="user"
          size={25}
          color="#909090"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    } else if (index == 3) {
      return (
        <Entypo
          name="open-book"
          size={25}
          color="#909090"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    } else if (index == 4) {
      return (
        <FontAwesome5
          name="newspaper"
          size={25}
          color="#6f6f6f"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    } else if (index == 5) {
      return (
        <Ionicons
          name="md-image"
          size={25}
          color="#6f6f6f"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    } else if (index == 6) {
      return (
        <FontAwesome5
          name="heart"
          size={25}
          color="#6f6f6f"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    }
    else if (index == 8) {
      return (
        <FontAwesome5
          name="heart"
          size={25}
          color="#6f6f6f"
          style={{
            alignSelf: "center",
            width: Metrics.WIDTH * 0.09,
            marginLeft: Metrics.HEIGHT * 0.04
          }}
        />
      );
    }

  };

  navigateDep(){
    this.getToken().then(token =>{
      if(token){
        this.props.navigation.navigate("AddAnnonce");
        }
        else {
          this.props.navigation.navigate("ECommerceLogin");
        }
    })
  }
  render() {


    //this.loadJWT()
    //this.state.token =  AsyncStorage.getItem('token');
    //console.log("vaaaaaaaaalue " + JSON.stringify(this.state.token))
    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#dd2c00", true);
      StatusBar.setTranslucent(true);
    }
    

    return (

      <View style={styles.mainView}>

        <View style={styles.headerView}>
          <TouchableOpacity>
            <Image
              source={{uri:"https://www.codemediastudioad.com/images/logo.png"}}
              style={styles.imageProfile}
            />
          </TouchableOpacity>


        </View>

        <View style={styles.menuView}>
          <View style={styles.menuViewItem}>
            
                <Content>

                  {MenuItems.map((item, index) => {
                    return (
                      <View>
                        {this.state.id == item.id ? (
                          <TouchableOpacity
                            key={index}
                            onPress={() => this.menuItemClickHandle(item.id)}
                            style={{
                              marginBottom: Metrics.HEIGHT * 0.002,
                              flexDirection: "row",
                              backgroundColor: "#f1f5f7"
                            }}
                          >
                            {this.renderIcon(index)}
                            <Text style={[styles.menuName, { fontWeight: "bold" }]}>
                              {item.name}
                            </Text>
                          </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                              key={index}
                              onPress={() => this.menuItemClickHandle(item.id)}
                              style={{
                                marginBottom: Metrics.HEIGHT * 0.002,
                                flexDirection: "row"
                              }}
                            >
                              {this.renderIcon(index)}
                              <Text style={styles.menuName}>{item.name}</Text>
                            </TouchableOpacity>
                          )}
                      </View>
                    );
                  })}
                </Content>

            
            <View style={styles.bottomBar}>
           
            <Button
             
             icon={
               <Icon
                 name="plus"
                 size={30}
                 color="white"
               />
             }
             iconLeft
             title="Déposer une annonce"
             buttonStyle={styles.buttonDeposerannone}

             onPress={() =>this.navigateDep()}
           /> 
            </View>
          </View>

        </View>
      </View>
    );
  }
}
