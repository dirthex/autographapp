import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  TextInput,
  ListView,
  Linking,
  Easing,
  Platform,
  FlatList,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import axios from "axios";
import styles from "./styles";
import Config from "../Config/config";
import { withNavigation } from 'react-navigation';

import { Images, Metrics, Fonts, Colors } from "../../../Themes/";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import ScrollableTabView, {
  ScrollableTabBar,
  DefaultTabBar
} from "../../../Components/react-native-scrollable-tab-view";
import {
  Content,
  Container,
  Button,
  Icon,
  Right,
  Item,
  Input,
  Header,
  Left,
  Body,
  Title,
  Segment,
  Label,
} from 'native-base';
import Moment from 'moment';
import { SocialIcon } from 'react-native-elements'






const rowHasChanged = (r1, r2) => r1 !== r2;
const ds = new ListView.DataSource({ rowHasChanged });

class Magazines extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = {
      id: "",

      loading: false, // user list loading
      isRefreshing: false,

      review: "",
      MenuList: false,
      GridList: true,
      Magazines: [],
      Magazine: []
    };
    this.props.navigation.addListener('willFocus', () => {

      //this.getMagazines();

      //  this.getBlog();
      // this.getMagazine();
      //this.getOffre();

    });
  }
  NavigateToMagazine(item) {
    AsyncStorage.multiSet([["ArrivedFrom", "Magazine"]]);
    this.props.navigation.navigate("pdf", {
      pdf: item.pdf,
      titre: item.FoodName
    }

    )
    console.log("tiiiite" + item.FoodName)
  }
  getMagazines(page) {
    console.log("dkhel 622")

    this.setState({ loading: true });
    axios.get(Config.SERVER_URL + '/api/v1/magazines/' + this.page
    ).then((res) => {
      this.setState({

        Magazines: res.data,

        loading: false,

      });


      for (var i = 0; i < this.state.Magazines.length; i++) {
        this.state.Magazine.push({
          id: this.state.Magazines[i].id,
          FoodName: this.state.Magazines[i].numero,
          Foodimg: Config.SERVER_URL + this.state.Magazines[i].cover,
          datePub: this.state.Magazines[i].datepub,
          pdf: Config.SERVER_URL + this.state.Magazines[i].pdf,
        })
      }
      console.log("blogsyyyy   ajajajaj" + JSON.stringify(this.state.Magazine))








    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  componentDidMount() {

    this.getMagazines(this.page);
    BackHandler.addEventListener('hardwareBackPress', () => {

      this.props.navigation.navigate("ECommerceMenu");
      return true;


    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);

  }
  componentWillMount() {

  }

  handleBackPress = () => {
    this.props.navigation.navigate("TabView");
    return true;
  };

  /* Grid FavoritPost START */

  /* Grid FavoritPost END */

  /* List FavoritPost START */

  /* List FavoritPost END */





  /* List renderRow START */
  /* renderItem = ({ item, index }) => {
     var that = this;
 
     return (
       <View style={styles.mainListRenderRow}>
         <View style={styles.Foodimg}>
           <Image source={item.Foodimg} style={styles.Foodimg,{resizeMode:"cover"}} />
          
         </View>
         <TouchableOpacity
           onPress={() => this.props.navigation.navigate("ProductDetails")}
         >
           <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
           <Text style={styles.FoodANameText}>60 Kub Pines Apt.797</Text>
           <View
             style={{
               flexDirection: "row",
               marginLeft: Metrics.HEIGHT * 0.01,
               marginBottom: Metrics.HEIGHT * 0.01
             }}
           >
              <Text style={styles.reviewText}>238 reviews</Text>
           </View>
         </TouchableOpacity>
       </View>
     );
   };*/
  /* List renderRow END */

  /* Grid renderRow START */

  renderItem = ({ item, index }) => {
    var that = this;

    return (

      <View style={styles.listMainviewBg}>
        <ScrollView>
          <TouchableOpacity
            onPress={() => this.NavigateToMagazine(item)}
          >
            <View style={styles.FoodimgGrid}>
              <Image source={{ uri: item.Foodimg }} style={styles.FoodimgGrid} />

            </View>
            <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start"
              }}
            >
              <MaterialCommunityIcons
                name="clock-outline"
                color="#263238"
                size={18}
                style={{ marginLeft: 14, marginTop: 10 }}
              />
              <Text
                style={[
                  styles.datemag,
                  {
                    marginLeft: 5,
                    marginTop: 10
                  }
                ]}
              >
                {Moment(item.datePub).format('DD-MM-YYYY')}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginBottom: Metrics.HEIGHT * 0.01,
                marginLeft: Metrics.HEIGHT * 0.01
              }}
            >

              {/*<Text style={[styles.reviewText, { width: Metrics.HEIGHT * 0.09 }]}>
              238 reviews
            </Text>*/}
            </View>

          </TouchableOpacity>
        </ScrollView>
      </View>

    );
  };


  /* Grid renderRow END */

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: '100%',
          backgroundColor: '#CED0CE'
        }}
      />
    );
  };
  handleLoadMore = () => {
    if (!this.state.loading) {
      this.page = this.page + 1; // increase page by 1
      // method for API call 
      console.log("dkhel 622")

      this.getMagazines(this.page);


    }
  };
  onRefresh() {
    this.setState({ isRefreshing: true })
    axios.get(Config.SERVER_URL + '/api/v1/magazines/' + this.page
    ).then((res) => {
      this.setState({

        Magazines: res.data,
        isRefreshing: false,


      });


      for (var i = 0; i < this.state.Magazines.length; i++) {
        this.state.Magazine.push({
          id: this.state.Magazines[i].id,
          FoodName: this.state.Magazines[i].numero,
          Foodimg: Config.SERVER_URL + this.state.Magazines[i].cover,
          datePub: this.state.Magazines[i].datepub,
          pdf: Config.SERVER_URL + this.state.Magazines[i].pdf,
        })
      }
      console.log("blogsyyyy   ajajajaj" + JSON.stringify(this.state.Magazine))








    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });
  }
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };

  render() {
    if (this.state.loading && this.page === 1) {
      return <View style={{
        width: '100%',
        height: '100%'
      }}><ActivityIndicator style={{ color: '#000' }} /></View>;
    }
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("black", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <View style={styles.mainview}>

        <Header style={styles.HeaderBg} androidStatusBarColor={"#000000"} transparent>
          <Left style={styles.left}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Entypo name="menu" color="#FFFFFF" size={30} />
            </TouchableOpacity>
          </Left>

          <Body style={styles.body}>
            <Text style={styles.headerTitle}></Text>
          </Body>

          <Right style={styles.right}>
            <SocialIcon
              type='facebook'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.facebook) }}
            />
            <SocialIcon
              type='instagram'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.instagram) }}
            />
            <SocialIcon
              type='youtube'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.youtube) }}
            />
          </Right>
        </Header>



        <View
          style={{
            backgroundColor: "#f5f5f5",
            height: Metrics.HEIGHT * 0.9
          }}
        >


          <View>
            <View style={styles.mainListContentGrid}>

              <FlatList
                contentContainerStyle={styles.listContentGrid}

                data={this.state.Magazine}
                renderItem={this.renderItem}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.isRefreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                keyExtractor={(item, index) => item.id.toString()}
                ItemSeparatorComponent={this.renderSeparator}
                ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                onEndReached={this.handleLoadMore.bind(this)}

                ListEmptyComponent={
                  <View style={{ justifyContent: "center" }}>
                    <Text style={{ alignSelf: "center" }}>

                    </Text>
                  </View>
                }

              />
            </View>
          </View>

        </View>
      </View>



    );
  }
}
export default withNavigation(Magazines);