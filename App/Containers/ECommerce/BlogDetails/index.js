import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  Platform,
  BackHandler,
  I18nManager,
  Modal,
  TextInput,
  Button,
  ImageBackground,
  ListView,
  AsyncStorage,
  Dimensions,
  Linking,
  WebView,
  WebBrowser,
  NetInfo,
  KeyboardAvoidingView,

} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { } from 'react-native-render-html/src/HTMLUtils';

import {
  Content,
  Container,
  Right,
  Header,
  Left,
  Body,
  Title,
  Spinner
} from "native-base";
import axios from "axios";
import Config from "../Config/config";
import PropTypes from "prop-types";

import HTML from "react-native-render-html";

import styles from "./styles";
import { Images, Fonts, Metrics, Colors } from "../../../Themes/";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Globals from "../Globals";
import Moment from 'moment';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
// const rowHasChanged = (r1, r2) => r1 !== r2;
// const ds1 = new ListView.DataSource({ rowHasChanged });

var content = "";

const tagsStyles = {
  p: {
    lineHeight: Metrics.HEIGHT * 0.03,
    fontFamily: Fonts.helveticaRegular,
    textAlign: "left",
    color: "#111111",
    fontSize: Fonts.moderateScale(14)
  },
  h2: {
    color: "#0e1130",
    fontFamily: Fonts.helveticaRegular,
    fontSize: Fonts.moderateScale(16),
    textAlign: "left",
    paddingTop: Metrics.HEIGHT * 0.02,
    paddingBottom: Metrics.HEIGHT * 0.02,
    lineHeight: Metrics.HEIGHT * 0.04
  }
};

function handleFirstConnectivityChange(isConnected) {
  NetInfo.isConnected.removeEventListener(
    "connectionChange",
    handleFirstConnectivityChange
  );
}

const rowHasChanged = (r1, r2) => r1 !== r2;
const ds = new ListView.DataSource({ rowHasChanged });
var dataSourceObjects = [];

const DEFAULT_PROPS = {
  tagsStyles: { p: { margin: 0, marginLeft: 10, marginRight: 10,justifyContent: "center" }, img: { width: 5 } , table: { marginLeft: 10 ,marginRight:10,textAlign:"center"}, td: {textAlign:"left" ,justifyContent: "center"}, div: {textAlign:"center"}},
  onLinkPress: (evt, href) => { Linking.openURL(href) },
};

export default class CategoryDetail extends React.Component {
  constructor(props) {

    super(props);

    this.state = {
      ecran: '',
      titleTxt: "",
      postID: "",
      userName: "",
      userEmail: "",
      userComment: "",
      id: "",
      content: "",
      title: "",
      commentCount: "",
      arriveFrom: "",
      commentData: [],
      url: "",
      authorName: "",
      isImage: false,
      isLoading: true,
      doneApiCall: false,
      internetConnection: false,
      date: "",
      idBlog: "",
      screenName: "",
      screenBlogMenu: "",
      // dataSource1: ds1.cloneWithRows(dataSourceObjects),
      dataSource: ds.cloneWithRows(dataSourceObjects)
    };

    this.props.navigation.addListener('willFocus', () => {
      const { navigation } = this.props;
      this.state.idBlog = navigation.getParam('id', '136');
      
      this.state.screenName = navigation.getParam('screen', '');
      //this.state.screenBlogMenu = navigation.getParam('screen','');

      console.log("screen name " + this.state.screenName)

      //this.getblogs(id);
    })
  }

  componentWillMount() {
    /*AsyncStorage.setItem("scroll", "pageBlog");
    AsyncStorage.multiGet([
      "ArrivedFrom",

    ]).then(data => {
      // console.log("catt data[2][1]");
      console.log("dta" + data[0][1]);

      this.setState({
        ecran: data[0][1],

      });
    })*/
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.state.screenName == "home") {
        //this.setState({screenName:""})
        //AsyncStorage.removeItem('scroll')
        this.props.navigation.navigate("ECommerceMenu",
        {
          numero:""
        });

      }
      if (this.state.screenName == "voiture") {
        this.props.navigation.navigate("BlogMenu", {
          numero: "0",
        });
      }
      if (this.state.screenName == "moto") {
        this.props.navigation.navigate("BlogMenu", {
          numero: "1",
        });
      }
      if (this.state.screenName == "bateau") {
        this.props.navigation.navigate("BlogMenu", {
          numero: "2",
        });
      }
      if (this.state.screenName == "page2") {
        this.props.navigation.navigate("ECommerceMenu", {
          numero: "page2",
        });
      }

      return true;

    });
    var that = this;


    NetInfo.isConnected.addEventListener(
      "connectionChange",
      handleFirstConnectivityChange
    );




    setTimeout(() => {
      this.getBlogDetails();
    }, 2000);
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      handleFirstConnectivityChange
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.removeItem("ArrivedFrom")

  }


  getBlogDetails() {

    axios.get(Config.SERVER_URL + '/api/v1/blog/' + this.state.idBlog
    ).then((res) => {
      this.setState({

        id: res.data.blog.id,
        name: res.data.blog.staffId,
        content: res.data.blog.html,
        titleTxt: res.data.blog.titre,
        title: res.data.blog.prixguyane,
        authorName: res.data.blog.staffId,
        isLoading: false,
        internetConnection: true,
        doneApiCall: true,
        image: Config.SERVER_URL + res.data.blog.cover,
        isImage: true,
        date: res.data.createdAt


      });



      console.log("blogsyyyy   ajajajaj" + this.state.id + this.state.titleTxt + this.state.title)

      console.log("blogsyyyy   ajajajaj" + this.state.name)

      console.log("blogsyyyy   ajajajaj" + this.state.content)






    }).catch((error) => {
      this.setState({
        error: 'Error retrieving data',
        loading: false
      });
    });


  }
  /*_postComment() {
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      handleFirstConnectivityChange
    );

    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        const config = {
          method: "GET"
        };
        fetch(
          "https://www.codemediastudioad.com/api/v1/blog/36" ,
            
          config
        )
          .then(response => response.json())
          .then(responseJson => {
            console.log(responseJson);

            try {
              if (responseJson.status == "ok") {
                this.setState({
                  userName: "",
                  userEmail: "",
                  userComment: ""
                });

                this._getPostDetails();
              }
            } catch (error) {
              alert("Something went wrong");
            }
          })
          .catch(error => {
            console.error(error);
            this.setState({
              internetConnection: false,
              isLoading: false
            });
          });
      } else {
        //alert("please check your Internet");
        this.setState({
          internetConnection: false,
          isLoading: false
        });
      }
    });
  }*/
  backNav() {


    if (this.state.screenName == "home") {
      //this.setState({screenName:""})
      //AsyncStorage.removeItem('scroll')
      this.props.navigation.navigate("ECommerceMenu",
      {
        numero:""
      });

    }
    if (this.state.screenName == "voiture") {
      this.props.navigation.navigate("BlogMenu", {
        numero: "0",
      });
    }
    if (this.state.screenName == "moto") {
      this.props.navigation.navigate("BlogMenu", {
        numero: "1",
      });
    }
    if (this.state.screenName == "bateau") {
      this.props.navigation.navigate("BlogMenu", {
        numero: "2",
      });
    }
    if (this.state.screenName == "page2") {
      this.props.navigation.navigate("ECommerceMenu", {
        numero: "page2",
      });
    }

    return true;



  }

  _backNavigation() {
    if (this.state.screenName == "home") {
      //this.setState({screenName:""})
      //AsyncStorage.removeItem('scroll')
      this.props.navigation.navigate("ECommerceMenu",
      {
        numero:""
      });

    }
    if (this.state.screenName == "voiture") {
      this.props.navigation.navigate("BlogMenu", {
        numero: "0",
      });
    }
    if (this.state.screenName == "moto") {
      this.props.navigation.navigate("BlogMenu", {
        numero: "1",
      });
    }
    if (this.state.screenName == "bateau") {
      this.props.navigation.navigate("BlogMenu", {
        numero: "2",
      });
    }
    if (this.state.screenName == "page2") {
      this.props.navigation.navigate("ECommerceMenu", {
        numero: "page2",
      });
    }

    return true;

  }




  render() {
    var that = this;
    var prix = this.state.title.toLocaleString();
    var result = prix.replace(/(\d)(?=(\d{3})+$)/g, '$1 ');


    return (
      <Content>
        <View style={styles.mainview}>
          <StatusBar hidden={true} />

          {this.state.isLoading ? (
            <View style={styles.imageOverlay}>
              <Spinner color="black" />
            </View>
          ) : (
              <View>
                {this.state.doneApiCall == true ? (
                  <View styles={{ height: Metrics.HEIGHT, width: Metrics.WIDTH }}>
                    {this.state.isImage ? (
                      <ImageBackground
                        source={{ uri: this.state.image }}
                        imageStyle={{ resizeMode: "cover" }}
                        style={styles.backk}
                      >
                        <Header
                          androidStatusBarColor={"transparent"}
                          style={styles.header}
                        >
                          <Left style={styles.left}>
                            <TouchableOpacity
                              style={styles.backArrow}
                              onPress={() => this.backNav()}
                            >
                              {/*
                               <Ionicons
                                 name={
                                   I18nManager.isRTL
                                     ? "ios-arrow-forward"
                                     : "ios-arrow-back"
                                 }
                                 size={35}
                                 color="white"
                                 style={{ paddingRight: 10 }}
                               />*/}
                              <Image
                                style={styles.backBtn}
                                source={Images.back}
                              />
                            </TouchableOpacity>
                          </Left>

                          <Body style={styles.body} />


                        </Header>
                      </ImageBackground>
                    ) : (
                        <ImageBackground
                          source={Images.No_image_found}
                          imageStyle={{ resizeMode: "cover" }}
                          style={styles.backk}
                        >
                          <Header
                            androidStatusBarColor={"#0e1130"}
                            style={styles.header}
                          >
                            <Left style={styles.left}>
                              <TouchableOpacity
                                style={styles.backArrow}
                                onPress={() => this._backNavigation()}
                              >
                                {/*
                             <Ionicons
                               name={
                                 I18nManager.isRTL
                                   ? "ios-arrow-forward"
                                   : "ios-arrow-back"
                               }
                               size={35}
                               color="white"
                               style={{ paddingRight: 10 }}
                             />*/}
                                <Image
                                  style={styles.backBtn}
                                  source={Images.back_blogdetail}
                                />
                              </TouchableOpacity>
                            </Left>
                            <Body style={styles.body} />

                          </Header>
                        </ImageBackground>
                      )}

                    <View style={styles.maincontent}>
                      <Text style={styles.desc}>{this.state.titleTxt}</Text>

                      {this.state.title == 0 ? (
                        null
                      ) : (
                          <Text style={styles.fashion}>{result}€</Text>
                        )
                      }
                      <View style={styles.likeview}>
                        <View style={{ flexDirection: 'row' }}>
                          <MaterialCommunityIcons
                            name="clock-outline"
                            color="#263238"
                            size={19}
                            style={{ marginLeft: 5 }}
                          />
                          <Text style={{
                            textAlign: "left",
                            fontFamily: Fonts.type.sfuiDisplaySemibold,
                            color: Colors.black,
                            fontSize: Fonts.moderateScale(14),
                            marginRight: "2%"
                          }}>
                            {Moment(this.state.date).format('DD-MM-YYYY')}
                          </Text>
                        </View>
                        <Text style={{
                          textAlign: "left",
                          fontFamily: Fonts.type.sfuiDisplaySemibold,
                          color: Colors.black,
                          fontSize: Fonts.moderateScale(14),
                          marginRight: "2%"
                        }}>
                          Eric Gruszka
                        </Text>


                      </View>
                      <View style={styles.divider} />
                      <View style={styles.listview}>
                        <HTML
                          html={this.state.content}
                          tagsStyles={tagsStyles}
                          
                          imagesMaxWidth={Dimensions.get('window').width*0.96}
                          ignoredStyles={['line-height', 'width']}
                          {...DEFAULT_PROPS}

                        />
                      </View>
                    </View>



                  </View>
                ) : null}
              </View>
            )}
        </View>
      </Content>
    );
  }
}
