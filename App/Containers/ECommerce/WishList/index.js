import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StatusBar,
  Platform,
  TouchableOpacity,
  BackHandler,
  Linking,
  ListView,
  ImageBackground,
  ScrollView,
  Picker,
  I18nManager,
  AsyncStorage
} from "react-native";
import {
  Container,
  Button,
  Right,
  Left,
  ListItem,
  Content,
  Body,
  Header
} from "native-base";
// Screen Styles
import styles from "./styles";
import Entypo from "react-native-vector-icons/Entypo";

import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
// import { Dropdown } from 'react-native-material-dropdown';
import Dropdown from "../../../Components/Dropdown/dropdown/";
import { Fonts, Metrics, Colors, Images } from "../../../Themes/";
import { CachedImage } from "react-native-cached-image";
import AntDesign from "react-native-vector-icons/AntDesign";
import EvilIcons from "react-native-vector-icons/EvilIcons";

import { SocialIcon } from 'react-native-elements'
var ProductData = [];

const WishListItemOne =
  "https://antiqueruby.aliansoftware.net/Images/woocommerce/wishListItemOne.png";
const WishListItemTwo =
  "https://antiqueruby.aliansoftware.net/Images/woocommerce/image_01.jpg";

export default class WishList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ArrivedForWishList: "",
      selectedSize: "M",
      selectedLots: "1",
      isDroDownOpen: false,
      typography: "M",
      cars: [],
      carsId: [],
      ecran: '',
    };
    this.onChangeText = this.onChangeText.bind(this);
    this.getCars();
  }




  _handleLike(id, like) {
    var i;
    for (i = 0; i < ProductData.length; i++) {
      if (ProductData[i].id == id) {
        if (like == true) {
          ProductData[i].isLike = false;
        } else {
          ProductData[i].isLike = true;
        }
      }
    }

    this.setState({
      ProductData: ProductData
    });
    ProductData = ProductData;
  }

  async componentWillMount() {
    AsyncStorage.multiGet([
      "ArrivedFrom",

    ]).then(data => {
      // console.log("catt data[2][1]");
      // console.log(data[2][1]);

      this.setState({
        ecran: data[0][1],

      });
    })
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.state.ecran == "Menu") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (this.state.ecran == "Annonce") {
        this.props.navigation.navigate("Annonces");
      }
      return true;

    });

  }

  getCars() {
    AsyncStorage.getItem('cars', (err, result) => {
      this.setState({
        cars: JSON.parse(result)
      })
      console.log("result" + result);
    });
  }
  async _handleDislike(item, id, like, index) {

    console.log("id" + id);
    console.log("index" + index);

    this.state.cars.splice(index, 1); //remove element




    console.log("new dataaa" + JSON.stringify(this.state.cars));


    try {
      await AsyncStorage.setItem(
        "cars",
        JSON.stringify(this.state.cars)
      );
    } catch (error) {
      console.log(error);
    }
    this.getCars();

    console.log("cars :" + JSON.stringify(this.state.cars));

  }


  _backPress(backFrom) {
    const { ArrivedForWishList } = this.state;

    if (ArrivedForWishList == "ECommerceCategoryProduct") {
      this.props.navigation.navigate("ECommerceCategoryProduct");
    } else if (ArrivedForWishList == "ECommerceSale") {
      this.props.navigation.navigate("ECommerceSale");
    } else if (ArrivedForWishList == "ECommerceMyBag") {
      this.props.navigation.navigate("ECommerceMyBag");
    } else if (ArrivedForWishList == "ECommerceMenu") {
      if (backFrom == "fromHardwareBack") {
        this.props.navigation.navigate("ECommerceMenu");
      }
      if (backFrom == "fromLeftIcon") {
        this.props.navigation.openDrawer();
      }
    } else if (ArrivedForWishList == "ECommerceMyAccount") {
      this.props.navigation.navigate("ECommerceMyAccount");
    } else if (ArrivedForWishList == "ECommerceMyInformation") {
      this.props.navigation.navigate("ECommerceMyInformation");
    } else if (ArrivedForWishList == "ECommerceOrderHistory") {
      this.props.navigation.navigate("ECommerceOrderHistory");
    } else if (ArrivedForWishList == "ECommerceSale") {
      this.props.navigation.navigate("ECommerceSale");
    } else if (ArrivedForWishList == "ECommerceCheckout") {
      this.props.navigation.navigate("ECommerceCheckout");
    } else if (ArrivedForWishList == "ECommerceLogin") {
      this.props.navigation.navigate("ECommerceLogin");
    } else if (ArrivedForWishList == "ECommerceProductDetails") {
      this.props.navigation.navigate("ECommerceProductDetails");
    } else if (ArrivedForWishList == "ECommerceProductDetailsTab") {
      this.props.navigation.navigate("ECommerceProductDetailsTab");
    } else {
      this.props.navigation.navigate("ECommerceMenu");
    }
  }

  _handleProductDetailClick() {
    this.props.navigation.navigate("AnnonceDetails", {
      screenName: "ECommerceWishList"
    });
  }

  _handleBagNavigation() {
    AsyncStorage.multiSet([["ArrivedFrom", "ECommerceWishList"]]);
    this.props.navigation.navigate("ECommerceMyBag");
  }

  onChangeText(text) {
    this.setState({ user_category_id: text });
  }

  onCheckBoxPress(id) {
    this.setState({
      selectedLots: id
    });
  }

  _handleDropDownClick(size) {
    this.setState({ selectedSize: size });
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _openDropDown(id) {
    this.onCheckBoxPress(id);
    this.setState({ isDroDownOpen: !this.state.isDroDownOpen });
  }

  _handleLEFT(ArrivedForWishList) {
    if (ArrivedForWishList == "ECommerceMenu") {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Ionicons name="ios-menu" size={30} color="white" />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => this._backPress("fromLeftIcon")}
        >
          <FontAwesome
            name={I18nManager.isRTL ? "angle-right" : "angle-left"}
            size={35}
            color="white"
            style={{ paddingRight: 20 }}
          />
        </TouchableOpacity>
      );
    }
  }

  render() {
    const { ArrivedForWishList, cars } = this.state;

    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#0e1130", true);
      StatusBar.setTranslucent(true);
    }
    /*  if(this.state.cars == ""){
     return (
      <Container style={styles.main}>
        <Header androidStatusBarColor={"#0e1130"} style={styles.header}>
          <Left style={styles.left}>
          <TouchableOpacity
              style={styles.backArrow}
              onPress={() =>this.props.navigation.navigate("Annonces")}
            >
              <AntDesign
                    name="left"
                    size={20}
                    color={"#ffffff"}
                    style={{
                      alignSelf: "center",
                      marginLeft: Metrics.HEIGHT * 0.03
                    }}
                  />
            </TouchableOpacity>
          </Left>
          <Body style={styles.body}>
            <Text style={styles.textTitle}>Wish List</Text>
          </Body>
          <Right style={styles.right}>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flexDirection: "row" }}>
                <View style={styles.heartBg}>
                  <FontAwesome
                    name="heart"
                    size={Fonts.moderateScale(8)}
                    style={styles.heartIcon}
                  />
                </View>
                <View style={styles.alertBg}>
                  <Text style={styles.alertTxt}>1</Text>
                </View>
              </View>
              <TouchableOpacity
                style={{ flexDirection: "row" }}
                onPress={() => this._handleBagNavigation()}
              >
                <SimpleLineIcons
                  name="handbag"
                  size={Fonts.moderateScale(18)}
                  style={styles.bagIcon}
                />
                <View style={styles.alertBg}>
                  <Text style={styles.alertTxt}>3</Text>
                </View>
              </TouchableOpacity>
            </View>
          </Right>
        </Header>
      
  <Content>
  <View>
  <Image source={require('./aucune.png')} />;
  </View>
  </Content>
  </Container>
  );
                  }
  */

    return (
      <Container style={styles.main}>
        <Header androidStatusBarColor={"#0e1130"} style={styles.header}>
          <Left style={styles.left}>
            <TouchableOpacity
              style={styles.backArrow}
              onPress={() => this.props.navigation.navigate("Annonces")}
            >
              <AntDesign
                name="left"
                size={20}
                color={"#ffffff"}
                style={{
                  alignSelf: "center",
                  marginLeft: Metrics.HEIGHT * 0.03
                }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={styles.body}>
            <Text style={styles.textTitle}>Favoris</Text>
          </Body>
          <Right style={styles.right}>
            <SocialIcon
              type='facebook'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.facebook) }}
            />
            <SocialIcon
              type='instagram'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.instagram) }}
            />
            <SocialIcon
              type='youtube'
              iconSize={15}
              style={{ height: 30, width: 30 }}
              onPress={() => { Linking.openURL(Config.youtube) }}
            />
          </Right>
        </Header>
        {this.state.cars.length > 0 ? (
          <View style={{ width: Metrics.WIDTH, height: Metrics.HEIGHT * 0.9 }}>
            <Content>

              {cars.map((item, index) => {
                return (

                  <View style={styles.row} key={index}>
                    <TouchableOpacity
                      style={styles.rowBg}
                      onPress={() => this._handleProductDetailClick()}
                    >
                      <CachedImage
                        source={item.productImage}
                        style={styles.productImage}
                      >
                        {item.isLike == true ? (
                          <TouchableOpacity
                            onPress={() => this._handleLike(item.id, item.isLike)}
                          >
                            <FontAwesome
                              name="heart"
                              size={18}
                              color={"red"}
                              style={styles.heartListIcon}
                            />
                          </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                              onPress={() => this._handleDislike(item.id,
                                item.isLike,
                                item.index
                              )}
                            >
                              <FontAwesome
                                name="heart"
                                size={18}
                                color={"#cecece"}
                                style={styles.heartListIcon}
                              />
                            </TouchableOpacity>
                          )}
                      </CachedImage>

                      <View style={styles.productDetailBg}>
                        <Text style={styles.productTitleTxt}>
                          {item.productTitle}
                        </Text>

                        <View
                          style={[
                            styles.detailFieldRow,
                            {
                              paddingTop: Metrics.WIDTH * 0.01,
                              paddingBottom: Metrics.WIDTH * 0.01
                            }
                          ]}
                        >
                          <Text style={styles.priceTxt}>{item.price}</Text>
                          <TouchableOpacity
                            style={styles.closeIconBg}
                            onPress={() => this._handleDislike(item, item.id, item.isLike)}
                          >
                            <FontAwesome
                              name="close"
                              size={12}
                              style={styles.closeImg}
                            />
                          </TouchableOpacity>
                        </View>

                        <View
                          style={[
                            styles.detailFieldRow,
                            { marginTop: Metrics.WIDTH * 0.07 }
                          ]}
                        >
                          <EvilIcons
                            name="calendar"
                            size={35}
                            color={"#000000"}
                          />

                          <View>
                            <Text style={styles.priceTxt}>  1996</Text>
                          </View>
                        </View>

                        <View
                          style={[
                            styles.detailFieldRow,
                            {
                              marginTop: Metrics.WIDTH * 0.03,
                              marginBottom: Metrics.HEIGHT * 0.015
                            }
                          ]}
                        >
                          <Entypo
                            name="gauge"
                            size={30}
                            color={"#000000"}

                          />
                          <View style={styles.listColorBg}>
                            <Text style={styles.priceTxt}>190.96KM</Text>
                          </View>
                        </View>


                      </View>
                      {this.state.selectedLots == item.id ? (
                        <View>
                          {this.state.isDroDownOpen == true ? (
                            <View style={styles.listDropDown}>
                              {item.size.map((itemSize, index) => {
                                return (
                                  <TouchableOpacity
                                    onPress={() =>
                                      this._handleDropDownClick(itemSize.value)
                                    }
                                    key={index}
                                  >
                                    <Text>{itemSize.value}</Text>
                                  </TouchableOpacity>
                                );
                              })}
                            </View>
                          ) : null}
                        </View>
                      ) : null}
                    </TouchableOpacity>
                    <View
                      style={
                        item.id === ProductData.length ? null : styles.listDivider
                      }
                    />
                  </View>
                );
              })}
            </Content>
          </View>
        ) : (

            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: "80%"
            }}>
              <Image style={{
                height: 200, width: 270
              }} source={require('./aucune.png')} />
            </View>

          )}

      </Container>
    );
  }
}
