import React, { Component } from "react";
import DialogInput from 'react-native-dialog-input';
import {
  Text,
  View,
  Image,
  StatusBar,
  Platform,
  ImageBackground,
  TouchableOpacity,
  BackHandler,
  I18nManager,
  Alert,
  AsyncStorage,

} from "react-native";
import {
  Container,
  Button,
  Icon,
  Right,
  Item,
  Input,
  Header,
  Left,
  Body,
  Title,
  Form
} from "native-base";
import FontAwesome from "react-native-vector-icons/FontAwesome";
// Screen Styles
import styles from "./styles";
import { Images } from "../../../Themes/";
import Config from "../Config/config";
import axios from "axios";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
export default class Signin_04 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emairen: "",
      isDialogVisible: false,
    }
  }
  componentWillMount() {
    var that = this;
    BackHandler.addEventListener("hardwareBackPress", function () {
      that.props.navigation.navigate("ECommerceMenu");
      return true;
    });
  }
  GetSectionListItem = (item) => {
    Alert.alert(item)
  }
  showDialog(isShow) {
    this.setState({ isDialogVisible: isShow });
  }
  sendInput(emairen) {
   console.log("email" + emairen)
    axios.post(Config.SERVER_URL + '/api/v1/forgotpassword', {
      email: emairen
    }).then(response => {
      console.log("******"+response.data)
      if (response.data == "OK") {
        Alert.alert("votre mot de passe a ete reinitialiser")
      }
      else {
        Alert.alert("Veuillez réssayer plutard")
      }
    }).catch((error) => {
      console.log(error)
    });
  }
  reinitialpwd() {

  }


  login() {


    axios.post(Config.SERVER_URL + '/api/v1/login', {


      email: this.state.email,
      password: this.state.password,

    })
      .then(response => {
        console.log("repossssssssse" + JSON.stringify(response.data));
        //console.log("repossssssssse" + response.data.error);
        console.log("repossssssssse.token" + JSON.stringify(response.data.token));
        if (response.data.token != null) {
          AsyncStorage.setItem("token", response.data.token);
          this.props.navigation.navigate("MonCompte")

        }
        else if (response.data.error == "email ou mot de passe incorrect") {
          this.GetSectionListItem("email ou mot de passe incorrect")
        }
        else if (response.data.error == "Un email de verification vous a été envoyer activer votre compte") {


          this.GetSectionListItem("Un email de verification vous a été envoyer activer votre compte")
        }
        else {
          this.GetSectionListItem("Une erreur est survenue")
        }


      })
      .catch((error) => {
        console.log("teeest");
        console.log(error);
        //this.onLoginFail();

      });
  }


  render() {

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <Container>
        <DialogInput isDialogVisible={this.state.isDialogVisible}
          title={"Reintialiser votre mot de passe"}
          message={"Email"}
          hintInput={"Email"}
          submitInput={(emairen) => { this.sendInput(emairen) }}
          closeDialog={() => { this.showDialog(false) }}>
        </DialogInput>
        <KeyboardAwareScrollView>
          <ImageBackground source={{ uri: "https://images.pexels.com/photos/1037995/pexels-photo-1037995.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" }} style={styles.backgroundImage}>
            <Header style={styles.header}>
              <Left style={styles.left}>
                <TouchableOpacity
                  style={styles.backArrow}
                  onPress={() => this.props.navigation.navigate("Home")}
                >
                  <FontAwesome
                    name={I18nManager.isRTL ? "angle-right" : "angle-left"}
                    size={30}
                    color="white"
                  />
                </TouchableOpacity>
              </Left>
              <Body style={styles.body} />
              <Right style={styles.right} />
            </Header>
            <View style={styles.logosec}>
              <Image source={{ uri: "https://www.codemediastudioad.com/images/logo.png" }} style={styles.logostyle} />
            </View>
            <Form style={styles.form}>
              <Item rounded style={styles.inputStyle}>
                <Input
                  placeholderTextColor="#ffffff"
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  placeholder="Email"
                  style={styles.inputmain}
                  onChangeText={email => { this.setState({ email }) }}
                />
              </Item>
              <Item rounded style={[styles.inputStyle, { marginTop: 10 }]}>
                <Input
                  placeholderTextColor="#ffffff"
                  placeholder="Mot de passe"
                  secureTextEntry={true}
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  style={styles.inputmain}
                  onChangeText={password => { this.setState({ password }) }}
                />
              </Item>
              <TouchableOpacity
                info
                style={styles.signInbtn}
                onPress={() => this.login()}
              >
                <Text autoCapitalize="words" style={styles.buttongetstarted}>
                  Se connecter
              </Text>
              </TouchableOpacity>
            </Form>
            <View style={styles.editInfoMainView}>
              <View style={{ flexDirection: 'row', }}>

                <TouchableOpacity
                  style={styles.editInfoView}
                  onPress={() => this.props.navigation.navigate("register")}
                >
                  <Text style={styles.buttongetstarted}>S'inscrire</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.editInfoView}
                  onPress={() => { this.showDialog(true) }}
                >
                  <Text style={styles.buttongetstarted}>Mot de passe oublié ?</Text>
                </TouchableOpacity>
              </View>

            </View>
          </ImageBackground>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}
