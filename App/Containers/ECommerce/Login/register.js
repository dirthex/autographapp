import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  I18nManager,
  Image,
  Alert,
  Button,
  BackHandler,
} from 'react-native';
import styles from "./styles";
import { Metrics, Fonts, Colors, Images } from "../../../Themes";
import {
  Container,
  Content,
  Right,
  Form,
  Item,
  Label,
  Input,
  Left,
  Icon,
  Header,
  Body,

} from "native-base";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import Foundation from "react-native-vector-icons/Foundation"
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Config from "../Config/config";
import axios from "axios";


const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 16.265;
const LONGITUDE = -61.551;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

export default class Location extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: [
        {
          value: "Professionnel"
        },
        {
          value: "Particulier"
        }
      ],
      nom: "",
      email: "",
      password: "",
      siren: "",
      siret: "",
      longitude: "",
      latitude: "",
      tel: "",
      typeValue: "",
      idType: "",
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: {
        coordinate: {
					latitude: 16.265,
					longitude:  -61.551,
        },
        key: id,

      }
    };
  }
  openDialog = (show) => {
		this.setState({ showDialog: show });
  }
  openDialo = (show) => {
		this.setState({ showDialo: show });
	}

  componentWillMount(){
    BackHandler.addEventListener('hardwareBackPress', () => {
    
        this.props.navigation.navigate("ECommerceLogin");
     
      return true;

    });
  }
	openConfirm = (show) => {
		this.setState({ showConfirm: show });
	}

	openProgress = () => {
		this.setState({ showProgress: true });

		setTimeout(
			() => {
				this.setState({ showProgress: false });
			},
			4000,
		);
	}

	optionYes = () => {
		this.openConfirm(false);
		// Yes, this is a workaround :(
		// Why? See this https://github.com/facebook/react-native/issues/10471
		setTimeout(
			() => {
				Alert.alert("The YES Button touched!");
			},
			300,
		);
	}

	optionNo = () => {
		this.openConfirm(false);
		// Yes, this is a workaround :(
		// Why? See this https://github.com/facebook/react-native/issues/10471
		setTimeout(
			() => {
				Alert.alert("The NO Button touched!");
			},
			300,
		);
	}
  onMapPress(e) {
    this.setState({
      markers:
      {
        coordinate: e.nativeEvent.coordinate,
        key: id++,

      },
    });

    SaveAddress = () => {
      console.log(JSON.stringify(this.state.markers[0].coordinate.latitude))
    }
    console.log("eeee" + JSON.stringify(this.state.markers.coordinate.latitude) + JSON.stringify(this.state.markers.coordinate.longitude))
  }
  _renderFooter = () => {


    // Sign Up

    return (
      <TouchableOpacity
        style={styles.facebookBtnBg}
        onPress={() => this.inscription()}>
        <Text style={styles.loginWithFacebookTxt}>S'inscrire</Text>
      </TouchableOpacity>
    );

  };
  GetSectionListItem = (item) => {
    Alert.alert(item)
  }
  inscription() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if(this.state.email =="" || this.state.password =="" || this.state.password.length < 8 ||
     this.state.tel ==""  )
     { 
       this.openDialog(true)
     }
     
    else if (this.state.type == "Professionnel" ) {
    if(this.state.siren =="" && this.state.siret ==""){
      this.openDialo(true)
    }else{
      axios.post(Config.SERVER_URL + '/registre', {

        nom: this.state.nom,
        email: this.state.email,
        password: this.state.password,
        siren: this.state.siren,
        siret: this.state.siret,
        long: this.state.markers.coordinate.longitude,
        lat: this.state.markers.coordinate.latitude,
        tel: this.state.tel,
        type: this.state.idType,
      })
        .then(response => {
          console.log("repossssssssse" + JSON.stringify(response.data));
          console.log("repossssssssse" + response.data);
          /*if(response.data == "depense créer avec succés") {
              this.GetSectionListItem("Nouvelle dépense ajoutée");
             
          }
          else if (response.data == "seulement 20 opérations par catégorie sont auorisées") {
              this.GetSectionListItem(response.data);
          }
          else {
                  this.GetSectionListItem("Depense n'a pas été crée , veuillez réessayer");
          }*/

          // this.props.navigation.navigate('signIn')

        })
        .catch((error) => {
          console.log("teeest");
          console.log(error);
          //this.onLoginFail();

        });
      }
    }
    else {
      axios.post(Config.SERVER_URL + '/registre', {

        nom: this.state.nom,
        email: this.state.email,
        password: this.state.password,
        tel: this.state.tel,
        type: 1,
      })
        .then(response => {
          console.log("repossssssssse" + JSON.stringify(response.data));
          console.log("repossssssssse" + response.data);
          if (response.data == "200") {
            this.GetSectionListItem(" Un email de confirmation vous a été envoyer");

          }
          else if (response.data == "400") {
            this.GetSectionListItem("Email déjà existant ");
          }
          else {
            this.GetSectionListItem("Une erreur est survenue , ressayer plus tard");
          }

          // this.props.navigation.navigate('signIn')

        })
        .catch((error) => {
          console.log("teeest");
          console.log(error);
          //this.onLoginFail();

        });
    }
  }
  getValueoType = (value) => {
    this.setState({ typeValue: value })
  }

  render() {
    return (
      <Container>
        <KeyboardAwareScrollView>


          <View style={styles.formview}>
            <View style={{
              width: Metrics.WIDTH,
              height: Metrics.WIDTH * 0.7,
              justifyContent: "flex-end",
              backgroundColor: "#dddbca",
              alignItems: "center"
            }}>
              <Image source={{uri:"https://www.codemediastudioad.com/images/logo.png"}} style={styles.logostyle2} />
            </View>
            
            <Form style={styles.inputLabel}>
              <Item underline style={styles.itememail}>
                <SimpleLineIcons name="user" color="#b7b7b7" size={17} />
                <Input
                  placeholderTextColor="#b7b7b7"
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  placeholder="Full Name"
                  style={styles.inputemail}
                  onChangeText={nom => this.setState({ nom })}
                />
              </Item>

              <Item underline style={styles.itememail}>
                <MaterialIcons name="mail-outline" color="#b7b7b7" size={19} />
                <Input
                  placeholderTextColor="#b7b7b7"
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  placeholder="Email"
                  style={styles.inputemail}
                  onChangeText={email => this.setState({ email })}
                />
              </Item>

              <Item underline style={styles.itempassword}>
                <SimpleLineIcons name="lock-open" color="#b7b7b7" size={17} />
                <Input
                  placeholderTextColor="#b7b7b7"
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  secureTextEntry={true}
                  placeholder="Password"
                  style={styles.inputpassword}
                  onChangeText={password => this.setState({ password })}
                />
              </Item>

              <Item underline style={styles.itempassword}>
                <SimpleLineIcons name="lock-open" color="#b7b7b7" size={17} />
                <Input
                  placeholderTextColor="#b7b7b7"
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  secureTextEntry={true}
                  placeholder="Confirm Password"
                  style={styles.inputpassword}
                />
              </Item>
              <Item underline style={styles.itememail}>
                <SimpleLineIcons name="phone" color="#b7b7b7" size={17} />
                <Input
                  placeholderTextColor="#b7b7b7"
                  textAlign={I18nManager.isRTL ? "right" : "left"}
                  placeholder="Tele"
                  style={styles.inputemail}
                  keyboardType={'numeric'}
                  onChangeText={tel => this.setState({ tel })}
                />
              </Item>
              <View underline style={{
                alignSelf: "center",
                marginLeft: "6%",
                width: Metrics.WIDTH * 0.8,
                height: 60
              }}>
                <Dropdown
                  label="Type"
                  data={this.state.type}
                  textColor={'#959595'}
                  onChangeText={value => this.getValueoType(value)}
                //value= {this.state.positionChoisi}
                />
              </View>
              {this.state.typeValue == "Professionnel" ? (
                <View>

                  <Item underline style={styles.itememail}>
                    <Foundation name="list-bullet" color="#b7b7b7" size={17} />
                    <Input
                      placeholderTextColor="#b7b7b7"
                      textAlign={I18nManager.isRTL ? "right" : "left"}
                      placeholder="Numero Siret*"
                      keyboardType={'numeric'}
                      style={styles.inputemail}
                      onChangeText={siret => this.setState({ siret })}
                    />
                  </Item>
                  <Item underline style={styles.itememail}>
                    <Foundation name="list-bullet" color="#b7b7b7" size={17} />
                    <Input
                      placeholderTextColor="#b7b7b7"
                      textAlign={I18nManager.isRTL ? "right" : "left"}
                      placeholder="Numero Siren*"
                      style={styles.inputemail}
                      keyboardType={'numeric'}
                      onChangeText={siren => this.setState({ siren })}
                    />
                  </Item>
                 
                </View>
              ) : null}
            </Form>
            {this.state.typeValue =="Professionnel" ?(
            <MapView
              provider={this.props.provider}
              style={styles.MapView}
              initialRegion={this.state.region}
              onPress={e => this.onMapPress(e)}
            >

              <Marker
                key={this.state.markers.key}
                coordinate={this.state.markers.coordinate}

              />



            </MapView>
            ):null}
            

          </View>

        </KeyboardAwareScrollView>
        <Dialog
                    title="Erreur"
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                        }
                    }
                    onTouchOutside={ () => this.openDialog(false) }
                    visible={ this.state.showDialog }
                >
					 <Image
                        source={
                            
                                Images.croix
                            
                        }
                        style={
                            {
                                width: 99,
                                height: 87,
                                backgroundColor: "black",
                                marginTop: 10,
								resizeMode: "contain",
								backgroundColor:"white"
                            }
                        }
                    />
                   
                    <Text style={ { marginVertical: 30 } }>
					Tous les champs sont obligatoires
					    </Text>

						<Button
                        onPress={ () => this.openDialog(false) }
                        style={ { marginTop: 10 } }
                        title="CLOSE"
                   />

					
                </Dialog>

                <Dialog
                    title="Erreur"
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                        }
                    }
                    onTouchOutside={ () => this.openDialo(false) }
                    visible={ this.state.showDialo }
                >
					 <Image
                        source={
                            
                                Images.croix
                            
                        }
                        style={
                            {
                                width: 99,
                                height: 87,
                                backgroundColor: "black",
                                marginTop: 10,
								resizeMode: "contain",
								backgroundColor:"white"
                            }
                        }
                    />
                   
                    <Text style={ { marginVertical: 30 } }>
					Remplissez le Siren ou le Siret
					    </Text>

						<Button
                        onPress={ () => this.openDialo(false) }
                        style={ { marginTop: 10 } }
                        title="CLOSE"
                   />

					
                </Dialog>

        <View style={styles.bottomView}>{this._renderFooter()}</View>
      </Container>


    );
  }
}


