import { Platform, StyleSheet, Dimensions } from "react-native";
// Screen Styles
import { Fonts, Metrics, Colors } from "../../../Themes/";

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: Metrics.WIDTH,
	height: Metrics.HEIGHT,
	backgroundColor:"#23292e",
  },

  header: {
    backgroundColor: Colors.transparent,
    height: Metrics.WIDTH * 0.15,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {},
      android: {
        marginTop: Fonts.moderateScale(25)
      }
    }),
    elevation: 0
  },
  editInfoMainView:{
    bottom: 0,
    position:'absolute',
    backgroundColor:"#dfdbdb38",
    width:Metrics.WIDTH
  },
  editInfoView:{
    padding: Metrics.HEIGHT * 0.015,
    margin: Metrics.HEIGHT * 0.015,
    flex: 1,
	backgroundColor: "#23292e",
    borderRadius: 5,
    justifyContent: 'center',
    textAlign:"center",
    alignItems: 'center'
  },
  left: {
    flex: 0.5,
    backgroundColor: "transparent"
  },
  backArrow: {
    width: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    flex: 3,
    alignItems: "center",
    backgroundColor: "transparent"
  },
  logosec: {
    width: Metrics.WIDTH,
	height: Metrics.WIDTH * 0.3,
	marginTop: Metrics.WIDTH * 0.2,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  logostyle: {
	alignSelf: "center",
    width: Metrics.WIDTH * 0.6,
    height: Metrics.HEIGHT * 0.2	
  },
  logostyle2: {
	alignSelf: "center",
    width: Metrics.WIDTH * 0.6,
    height: Metrics.HEIGHT * 0.3	
  },
  form: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT * 0.4,
    backgroundColor: "transparent",
    borderColor: "transparent",
    justifyContent: "center",
    alignSelf: "center"
  },
  inputStyle: {
    borderColor: "transparent",
    justifyContent: "center",
    alignSelf: "center",
    width: Metrics.WIDTH * 0.8
  },
  inputmain: {
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: Colors.snow,
    justifyContent: "center",
    alignSelf: "center",
    paddingTop: 12,
    paddingBottom: 10,
    paddingLeft: 20,
    borderRadius: 40,
    width: Metrics.WIDTH * 0.8,
    backgroundColor: "#000"
  },
  signInbtn: {
    backgroundColor: Colors.red,
    justifyContent: "center",
    alignSelf: "center",
    paddingTop: 12,
    paddingBottom: 12,
    borderRadius: 40,
    width: Metrics.WIDTH * 0.8,
    marginTop: 35
  },
  buttongetstarted: {
    alignSelf: "center",
    justifyContent: "center",
	fontFamily: Fonts.type.sfuiDisplaySemibold,
	fontSize: Fonts. moderateScale(14),
    color: Colors.snow
  },
  buttongettext: {
    alignSelf: "center",
    justifyContent: "center",
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    color: Colors.white,
    marginTop: 25
  },
  bottomView: {
    marginTop: 30
  },
  fbButton: {
    backgroundColor: "#3b5998",
    height: Metrics.HEIGHT * 0.07,
    width: Metrics.WIDTH * 0.8,
    borderRadius: 40,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  fbview: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center"
  },
  fbButtonText: {
    color: "#fff",
    fontSize: Fonts.moderateScale(17),
    left: 10,
    fontFamily: Fonts.type.sfuiDisplayRegular,
    alignItems: "center",
    alignSelf: "center"
  },
  bottomText: {
    width: "100%",
    height: 40,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 20,
    flexDirection: "row"
  },

  bottomText01: {
    fontSize: Fonts.moderateScale(16),
    color: "#FFFFFF",
    fontFamily: Fonts.type.sfuiDisplayRegular
  },

  bottomText02: {
    fontSize: Fonts.moderateScale(16),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: "#969696"
  },
  container: {
		height: Metrics.HEIGHT,
		width: Metrics.WIDTH,
		backgroundColor: Colors.snow,
	},

	header: {
		backgroundColor: '#23292e',
		borderBottomWidth: 0,
		...Platform.select({
			ios: {
				paddingTop: Metrics.HEIGHT * 0.02,
				height: Metrics.HEIGHT * 0.1,
			},
			android: {
				paddingTop: Metrics.HEIGHT * 0.02,
				height: Metrics.HEIGHT * 0.1,
			},
		}),
		elevation: 0,
		paddingLeft: Metrics.WIDTH * 0.05,
		paddingRight: Metrics.WIDTH * 0.05,
	},

	left: {
		flex: 1,
		backgroundColor: Colors.transparent,
	},

	backArrow: {
		justifyContent: 'center',
		alignItems: 'center',
	},

	body: {
		flex: 2,
		alignItems: 'center',
		backgroundColor: Colors.transparent,
	},

	textTitle: {
		color: Colors.snow,
		fontSize: Fonts.moderateScale(20),
		alignSelf: 'center',
		fontFamily: Fonts.type.helveticaNeueLight,
		...Platform.select({
			ios: {},
			android: {
				paddingTop: Metrics.WIDTH * 0.02,
			},
		}),
	},

	right: {
		flex: 1,
		backgroundColor: Colors.transparent,
		...Platform.select({
			ios: {},
			android: {
				height: Metrics.HEIGHT * 0.06,
				marginTop: -(Metrics.WIDTH * 0.03),
			},
		}),
	},

	heartBg: {
		width: Metrics.WIDTH * 0.054,
		height: Metrics.WIDTH * 0.054,
		borderRadius: Metrics.WIDTH * 0.027,
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: Colors.snow,
		alignItems: 'center',
		justifyContent: 'center',
	},

	bagIcon: {
		marginLeft: Metrics.WIDTH * 0.04,
		color: Colors.snow,
	},
	input: {    
		borderBottomWidth: 1.5, 
		marginLeft: 20,
		color: '#333',       
	  },

	heartIcon: {
		color: Colors.snow,
		alignSelf: 'center',
	},
	HeaderBg: {
		backgroundColor: "#23292e",
		borderBottomWidth: 1
	  },
	

	content: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT * 0.8,
	},

	bottomView: {
		width: Metrics.WIDTH,
		alignItems: 'center',
		justifyContent: 'center',
		height: Metrics.HEIGHT * 0.094,
	},

	divider: {
		backgroundColor: '#d8d8d8',
		width: Metrics.WIDTH,
		height: Metrics.WIDTH * 0.003,
	},

	loginSignUpTxt: {
		fontSize: Fonts.moderateScale(16),
		fontFamily: Fonts.type.sfuiDisplayRegular,
	},

	facebookBtnBg: {
		backgroundColor: '#dddbca',
		width: Metrics.WIDTH * 0.96,
		alignSelf: 'center',
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: Metrics.WIDTH * 0.025,
		paddingBottom: Metrics.WIDTH * 0.025,
		
	},

	loginWithFacebookTxt: {
		color: "black",
		fontSize: Fonts.moderateScale(17),
		// fontFamily: Fonts.type.sfuiDisplayLight,
		marginLeft: Metrics.WIDTH * 0.02,
	},

	loginSignUpContent: {
		...Platform.select({
			ios: {
				height: Metrics.HEIGHT * 0.714,
			},
			android: {
				height: Metrics.HEIGHT * 0.7,
			},
		}),
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
	},

	textInput: {
		height: Metrics.HEIGHT * 0.07,
		alignSelf: 'center',
		width: Metrics.WIDTH * 0.9,
		fontSize: Fonts.moderateScale(14),
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: '#959595',
		marginLeft: 15,
		paddingLeft: 15,
		marginTop: Metrics.HEIGHT * 0.02,
		backgroundColor: 'red',
	},

	item: {
		alignSelf: 'center',
		width: Metrics.WIDTH * 0.94,
		justifyContent: 'center',
	},
	ListContentMain: {
		backgroundColor: "#f5f5f5",
		marginLeft: Metrics.HEIGHT * 0.007,
		marginRight: Metrics.HEIGHT * 0.007,
		...Platform.select({
		  ios: {
			shadowOpacity: 0.4,
			borderWidth: 1
		  },
		  android: {
			shadowOpacity: 0.1
		  }
		}),
		shadowColor: "gray",
		shadowOffset: { width: 2, height: 2 },
		shadowRadius: 4,
		elevation: 3,
		borderColor: "#e8e8e8",
		marginBottom: Metrics.HEIGHT * 0.03,
		borderRadius: 2,
		marginTop: Metrics.HEIGHT * 0.02,
		height:"94%"
	  },
	

	floatingView: {
		alignSelf: 'center',
		width: Metrics.WIDTH * 0.94,
		justifyContent: 'center',
		marginTop: Metrics.HEIGHT * 0.015,
	},

	inputemail: {
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: '#959595',
		fontSize: Fonts.moderateScale(15),
	},
	textLabel: {
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: '#959595',
		fontSize: Fonts.moderateScale(15),
		marginLeft: 10,
		fontSize: 10,
	},

	forgotPasswordTxt: {
		color: '#0691ce',
		fontSize: Fonts.moderateScale(16),
		fontFamily: Fonts.type.sfuiDisplayRegular,
	},

	loginBg: {
		backgroundColor: '#0691ce',
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
		height: Metrics.HEIGHT * 0.06,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 5,
	},

	alertBg: {
		width: Metrics.WIDTH * 0.03,
		height: Metrics.WIDTH * 0.03,
		borderRadius: Metrics.WIDTH * 0.015,
		backgroundColor: '#ff0000',
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: -(Metrics.WIDTH * 0.018),
	},

	alertTxt: {
		fontSize: Fonts.moderateScale(8),
		fontFamily: Fonts.type.sfuiDisplayMedium,
		color: Colors.snow,
	},

	dropdown: {
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
		height: Metrics.HEIGHT * 0.07,
		backgroundColor: 'red',
	},

	picker: {
		width: Metrics.WIDTH * 0.94,
		alignSelf: 'center',
		height: Metrics.HEIGHT * 0.07,
		backgroundColor: 'green',
	},

	activeTab: {
		color: Colors.snow,
		fontFamily: Fonts.type.sfuiDisplayRegular,
		fontSize: Fonts.moderateScale(14),
	},

	normalTab: {
		color: '#0691ce',
		fontFamily: Fonts.type.sfuiDisplayRegular,
		fontSize: Fonts.moderateScale(14),
	},

	segmentTab: {
		...Platform.select({
			ios: {
				width: Metrics.WIDTH * 0.467,
				height: 31,
			},
			android: {
				width: Metrics.WIDTH * 0.465,
				height: 29,
			},
		}),
		backgroundColor: '#FFF',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
	},

	segmentSelectedTab: {
		...Platform.select({
			ios: {
				width: Metrics.WIDTH * 0.467,
				height: 31,
			},
			android: {
				width: Metrics.WIDTH * 0.465,
				height: 29,
			},
		}),
		backgroundColor: '#0691ce',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
	},

	contentOne: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT * 0.8,
	},

	segmentSelectedTabOne: {
		width: Metrics.WIDTH * 0.47,
		height: Metrics.HEIGHT * 0.05,
		backgroundColor: '#0691ce',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
		borderRadius: Metrics.HEIGHT * 0.007,
	},

	segmentTabOne: {
		width: Metrics.WIDTH * 0.47,
		height: Metrics.HEIGHT * 0.05,
		backgroundColor: 'transparent',
		borderColor: '#0691ce',
		justifyContent: 'center',
		alignSelf: 'center',
	},

	segmentTabSecOne: {
		width: Metrics.WIDTH * 0.94,
		height: Metrics.HEIGHT * 0.05,
		marginVertical: Metrics.HEIGHT * 0.03,
		borderRadius: Metrics.HEIGHT * 0.007,
		backgroundColor: Colors.snow,
		borderColor: '#0691ce',
		padding: 0,
		borderWidth: 1,
		alignSelf: 'center',
	},

	activeTabOne: {
		color: Colors.snow,
		//  fontFamily: Fonts.type.helveticaNeueLight,
		fontSize: Fonts.moderateScale(15),
	},

	normalTabOne: {
		color: '#0691ce',
		//  fontFamily: Fonts.type.helveticaNeueLight,
		fontSize: Fonts.moderateScale(15),
	},

	segmentTabSec: {
		padding: 0,
		marginTop: 0,
		backgroundColor: Colors.snow,
		borderWidth: 1,
		borderRadius: 5,
		borderColor: '#0691ce',
		width: Metrics.WIDTH * 0.94,
		height: 32,
		alignSelf: 'center',
		marginTop: Metrics.WIDTH * 0.02,
	},
	map:{
		flex:1
	},
	MapView: {
		width: Metrics.WIDTH,
		...Platform.select({
		  ios: {
			height: Metrics.HEIGHT * 0.5
		  },
		  android: {
			height: Metrics.HEIGHT * 0.5
		  }
		}),
	  },
	forgotPasswordBg: {
		alignSelf: 'flex-end',
		marginTop: Metrics.HEIGHT * 0.015,
		marginRight: Metrics.HEIGHT * 0.015,
	},
	container: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT,
		backgroundColor: "white"
	  },
	
	  header: {
		backgroundColor: Colors.transparent,
		height: 55,
		borderBottomWidth: 0,
		...Platform.select({
		  ios: {},
		  android: {
			marginTop: Fonts.moderateScale(25)
		  }
		}),
		elevation: 0
	  },
	  left: {
		flex: 0.5,
		backgroundColor: "transparent"
	  },
	  backArrow: {
		width: 30,
		alignItems: "center"
	  },
	  body: {
		flex: 3,
		alignItems: "center",
		backgroundColor: "transparent",
		justifyContent: "center"
	  },
	  textTitle: {
		color: "#000",
		fontSize: Fonts.moderateScale(16),
		marginTop: 5,
		alignSelf: "center",
		fontFamily: Fonts.type.sfuiDisplaySemibold
	  },
	  right: {
		flex: 0.5,
		backgroundColor: "transparent"
	  },
	  main: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT,
		backgroundColor: "transparent"
	  },
	
	  titleBar: {
		width: Metrics.WIDTH,
		height: 50,
		backgroundColor: "#fff",
		marginTop: 15,
		flexDirection: "row",
		padding: 10
	  },
	
	  textTitle: {
		color: Colors.darktext,
		fontSize: Fonts.moderateScale(16),
		alignSelf: "center",
		fontFamily: Fonts.type.SFUIDisplaySemibold
	  },
	
	  textNext: {
		color: Colors.lightgrey,
		fontSize: Fonts.moderateScale(18),
		fontFamily: Fonts.type.SFUIDisplayMedium
	  },
	
	  profileImage: {
		width: 90,
		height: 90,
		borderRadius: 45,
		borderColor: "white",
		borderWidth: 3,
		alignSelf: "center",
		marginBottom: 20
	  },
	
	  inputLabel: {
		alignSelf: "center",
		width: Metrics.WIDTH,
		backgroundColor: "white",
		
	  },
	  formview: {
		backgroundColor: "#e6e6e6",
		width: Metrics.WIDTH,
	  },
	
	  inputemail: {
		marginLeft: 10,
		marginTop: 2,
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: Colors.shadows
	  },
	  tandc: {
		flexDirection: "row",
		width: Metrics.WIDTH,
		justifyContent: "center"
	  },
	  and: {
		color: Colors.darktext,
		fontSize: Fonts.moderateScale(12),
		fontFamily: Fonts.type.SFUIDisplayRegular
	  },
	  itememail: {
		marginBottom: Metrics.HEIGHT * 0.01,
		alignSelf: "center",
		width: Metrics.WIDTH * 0.8,
		height: 50
	  },
	  facebookBtnBg: {
		backgroundColor: '#fbe122',
		width: Metrics.WIDTH * 0.96,
		alignSelf: 'center',
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: Metrics.WIDTH * 0.025,
		paddingBottom: Metrics.WIDTH * 0.025,
		
	  },
	  bottomView: {
		width: Metrics.WIDTH,
		alignItems: 'center',
		justifyContent: 'center',
		height: Metrics.HEIGHT * 0.094,
	  },
	  inputpassword: {
		marginLeft: 10,
		marginTop: 2,
		fontFamily: Fonts.type.SFUIDisplayRegular,
		color: Colors.shadows
	  },
	
	  itempassword: {
		alignSelf: "center",
		marginTop: 5,
		width: Metrics.WIDTH * 0.8,
		height: 50
	  },
	
	  itemconpassword: {
		alignSelf: "center",
		marginTop: 5,
		width: Metrics.WIDTH * 0.8,
		height: 50,
		borderColor: "rgba(255,255,255,.4)"
	  },
	
	  iconitem: {
		color: Colors.shadows
	  },
	
	  textPolicyDescription: {
		color: Colors.darktext,
		fontSize: Fonts.moderateScale(12),
		fontFamily: Fonts.type.SFUIDisplayRegular,
		alignSelf: "center",
		marginTop: 20
	  },
	
	  textTermsCondition: {
		color: Colors.loginBlue,
		fontSize: Fonts.moderateScale(12),
		fontFamily: Fonts.type.SFUIDisplaySemibold
	  }
});
export default styles;
