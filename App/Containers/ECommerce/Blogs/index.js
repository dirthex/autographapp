import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  ListView,
  Linking,
  Easing,
  Platform,
  RefreshControl,
  AsyncStorage,
  ScrollView,
  ActivityIndicator,
  FlatList
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {
  Content,
  Container,
  Icon,
  Right,
  Header,
  Left,
  Title,
  Body
} from "native-base";
import styles from "./styles";
import Config from "../Config/config";
import axios from "axios";
import { withNavigation } from 'react-navigation';

import { Images, Metrics, Fonts, Colors } from "../../../Themes/";

import AntDesign from "react-native-vector-icons/AntDesign";







class Favorites extends Component {
  constructor(props) {
    super(props);
    this.page=1;
    this.state = {
      blogs:[],
      blog:[], 
    
      headr:'',
      loading: false, // user list loading
      isRefreshing: false, 
    
     
    };
   /* this.props.navigation.addListener('willFocus', () => {
    
      this.getBlog();
    //  this.getBlog();
     // this.getMagazine();
      //this.getOffre();
      
          });*/
  }
  getBlog(page){
    console.log("dkhel 622")
    
    axios.get(Config.SERVER_URL+'/api/v1/blogs/'+this.page
    ).then((res) => {
      this.setState({
    
        blogs:res.data.blogs,
        loading: false,


    });  
    
   
    for(var i=0 ; i<this.state.blogs.length ;i++){
      this.state.blog.push({
    id:this.state.blogs[i].id,
    FoodName: this.state.blogs[i].titre,
    Foodimg: Config.SERVER_URL + this.state.blogs[i].cover,
    Description :  this.state.blogs[i].resume
   })
  }
  console.log("blogsyyyy   ajajajaj" + JSON.stringify(this.state.blog))

  
  
  
  



}).catch((error) => {
    this.setState({
        error: 'Error retrieving data',
        loading: false
    });
});


  }
  onRefresh(){
    this.setState({isRefreshing : true})
    axios.get(Config.SERVER_URL+'/api/v1/blogs/'+this.page
     ).then((res) => {
       this.setState({
     
        blogs:res.data.blogs,
          isRefreshing:false,
 
 
     });  
     
    
  
     for(var i=0 ; i<this.state.blogs.length ;i++){
      this.state.blog.push({
    id:this.state.blogs[i].id,
    FoodName: this.state.blogs[i].titre,
    Foodimg: Config.SERVER_URL + this.state.blogs[i].cover,
    Description :  this.state.blogs[i].resume
   })
  }
   console.log("blogsyyyy   ajajajaj" + JSON.stringify(this.state.Magazine))
 
   
   
   
   
 
 
 
 }).catch((error) => {
     this.setState({
         error: 'Error retrieving data',
         loading: false
     });
 });
  }
  renderSeparator = () => {
    return (
      <View
        style={{
          
        }}
      />
    );
  };
  handleLoadMore = () => {
    if (!this.state.loading) {
      this.page = this.page + 1; // increase page by 1
     // method for API call 
     console.log("dkhel 622")
    
     this.getBlog(this.page);
 
 
    }
  };

  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
     if (!this.state.loading) return null;
     return (
       <ActivityIndicator
         style={{ color: '#000' }}
       />
     );
   };
 
 
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.getBlog(this.page);
  }

  componentWillMount() {
   
      
  

 AsyncStorage.multiGet([
  "header",
  
]).then(data => {
  // console.log("catt data[2][1]");
   console.log("header" + data[0][1]);

    this.setState({
      headr: data[0][1],
 
    });
  })
  }
  

  handleBackPress = () => {
    this.props.navigation.navigate("FoodLogin");
    return true;
  };

 
 
  renderItem = ({ item, index }) => {
 

    return (
      <View style={styles.mainListRenderRow}>
        <View style={styles.Foodimg}>
          <Image source={{uri:item.Foodimg}} style={styles.Foodimg} />

         
        </View>
        <TouchableOpacity  onPress={() =>this.props.navigation.navigate("BlogDetails",{
                            id:item.id,
                            screen:"page2"
                          }) }> 
          <Text style={styles.FoodDetailsText}>{item.FoodName}</Text>
          <Text style={styles.FoodANameText}>{item.Description} </Text>
          <Text style={styles.FoodANameText}> </Text>
        </TouchableOpacity>
      
      </View>
      
    );
  };

  render() {
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#000000", true);
      StatusBar.setTranslucent(true);
    }

    

    return (
    
      <ScrollView>
       
      <View style={styles.mainView}>

 
         <View style={styles.MainListBg}>
           
            
            <FlatList
                         // contentContainerStyle={styles.listContent}

                          data={this.state.blog}
                          renderItem={this.renderItem}
                          refreshControl={
                            <RefreshControl
                              refreshing={this.state.isRefreshing}
                              onRefresh={this.onRefresh.bind(this)}
                            />
                          }
                          keyExtractor={(item, index) => item.id.toString()}
                          ItemSeparatorComponent={this.renderSeparator}
                          ListFooterComponent={this.renderFooter.bind(this)}
                          onEndReachedThreshold={0.4}
                          onEndReached={this.handleLoadMore.bind(this)}

                          ListEmptyComponent={
                            <View style={{ justifyContent: "center" }}>
                              <Text style={{ alignSelf: "center" }}>
                              
                              </Text>
                            </View>
                          }
                          
                        />

















           
          </View>
        
      </View>
      </ScrollView>
    );
    
  }
}

export default withNavigation(Favorites);