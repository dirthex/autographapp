import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	StatusBar,
	TouchableOpacity,
	Platform,
	BackHandler,
	I18nManager,
	AsyncStorage,
	Dimensions,
	Alert
} from 'react-native';
import {
	Content,
	Container,
	Right,
	Header,
	Left,
	Body,
	Title,
} from 'native-base';
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import styles from './styles';
import { Images, Fonts, Metrics, Colors } from '../../../Themes/';
import FloatingLabelInput from '../FloatingLabelInput';
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from "axios";
import Config from "../Config/config"
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class EditInformation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			token: "",
			id: "",
			email: "",
			nom: "",
			tel: "",
			avatar: "",
			type: "",
			siren: "",
			siret: "",
			type: "",
			adresse: "",
			longitude: "",
			latitude: "",
			loading: true,
			description: "",

		};
	}

	componentWillMount() {
		const { navigation } = this.props;

		this.state.id = navigation.getParam('id', null);
		this.state.nom = navigation.getParam('nom', null);
		this.state.email = navigation.getParam('email', null);
		this.state.tel = navigation.getParam('tel', null);
		this.state.adresse = navigation.getParam('adresse', null);
		this.state.description = navigation.getParam('description', null);
		this.state.type = navigation.getParam('type', null);
		this.state.token = navigation.getParam('token', null);
		this.state.latitude = navigation.getParam('latitude', null),
		this.state.longitude = navigation.getParam('longitude', null)
	



		var that = this;
		BackHandler.addEventListener('hardwareBackPress', function () {
			that.props.navigation.navigate('ECommerceMyInformation');
			return true;
		});
	}

	modifierInfo(){
		console.log("*****************")
		console.log("toooooken"+this.state.token)
		console.log(this.state.id)
		const headers = {
			'Authorization': 'Bearer ' + this.state.token
		};
		let data = {
		
				nom: this.state.nom,
				email: this.state.email,
				tel: this.state.tel,
				
		}
		axios.post(Config.SERVER_URL + '/api/v1/profile/info/'+this.state.id, data,  {
			headers
		}
		).then((res) => {
			console.log(res.data)
			if(res.data ==200 ){
				Alert.alert("Modification avec succés")
			}
			else {
				Alert.alert("Veuillez réssayer plutard")
			}
			this.setState({loading: false })





		}).catch((error) => {
			this.setState({
				error: 'Error retrieving data',
				loading: false
			});
		});
		if(this.state.type ==2){
			console.log("2222222222222222222222222222");
			let dataPro = {
				add:this.state.adresse,
				desc:this.state.description
			}
			axios.post(Config.SERVER_URL + '/api/v1/profile/pro/'+this.state.id, dataPro,  {
				headers
			}
			).then((res) => {
				console.log({res})
				if(res.data ==200 ){
					Alert.alert("Modification avec succés")
				}
				else {
					Alert.alert("Veuillez réssayer plutard")
				}
				this.setState({loading: false })
	
	
	
	
	
			}).catch((error) => {
				this.setState({
					error: 'Error retrieving data',
					loading: false
				});
			});
		}

	}
	onChangePasswordClick() {
		this.props.navigation.navigate('ECommerceChangePassword',{
			id:this.state.id,
			token:this.state.token
		});
	}

	onBackClick() {
		this.props.navigation.navigate('ECommerceMyInformation');
	}

	render() {
		StatusBar.setBarStyle('light-content', true);
		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor('#0e1130', true);
			StatusBar.setTranslucent(true);
		}

		return (
			<Container style={styles.container}>
				<Header androidStatusBarColor={'#0e1130'} style={styles.header}>
					<Left style={styles.left}>
						<TouchableOpacity
							style={styles.backArrow}
							onPress={this.onBackClick.bind(this)}>
							<FontAwesome
								name={I18nManager.isRTL ? 'angle-right' : 'angle-left'}
								size={30}
								color="white"
								style={{ paddingRight: 20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body style={styles.body}>
						<Text style={styles.textTitle}>Mon profile</Text>
					</Body>
					<Right style={styles.right}>

					</Right>
				</Header>

				<View style={styles.mainView}>
					<Content>
						<View style={styles.floatingView}>
							<FloatingLabelInput
								label="Name"
								value={this.state.nom}
								keyboardType="default"
								returnKeyType="done"
								maxLength={20}
								selectionColor={'#959595'}
								autoCapitalize="none"
								onChangeText={nom => this.setState({ nom })}
							/>

							<View style={{ height: 10 }} />

							<FloatingLabelInput
								label="Email"
								value={this.state.email}
								keyboardType="default"
								returnKeyType="done"
								maxLength={20}
								selectionColor={'#959595'}
								autoCapitalize="none"
								onChangeText={email => this.setState({ email })}
							/>

							<View style={{ height: 10 }} />

							<FloatingLabelInput
								label="Tel"
								value={this.state.tel}
								keyboardType="email-address"
								returnKeyType="done"
								maxLength={40}
								selectionColor={'#959595'}
								autoCapitalize="none"
								onChangeText={tel => this.setState({ tel })}
							/>

							<View style={{ height: 10 }} />
							<FloatingLabelInput
								label="Adresse"
								value={this.state.adresse}
								keyboardType="email-address"
								returnKeyType="done"
								maxLength={40}
								selectionColor={'#959595'}
								autoCapitalize="none"
								onChangeText={adresse => this.setState({ adresse })}
							/>

							{this.state.type == 2 ? (
								<View>
									
								
									<View style={{ height: 10 }} />
									<FloatingLabelInput
										label="Description"
										value={this.state.description}
										keyboardType="email-address"
										returnKeyType="done"
										maxLength={40}
										selectionColor={'#959595'}
										autoCapitalize="none"
										onChangeText={description => this.setState({ description })}
									/>
									<MapView
                      style={styles.MapView}
                      provider={PROVIDER_GOOGLE}
                      region={{
                        latitude: parseFloat(this.state.latitude),
                        longitude: parseFloat(this.state.longitude),
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                      }}
                      initialRegion={{
                        latitude: parseFloat(this.state.latitude),
                        longitude: parseFloat(this.state.longitude),
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                      }}
                    >

                      <MapView.Marker
                        coordinate={{
                          latitude: parseFloat(this.state.latitude),
                          longitude: parseFloat(this.state.longitude),
                          latitudeDelta: 0.015,
                          longitudeDelta: 0.0121
                        }}
                      >
                        <Entypo name="location-pin" size={35} color="#f05522" />
                      </MapView.Marker>


                    </MapView>

						

								</View>

							) : null}

							<View style={{ height: 10 }} />




						</View>
					</Content>
					<View style={styles.editInfoMainView}>
						<View style={{ flexDirection: 'row', }}>

							<TouchableOpacity
								style={styles.editInfoView}
								onPress={() => this.modifierInfo()}>
								<Text style={styles.editInfoText}>Mettre a jour</Text>
							</TouchableOpacity>

							<TouchableOpacity
								style={styles.editInfoView}
								onPress={this.onChangePasswordClick.bind(this)}>
								<Text style={styles.editInfoText}>Changer le mot de passe</Text>
							</TouchableOpacity>
						</View>

					</View>
				</View>
			</Container>
		)
	}
}
